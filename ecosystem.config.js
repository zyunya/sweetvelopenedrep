module.exports = {
	/**
	 * Application configuration section
	 * http://pm2.keymetrics.io/docs/usage/application-declaration/
	 */
	apps: [
		//MAIN APPLICATION CHAKIRBERG FRONTEND SERVER
		{
			name: "chFront",
			script: "server.js",
			cwd: "/var/www/chakirberg",
			//watch     : ["/var/www/chakirberg/dist","/var/www/chakirberg/dist-server"],
			env: { NODE_ENV: "prod" }
		},

		// MAIN APPLICATION CHAKIRBERG API SERVER
		{
			name: "chBack",
			script: "app.js",
			cwd: "/var/www/chakirberg/SERVER",
			instances: 2, //OR "max"
			exec_mode: "cluster", // OR "fork" in fork mode remove 'instances' field
			//watch     : ["app.js","controllers/"],
			env: { NODE_ENV: "prod" }
		},

		// First application
		{
			name: 'swos',
			script: '/var/www/swproject/SERVER/app.js',
			env: {
				COMMON_VARIABLE: 'true'
			},
			env_production: {
				NODE_ENV: 'production'
			}
		},

		/* Second application
		{
		  name      : 'swgiftsapi',
		  script    : '/var/www/swgifts/SERVER/app.js',
		  env: {
			NODE_ENV: 'prod'
		  }
		},
	   {
		  name      : 'swgiftsserverrendered',
			script    : '/var/www/swgifts/server.js',
		  watch     : ["/var/www/swgifts/dist","/var/www/swgifts/dist-server"],
		  env: {
			COMMON_VARIABLE: 'true',
			 NODE_ENV: 'prod'

		  },
		  env_production : {
			NODE_ENV: 'prod'
		  }
		}
	*/

	],

	/**
	 * Deployment section
	 * http://pm2.keymetrics.io/docs/usage/deployment/
	 */
	deploy: {
		production: {
			user: 'node',
			host: '212.83.163.1',
			ref: 'origin/master',
			repo: 'git@github.com:repo.git',
			path: '/var/www/production',
			'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
		},
		dev: {
			user: 'node',
			host: '212.83.163.1',
			ref: 'origin/master',
			repo: 'git@github.com:repo.git',
			path: '/var/www/development',
			'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env dev',
			env: {
				NODE_ENV: 'dev'
			}
		}
	}
};

