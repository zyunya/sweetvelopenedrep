var mongoose = require("mongoose");

var typosSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true,
		default : ""
	},

	number :{
		type: Number,
		required: true,
		default : 0
	},


	lang: {
		type: String,
		default: "ru"
	}

}
);

var typos = mongoose.model('typos',  typosSchema );

module.exports = {
	Typos :  typos
}