var mongoose = require("mongoose");

var themesSchema = new mongoose.Schema({

	number : {
		type: Number,
		required: true
	},

	name: {
		type: String,
		required: true
	},

	primarycolor :{
		type: String,
		required: true,
	},

	secondarycolor :{
		type: String,
		required: true,
	},

	altcolor :{
		type: String,
		required: true,
	},


},
{
	timestamps:
	{
		createdAt: 'created_at',
		updatedAt: 'updated_at'
	}

});


var themes = mongoose.model('themes', themesSchema);

module.exports = {
	Themes : themes
}