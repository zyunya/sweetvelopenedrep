var mongoose = require("mongoose");

var PostSchema = new mongoose.Schema({

	owner: {
		type: mongoose.Schema.ObjectId,
		ref: "kings",
		required: true
	},

	title: {
		type: String,
		index: true,
		required: true
	},

	desc: {
		type: String,
		required: false,
		default: ""
	},
/*
	theme: {
		type: Number,
		required: false,
		default: 1
	},
*/
	category: {
		type: Number,
		required: true,
	},
/*
	type: {
		type: String,
		required: true
	},
*/
	image: {
		type: Array,
		required: false
	},

	mainimage: {
		type: String,
		required: false,
		default: ''
	},

	active: {
		type: Number,
		default: 1
	},

	anonym: {
		type: Boolean,
		default: false
	},

	approve: {
		type: Boolean,
		default: false
	},

	gallery: {
		type: Boolean,
		default: false
	},

	priority: {
		type: Number,
		default: 0
	},

	production: {
		type: Boolean,
		default: false
	},

	public: {
		type: Boolean,
		default: false
	},

	comments: {
		type: Boolean,
		default: true
	},

	productiontimestamp: {
		type: Date
	},

	lang: {
		type: String,
		default: "ru"
	}

},
	{
		timestamps:
			{
				createdAt: 'created_at',
				updatedAt: 'updated_at'
			}
	}
);


var Post = mongoose.model('Posts', PostSchema);

// VIRTUAL INNER JOIN POPULATION ///

PostSchema.virtual('ctg', {
	ref: 'categories',
	localField: 'category',
	foreignField: 'number',
	justOne: true
});



module.exports = {
	Post: Post
}