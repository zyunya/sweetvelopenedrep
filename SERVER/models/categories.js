var mongoose = require("mongoose");

var categorieSchema = new mongoose.Schema({

	name: {
		type: String,
		required: true,
	},

	number :{
		type: Number,
		required: true,
	},


	lang: {
		type: String,
		default: "ru"
	}

}
);

var categorie = mongoose.model('categories', categorieSchema);

module.exports = {
	Categories : categorie
}