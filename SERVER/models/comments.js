var mongoose = require("mongoose");

var commentsSchema = new mongoose.Schema({

	owner : {
		type: mongoose.Schema.ObjectId,
		ref: "kings",
		required: true
	},

	/* ENTITY WHICH OWNS COMMENTS */
	commentbox: {
		type: mongoose.Schema.ObjectId,
		required: true
	},

	comment :{
		type: String,
		required: true,
	},

	type : {
		type : String,
		required: false,
		default : "post"
	}

},
{
	timestamps:
	{
		createdAt: 'created_at',
		updatedAt: 'updated_at'
	}

});


var comments = mongoose.model('comments', commentsSchema);

module.exports = {
	Comments : comments
}