var mongoose = require("mongoose");

var KingsSchema = new mongoose.Schema({

	login: {
		type: String,
		index: true,
		required : true
	},

	password: {
		type: String,
		required :true,
		select: false
	},

	email :{
		type : String,
		required : true,
		default : '',
	},

	firstname :{
		type : String,
		required : false,
		default : ''
	},

	lastname :{
		type : String,
		required : false,
		default : ''
	},

	typo :{
		type : Number,
		required : false,
		default : 0
	},

	pokerface: {
		type: Array,
		required :false,
		default : ""
	},

	pokerfaceformatted : {
		type: Array,
		required :false,
		default : []
	},

	role:{
		type : String,
		required:true
	},

	active:{
		type : Number,
		default: 1
	},

	anonym:{
		type : Number,
		default: 0,
		required:false
	},

	about:{
		type : String,
		default: ''
	},

	lang:{
		type : String,
		required : false
	},

	theme:{
		type : Number,
		required : false,
		default: 0
	},

	instagram:{
		type : String,
		default: ''
	},

	facebook:{
		type : String,
		default: ''
	},

	vkontakte:{
		type : String,
		default: ''
	},

	linkedin:{
		type : String,
		default: ''
	},

	twitter:{
		type : String,
		default: ''
	},

},
{
	timestamps:
	{
		createdAt: 'created_at',
		updatedAt: 'updated_at'
	}

});


var Kings = mongoose.model('kings', KingsSchema);

KingsSchema.virtual('posts', {
	ref: 'Posts',
	localField: '_id',
	foreignField: 'owner'
});


KingsSchema.virtual('tps', {  // ALIAS NAME
	ref: 'typos',			// MODEL NAME
	localField: 'typo',
	foreignField: 'number',
	justOne: true
});


KingsSchema.virtual('style', {  // ALIAS NAME
	ref: 'themes',			// MODEL NAME
	localField: 'theme',
	foreignField: 'number',
	justOne: true
});


module.exports = {
	Kings: Kings
}