const crypto = require('crypto');
var env = require('./config/env')[process.env.NODE_ENV];
var jwt = require('jsonwebtoken');
var config = require("./config/config");
var striptags = require('striptags');
var sanitizeHtml = require('sanitize-html');


const HELPERS = {

	hashGen() {
		return crypto.createHash('sha1').update(new Date().toString()).digest('hex');
	},

	hashGenSalt(salt) {
		return crypto.createHash('sha1').update( new Date().toString() +  salt).digest('hex');
	},

	getImgArray(source) {
		var imageName = [];
		source.forEach(el => {
			imageName.push(el.filename)
		});
		return imageName;
	},


	imgAbs(arr) {
		return arr.map((img) => {
			return env.IMG_PATH + img
		})
	},


	imgAbsOne(one) {
		return env.IMG_PATH + one;
	},


	fetchPayload(req) {
		if(req.headers.authorization)
			return jwt.decode(req.headers.authorization)
	},


	stringTokenPayload(token) {
		if(token)
			return jwt.decode(token);
	},


	superUser(role) {
		if (role === "superuser")
			return true;
		else
			return false;
	},

	superUserOk(req) {
		if (jwt.decode(req.headers.authorization).role === "superuser")
			return true;
		else
			return false;
	},


	fetchAll(all) {
		if (all === "ALL")
			return true;
		else
			return false;
	},

	searchFilter(all) {
		if (all === 888)
			return true;
		else
			return false;
	},

	imageValidation(img){

		var type = img.img.type ;
		var size  = img.img.size ;

		if( !config.allowedImgFormats.includes( type ) )
			return  `wrongfileformat`;

		if( size > config.imgMaxSize )
			return  `Max size ${config.imgMaxSizeString} `;

		else
			return true;

	},


	handleMulterErrors(err){

		if( err && err.code !== undefined ){

			switch(err.code){

				case "LIMIT_UNEXPECTED_FILE" : err.code = `maximumfilestoupload` ; break;
				case "LIMIT_FILE_SIZE"        : err.code =`maximumfilesize` 	; break;
				default : err.code = "BLABLABLA" ; break;

			}

			return err.code;

		}

		else
			return err;

	},


	getRandomInt() {

		var rand =  Math.floor(Math.random() * (800 - 100)) + 100;
		rand = Math.floor(rand);
		return rand;

	},


	secureHtml(html){

		 return sanitizeHtml(html, {

			allowedTags: ['p', 'h3', 'h2', 'h1', 'figure', 'figurecaption','img', 'section', 'iframe', 'article', 'div','font','b','i','a','br','hr'],

			allowedAttributes: {

				'a': ['href'],
				'iframe':['src' ,'width' , 'height' , 'style' , 'scrolling' , 'frameborder' , 'allowTransparency'],
				'img' :['src','class'],
				'figure' :['class'],
				'font' : ['color'],
				'hr' : ['class'],
				'div' : ['class']

			},

			allowedIframeHostnames: ['www.youtube.com','www.facebook.com','twitframe.com','https://twitframe.com']

		});

	}


}

module.exports = HELPERS;