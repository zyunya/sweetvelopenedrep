const express = require('express');
const router = express.Router();
const requestCtl = require('../controllers/requestController');


router.post(`/checkyoutubelink`, requestCtl.youtubeLinkHandler);
router.post(`/opengraph`     , requestCtl.openGraph);


module.exports = router;