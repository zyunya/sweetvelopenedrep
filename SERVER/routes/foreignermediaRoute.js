const express = require('express');
var   config = require('../config/config');
const router = express.Router();
const foreignermediaCtl = require('../controllers/foreignermediaController');
const routePrefix = "/foreignermedia";

/*

#name : String    = Account Name
#skip  : Number   = Skip posts
#limit  : Number  = Limit f posts to fetch
#lang  : String    = Language of posts to fetch

AVAILABLE LANGS RU AND EN IF YOU WANT TO FETCH ALL LANGS POSTS USE 'none' as LANG PARAM
EXAMPLES

/fetchposts/sweetvel/0/20/3/ru`
/fetchposts/bestseller/10/20/3/en`
/fetchposts/chakirberg/0/40/2/none`

*/

router.get(`${routePrefix}/fetchposts/:name/:skip/:limit/:category/:lang`, foreignermediaCtl.fetchForeignerData);
router.get(`${routePrefix}/fetchcategories/:name`, foreignermediaCtl.fetchForeignerCategories);

module.exports = router;