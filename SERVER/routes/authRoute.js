const express = require('express');
const formidable = require('express-formidable');
const router = express.Router();
const authCtl = require('../controllers/authController');
var url  = require('url');


/* SIGN UP USERS

#POST -Body

login         : String
email         : String
password      : String
repeatpassword : String

*/

router.post('/sign/signupuser',  authCtl.signUpUser);


router.post('/sign/signup',   authCtl.signUpAdmin);
router.get('/sign/signupconfirmation/:token',  authCtl.signUpAdminConfirmation);
router.post('/sign/signin',  authCtl.signIn);


module.exports = router;