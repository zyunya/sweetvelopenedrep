const express = require('express');
const formidable = require('express-formidable');
const router = express.Router();
const kingsCtl = require('../controllers/kingsController');
const SuperAdminMW = require('../middleware/superadmin');
var url  = require('url');


/* SUPER ADMIN ACTIONS */
router.put('/supermode/superadminkeyupdater', kingsCtl.updateSignUpKey);
router.get('/supermode/getsignupkey', kingsCtl.getCurrentSignUpKey);
router.put('/supermode/setuseractive/:id', kingsCtl.setUserActive);
router.get('/supermode/userslist/:limit'  ,  kingsCtl.usersList);
router.get('/supermode/userslist/:limit/:name'  ,  kingsCtl.usersListByName);
router.delete('/supermode/deleteaccount/:id'  ,  kingsCtl.deleteAccount);


/* DASHBOARD */
router.get('/dash/fetchprofile', kingsCtl.fetchProfile);
router.put('/dash/editprofile', kingsCtl.editProfile);
router.put('/dash/resetpassword', kingsCtl.resetPassword);
router.put('/dash/resetavatar', formidable() , kingsCtl.resetAvatar);


module.exports = router;