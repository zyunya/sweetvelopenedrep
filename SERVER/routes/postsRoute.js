const express = require('express');
var config = require('../config/config');
const formidable = require('express-formidable');
const router = express.Router();
const postCtl = require('../controllers/postsController');
const SuperAdminMW = require('../middleware/superadmin');

/*FOR ADMIN */

router.get('/dash/fetchposts/:skip/:limit/:category/:active/:lang', postCtl.fetchPosts);
router.get('/dash/fetchposts/:skip/:limit/:category',           postCtl.fetchPosts);
router.get('/dash/fetchposts/:skip/:limit',                   postCtl.fetchPosts);

/* SUPER USER MODE */
router.get('/supermode/fetchposts/:skip/:limit/:active/:lang', postCtl.fetchPostsSuperUser);
router.put('/supermode/activationpost/:id'              , postCtl.activationPost);
router.put('/supermode/setapprove/:id'                , postCtl.setApprove);
router.put('/supermode/setpriority/:id'                 , postCtl.setPriority);

/* COMON ADMIN CRUD  */
router.delete('/dash/removepost/:postid', postCtl.removePost);
router.post('/dash/createpost/:lang',     postCtl.createPost);
router.put('/dash/updatepost/:id/:lang',   postCtl.updatePost);
router.put('/dash/removepostimage/:id',  postCtl.removePostImage);
router.put('/dash/uploadpostimage/:id',   postCtl.uploadPostImages);
router.put('/dash/setmainpostimage/:id',  postCtl.setMainImage);


router.put('/dash/setanonym/:id' ,   postCtl.setAnonym);
router.put('/dash/setgallery/:id' ,    postCtl.setGallery);
router.put('/dash/setcomments/:id' , postCtl.setComments);
router.put('/dash/setproduction/:id' , postCtl.setProduction);

/*FOR USERS */

router.get('/fetchpost/:id',  postCtl.fetchPost);
router.get('/fetchposts/:skip/:limit/:category/', postCtl.fetchPosts);

module.exports = router;