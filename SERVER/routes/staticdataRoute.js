const express = require('express');
var config = require('../config/config');
const router = express.Router();
const staticdataCtl = require('../controllers/staticdataController');
const routePrefix = "/staticdata";

router.get(`${routePrefix}/fetchcategories`, staticdataCtl.fetchCategories);
router.get(`${routePrefix}/fetchtypos`, staticdataCtl.fetchTypos);
router.get(`${routePrefix}/fetchthemes`, staticdataCtl.fetchThemes);
router.get(`${routePrefix}/fetchvideostream`, staticdataCtl.fetchVideoStream);

module.exports = router;