//SOME TIME AFTER MAKE AUTO REQUIRE


var auth 	      = require('./authRoute');
var kings 	      = require('./kingsRoute');
var posts 	      = require('./postsRoute');
var userMedia     = require('./usermediaRoute');
var foreignerMedia = require('./foreignermediaRoute');
var request       = require('./requestRoute');
var staticdata     = require('./staticdataRoute');


module.exports = [

	auth ,
	kings ,
	posts ,
	userMedia ,
	foreignerMedia ,
	request,
	staticdata

]