const express = require('express');
var config = require('../config/config');
const router = express.Router();
const usermediaCtl = require('../controllers/usermediaController');
const routePrefix = "/usermedia";
const AuthNeeded = "/usermedia/actions";
const  SuperMode = "/supermode"

router.get(`${routePrefix}/fetchpost/:id`, usermediaCtl.fetchPost);
router.get(`${routePrefix}/fetchposts/:skip/:limit/:category/:lang`, usermediaCtl.fetchPosts);
router.get(`${routePrefix}/fetchpostssaved/:skip/:limit/:postidarray`, usermediaCtl.fetchPostsSaved);
router.get(`${routePrefix}/fetchrelatedposts/:id/:limit/:category/:lang`, usermediaCtl.fetchRelatedPosts);
router.get(`${routePrefix}/fetchdesks/:limit`, usermediaCtl.fetchDesksRandom);
router.get(`${routePrefix}/httpstest`, usermediaCtl.httpstest);

/* HANDLE COMMENTS */
router.get(`${routePrefix}/fetchcomments/:commentbox/:limit/:skip`, usermediaCtl.fetchComments);
router.post(`${AuthNeeded}/addcomment`, usermediaCtl.addComment);
router.delete(`${AuthNeeded}/removecomment/:id`, usermediaCtl.removeComment);
router.delete(`${AuthNeeded}/removecommentowner/:id`, usermediaCtl.removeCommentOwner);
router.delete(`${SuperMode}/removecomment/:id`, usermediaCtl.removeCommentOwner);

module.exports = router;