var ObjectId = require('mongodb').ObjectID;
var env = require('../config/env')[process.env.NODE_ENV];
var cluster = require('cluster');
const HELPER = require('../helpers');
const postmodel = require('../models/post').Post;
const categorymodel = require('../models/categories').Categories;
const commentsmodel = require('../models/comments').Comments;
const kingsmodel = require('../models/kings').Kings;
var jwt = require('jsonwebtoken');


const userMedia = {

	fetchPost(req, res, err) {

		postmodel.findOne({ _id: ObjectId(req.params.id), active: 1, production: true }, function (err, item) {

			if (err)
				res.status(400).send({ status: 400, statusText: err, text: "Error" });

			if (!item)
				res.status(400).send({ status: 400, statusText: "PAGE NOT EXISTS" });

			else {
				item.image = HELPER.imgAbs(item.image);
				item.mainimage = item.image[0] || env.BROKEN_IMG;
				try { item.owner.pokerfaceformatted = HELPER.imgAbs(item.owner.pokerface); }
				catch (err) { console.log("PROPERTY OWNER UNDEFINED") };

				res.status(200).send({ status: 200, data: item });

			}

		}).populate({ path: 'owner', populate: { path: 'tps', model: 'typos' } });

	},



	fetchPosts(req, res, err) {

		try{
			console.log("I, worker " + cluster.worker.id || 0 + ", am alive");
		}
		catch(err){
			console.error("[ CLUSTER_ID_ERROR ] - " + err)
		}

		/* REQ PARAMS */
		var SKIP = parseInt(req.params.skip);
		var LIMIT = parseInt(req.params.limit);
		var NAME = req.params.name;
		var LANG = req.params.lang || "";
		var newposts = [];

		/*   OPTIONS   */
		var query = {};
		query.active = 1;
		query.anonym = false;
		query.production = true;
		query.lang = LANG;
		req.params.category !== "ALL" ? query.category = req.params.category : null;

		/*   COUNTER   */
		postmodel.count(query).exec((err, count) => {

			postmodel
				.find(query)
				.sort({ priority: -1, created_at: -1 })
				.skip(SKIP)
				.limit(LIMIT)
				.populate({ path: 'owner', populate: { path: 'tps', model: 'typos' } })
				.populate('ctg')
				.exec((err, posts) => {

					if (!posts)
						return res.status(200).send({ status: 200, data: [], count: 0 });

					else {

						try { posts.filter((el) => { el.image = HELPER.imgAbs(el.image) }); }

						catch (err) { console.log("IMAGE PROPERTY UNDEFINED") };

						try { posts.filter((el) => { el.owner.pokerfaceformatted = HELPER.imgAbs(el.owner.pokerface) }) }

						catch (err) { console.log("POKERFACE PROPERTY UNDEFINED") };

						return res.status(200).send({ status: 200, data: posts, count: count });

					}

				})

		})

	},


	fetchPostsSaved(req, res, err) {

		/* REQ PARAMS */
		var LIMIT = parseInt(req.params.limit);
		var SKIP = parseInt(req.params.skip);
		var POSTSARR = req.params.postidarray;

		/*   OPTIONS   */
		var query = {};
		query.active = 1;
		query.anonym = false;
		query.production = true;
		query._id = { $in: JSON.parse(POSTSARR) };

		/* QUERY */
		postmodel
			.find(query)
			.sort({ priority: -1, created_at: -1 })
			.skip(SKIP)
			.limit(LIMIT)
			.populate({ path: 'owner', populate: { path: 'tps', model: 'typos' } })
			.populate('ctg')
			.exec((err, posts) => {

				if (!posts)
					return res.status(200).send({ status: 200, data: [], count: 0 });

				else {

					try { posts.filter((el) => { el.image = HELPER.imgAbs(el.image) }); }

					catch (err) { console.log("IMAGE PROPERTY UNDEFINED") };

					try { posts.filter((el) => { el.owner.pokerfaceformatted = HELPER.imgAbs(el.owner.pokerface) }) }

					catch (err) { console.log("POKERFACE PROPERTY UNDEFINED") };

					return res.status(200).send({ status: 200, data: posts });

				}

			})


	},



	fetchRelatedPosts(req, res, err) {

		var ID = req.params.id;
		var LIMIT = parseInt(req.params.limit);
		var CATEGORY = parseInt(req.params.category);
		var LANG = req.params.lang;

		postmodel.aggregate([

			{ $match: { _id: { $ne: ObjectId(ID) }, active: 1, production: true, lang: LANG } },
			{ $sample: { size: LIMIT } }

		]).exec((err, item) => {

			if (err)
				res.status(400).send({ status: 400, statusText: err });

			if (!item)
				res.status(400).send({ status: 400, statusText: "NO RELATED POSTS" });

			else
				item.filter((el) => { return el.image = HELPER.imgAbs(el.image) }),
					res.status(200).send({ status: 200, data: item });

		});

	},


	fetchDesksRandom(req, res, err) {

		let LIMIT = parseInt(req.params.limit);

		kingsmodel.aggregate([

			/* FETCH ONLY DESKS WITH PROFILE PICTURES AND WITH APPROPRIAATE LOGIN NAMES */
			{ $match:  { active: 1 , login : /^[a-zA-Z]+$/ , "pokerface.0" : { $ne: "" } }  },
			{ $sample: { size: LIMIT } },
			{ $project: { login: 1, about : 1, pokerface: 1, pokerfaceformatted: 1, typo : 1 , tps: 1 } },

			{
				$lookup: {
					from: "typos",
					localField: "typo",
					foreignField: "number",
					as: "tps"
				}
			},

			{ $unwind: "$tps" }

		]).exec((err, item) => {

				if (err)
					return	res.status(400).send({ status: 400, statusText: err });

				if (!item)
					res.status(400).send({ status: 400, statusText: "NO ITEMS" , data : [] });

				else
					item.filter((el) => { return el.pokerfaceformatted = HELPER.imgAbs(el.pokerface) }),
						res.status(200).send({ status: 200, data: item });

			})


	},


	addComment(req, res, err) {

		req.body.owner = jwt.decode(req.headers['authorization']).id;

		var comment = new commentsmodel(req.body);

		comment.save((err, inserted) => {

			if (err)
				res.status(400).send({ status: 400, statusText: err });

			else
				Class.fetchLastComment(req, res, ObjectId(inserted._id))

		})
	},


	removeComment(req, res, err) {

		var owner = jwt.decode(req.headers['authorization']).id;

		commentsmodel.deleteOne({ _id: ObjectId(req.params.id), owner: ObjectId(owner) })

			.then((data, err) => {

				res.status(200).send({ status: 200, statusText: "deleted", id: req.params.id });

			})

			.catch(err => res.status(400).send({ status: 400, statusText: err }))

	},


	removeCommentOwner(req, res, err) {

		var owner = jwt.decode(req.headers['authorization']).id;

		commentsmodel.deleteOne({ _id: ObjectId(req.params.id) })

			.then((data, err) => {

				res.status(200).send({ status: 200, statusText: "deleted", id: req.params.id });

			})

			.catch(err => res.status(400).send({ status: 400, statusText: err }))

	},


	fetchComments(req, res, err) {

		let SKIP = parseInt(req.params.skip);
		let LIMIT = parseInt(req.params.limit);

		commentsmodel.find({ commentbox: ObjectId(req.params.commentbox) })

			.skip(SKIP)
			.limit(LIMIT)
			.sort({ created_at: -1 })
			.populate('owner')
			.exec((err, data) => {

				if (err)
					res.status(400).send({ status: 400, statusText: err });

				else {

					try { data.filter((el) => { return el.owner.pokerfaceformatted = HELPER.imgAbs(el.owner.pokerface) }) }

					catch (err) { console.log("UNEFINED PROPERTY"); }

					res.status(200).send({ status: 200, statusText: 'Success', data: data });

				}

			})


	},


	fetchLastComment(req, res, id) {

		commentsmodel.findOne({ _id: id })

			.populate('owner')
			.exec((err, data) => {

				try { data.owner.pokerfaceformatted = HELPER.imgAbsOne(data.owner.pokerface) }

				catch (err) { console.log("UNEFINED PROPERTY"); }

				res.status(200).send({ status: 200, statusText: 'Success', data: data });

			})

	},


	httpstest(req, res, err) {
		res.send("HTTPS WORKS FINE")
	}


}

var Class = userMedia;

module.exports = userMedia;