var ObjectId = require('mongodb').ObjectID;
var env = require('../config/env')[process.env.NODE_ENV];
const HELPER = require('../helpers');
const postmodel = require('../models/post').Post;
const kingsmodel = require('../models/kings').Kings;
const categorymodel = require('../models/categories').Categories;


const foreignerMedia = {

	fetchForeignerData(req, res, err) {

		var query = {};
		var SKIP = parseInt(req.params.skip);
		var LIMIT = parseInt(req.params.limit);
		var NAME = req.params.name;
		var LANG = req.params.lang
		var CATEGORY = req.params.category;

		kingsmodel.findOne({ login: NAME , active : 1 })

			.populate({ path: 'tps' })
			.populate({ path: 'style' })
			.exec((err, user) => {

				if(user){

					/* GENERATING IMAGE ABSOLUTE PATH */
					user.pokerfaceformatted = HELPER.imgAbs(user.pokerface);

					/* QUERY OPTIONS */
					query.owner     = user._id;
					query.production = true;
					query.active     = 1;
					CATEGORY !== "ALL"  ? query.category = req.params.category : null;
					LANG     !== "none" ? query.lang     = req.params.lang    : null;

					/*   COUNTER   */
					postmodel.count(query).exec((err, count) => {

						postmodel.find(query)

							.sort({ priority: -1, created_at: -1 })
							.skip(SKIP)
							.limit(LIMIT)
							.populate({ path: 'ctg' })
							.exec((err, posts) => {

								if(!posts)
									return res.status(200).send({ status: 200, owner: {}, posts: [], count: 0 });

								else{
									posts.filter((el) => { return el.image = HELPER.imgAbs(el.image) });
									return res.status(200).send({ status: 200, owner: user, posts: posts, count: count });
								}

						})

					});

				}
				else{
					return res.status(400).send({ status: 400,   statusText : "NO USER" });
				}

			});

	},


	fetchForeignerCategories(req, res, err){

		var NAME = req.params.name;

		kingsmodel.findOne({ login: NAME , active : 1 }, '_id').exec((err, user) => {

				if(user){

					postmodel.find({owner : ObjectId(user._id) , active : 1 , production : true }).distinct('category').exec((err, categories) => {

							categorymodel.find({ number : { $in : categories }} ,'name number lang -_id' ).exec( (err , ctg) =>{

								if(err)
									return res.status(400).send({ status: 400,   statusText :err });

								if(ctg)
									return res.status(200).send({ status: 200, ctg : ctg  });

								else
									return res.status(200).send({ status: 200, ctg : []  });

							} )

						})

				}

			})

	}


}


var Class = foreignerMedia;

module.exports = foreignerMedia;