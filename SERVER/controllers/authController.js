var nodemailer     = require('nodemailer');
var config         = require('../config/config');
const kingmodel    = require('../models/kings').Kings;
var env           = require('../config/env')[process.env.NODE_ENV];
var ObjectId       = require('mongodb').ObjectID;
var emailValidator  = require("email-validator");
var bcrypt        = require('bcrypt');
var HELPER       = require("../helpers");
var crypto        = require('crypto');
var fs            = require("fs");
var jwt           = require('jsonwebtoken');


const authCtl  = {


	signUpAdmin(req, res) {


		var strings     = req.body.strings;
		var receiver    = req.body.email.toLowerCase();
		var key        = req.body.key;
		var login       = req.body.email.toLowerCase();
		var password   = HELPER.hashGen()
		var token      = Class.makeTokenForRegister(login, password, req.body.email);
		var signUpCert  = fs.readFileSync(config.signupKey).toString().trim();
		// var login = HELPER.hashGenSalt(req.body.email);


		var messagecontent = `
			<p><img src = '${strings.stringmailimg}'></p>
			<p><a href = '${env.FINISH_SIGN_UP_PAGE}/${token}'>${strings.string1}</a></p>
			<p><b style = "color:yellowgreen">* ${strings.string2}:</b></p>
			<p><b>${strings.stringlogin} :</b> ${login}</p>
			<p><b>${strings.stringpassword} :</b> ${password} </p>
			<p><b style = "color:yellowgreen">* ${strings.string3}</b></p>`;


		var successText = `${strings.string5} ${receiver} ${strings.string4}`;
		var transporter = nodemailer.createTransport(config.SMTP);


		var mailOptions = {

			from   : config.adminEmail,
			to     : receiver,
			subject : strings.string6,
			html   : messagecontent

		};


		if (signUpCert !== key)
			return res.status(400).send({ status: 400, statusText: "wrongkey", });

		if (!emailValidator.validate(receiver))
			return res.status(400).send({ status: 400, statusText: "wrongemail", });

		kingmodel.findOne({ email : receiver }, "email" , (err,email) =>{

			if(email)
				return res.status(400).send({ status: 400, statusText: 'emailexists' });

			transporter.sendMail(mailOptions, function (error, info) {

				if (error)
					return res.status(400).send({ status: 400, statusText: "error", response: error }), console.log(error);

				else
					res.status(200).send({ status: 200, statusText: successText, response: info.response }), console.log(info.response);

				});

			})


	},



	signUpAdminConfirmation(req, res) {

		var token = req.params.token;
		var cert = fs.readFileSync(config.secretKey);

		jwt.verify(token, cert, (err, decoded) => {

			if (err)
				res.status(400).send({status: 400 , statusText :err.name == "TokenExpiredError" ? "linkexpired" : err.name });


			else {

				decoded.role = "admin";
				decoded.password = bcrypt.hashSync(decoded.password, 10)

				//Class.checkProfileExistanceByEmail(decoded.email).then(data => {

					new kingmodel(decoded).save((err, event) => {

						if (err)
							res.status(400).send({status: 400 , statusText : err });

						else
							res.status(200).send({statusText :"accountcreated"});

					});

				//}).catch(err => { res.status(400).send({status: 400 , statusText : err });});
			}

		});

	},



	/*  SIGN UP  FOR   USERS    */
	signUpUser(req ,res){


		/* EMAIL VALIDATION  */
		if(!emailValidator.validate(req.body.email))
			return res.status(400).send({ statusText: "wrongemail" });


		/* CHECK IF FORM NEW PASSWORDS ARE EQUAL */
		if (req.body.password !== req.body.repeatpassword)
			return res.status(400).send({ statusText: "passwordsnotequal" });


		/*  BODY  */
		var king = new kingmodel({

			login     : req.body.login,
			email    : req.body.email,
			password : bcrypt.hashSync(req.body.password , 10),
			role      : "user"

		})


		/* CHECK ACCOUNT EXISTANCE */
		Class.checkProfileExistanceByEmailandLogin(req.body.login, req.body.email).then(

			data => {

				if(data){

					king.save((err, event) => {

						if (err)   res.status(400).send({status: 400 , statusText : err });

						else      res.status(200).send({statusText :"accountcreated"});

					});

				}

			},

			err =>   res.status(400).send({ status: 400, statusText: err   })

		)

	},



	signIn(req, res) {

		var login = req.body.login.trim();
		var password = req.body.password.trim();

		Class.checkLogin(login).then(

			dataLogin => {

				if (dataLogin){

					Class.checkPassword(login, password).then(

						dataPass => {

							if (dataPass) {

								/* CHECK ID ACCOUNT BLOCKED */
								if(dataPass.active === 0) return res.status(400).send({ status: 400, statusText: 'accountblocked' });

								/*ON SUCCESS CREATE AND SEND AUTH TOKEN TO USER */
								var token = Class.makeToken(dataPass._id, dataPass.role, dataPass.login , dataPass.pokerface);
								res.status(200).send({ status: 200, statusText: 'SUCCESS', data: dataPass, token: token });

							}

						},

						err => res.status(400).send({ status: 400, statusText: err })
					)

				}

				else
					res.status(400).send({ status: 400, statusText: 'wrongcredits' });
			},

			err => { cosole.log(err) }
		)

	},


	/*            SUB QUERIES          */
	checkProfileExistanceByEmail(king, email) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ 'email': king }, function (err, kingprofile) {

				if (err)
					reject(err);

				if (kingprofile)
					reject('emailexists');

				else
					resolve(true);
			})

		})

	},


	checkProfileExistanceByEmailandLogin(login, email) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ $or: [ { login : login  }, { email : email  }] }, function (err, kingprofile) {

				if (err)
					reject(err);

				if (kingprofile)
					reject('emailorloginexists');

				else
					resolve(true);
			})

		})

	},


	/*      CHECK LOGIN      */
	checkLogin(king) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ 'login': king }, function (err, kingprofile) {

				if (err)
					reject(err);

				if (!kingprofile)
					resolve(false);

				else
					resolve(true);
			})

		})

	},



	/*      CHECK PASSWORD      */
	checkPassword(king, password) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ login: king }, 'password role login pokerface active', function (err, user) {

				if (err)
					reject(err)

				if (user)

					if (bcrypt.compareSync(password || "", user.password))
						resolve(user)

					else
						reject("wrongcredits")

			})
		})

	},



	/*    TOKEN GENERATORS     */
	makeToken(id, role, login, pokerface) {

		var cert = fs.readFileSync(config.secretKey);  // get private key
		return jwt.sign({ id: id, role: role,login : login , pokerface : `${env.IMG_PATH}${pokerface}` , time: new Date() }, cert, { expiresIn: config.TokenExp });

	},


	makeTokenForRegister(login, password, email) {

		var cert = fs.readFileSync(config.secretKey);  // get private key
		return jwt.sign({ login: login, password: password, email: email, time: new Date() }, cert, { expiresIn: config.TokenExp });

	},





}


var Class = authCtl;


module.exports =  authCtl;