var ObjectId = require('mongodb').ObjectID;
var env = require('../config/env')[process.env.NODE_ENV];
var path = require('path');
var fs = require('fs');
const HELPER = require('../helpers');

const categorymodel = require('../models/categories').Categories;
const typosmodel = require('../models/typos').Typos;
const themessmodel = require('../models/themes').Themes;

const staticData = {

	fetchCategories(req, res, err) {

		categorymodel.find((err, categories) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: err });

			else
				return res.status(200).send(categories);

		});


	},


	fetchTypos(req, res, err) {

		typosmodel.find((err, typos) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: err });

			else
				return res.status(200).send(typos);

		});

	},


	fetchThemes(req, res, err) {

		themessmodel.find((err, themes) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: err });

			else
				return res.status(200).send(themes);

		});

	},


	fetchVideoStream(req, res, err) {

		var filePath = './video.mp4';

		fs.exists(filePath, function (exists) {
			if (exists) {

				var readStream = fs.createReadStream(filePath);
				res.set('content-type', 'video/mp4');
				res.set('accept-ranges', 'bytes');
				readStream.pipe(res);

			}

		})

	}


}

module.exports = staticData;
