var request = require('request');
const cheerio = require('cheerio');
var url = require('url');


const REQUEST = {

	youtubeLinkHandler(req, res, err) {

		var urlString = req.body.urlstr;
		var host = "youtube.com"

		request(urlString, function (error, response, body) {

			if (error)
				return res.status(400).send({ data: error, status: 400 , statusText : "Не Корректная Ссылка" });

			if (!urlString.match(host))
				return	res.status(400).send({ data: error, status: 400 , statusText : "Ссылка не относится к YouTube" });

			var youtube = url.parse(urlString, true);

			if (youtube.query.v !== undefined) {

				var RESULT = {

					host: youtube.host,
					videoid: youtube.query.v,
					embedurl: `https://www.youtube.com/embed/${youtube.query.v}`

				}

				res.status(200).send({ data: RESULT, status: response.statusCode , statusText : "ok" });

			}

			else
				res.status(404).send({ data: {} , status : 404 , statusText : "Видео Не Доступно" });

		});
	},


	openGraph(req, res, err) {

		request(req.body.url, function (error, response, body) {

			if (error)
				res.send(error);

			const $ = cheerio.load(body);
			var RESULT = {
				title: $('meta[property="og:title"]').attr('content'),
				desc: $('meta[property="og:description"]').attr('content'),
				img: $('meta[property="og:image"]').attr('content'),
				url: $('meta[property="og:url"]').attr('content')
			}

			res.send({ data: RESULT, status: response.statusCode });

		});
	}

}

const Class = REQUEST;

module.exports = REQUEST;
