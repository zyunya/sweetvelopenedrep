var nodemailer = require('nodemailer');
var config = require('../config/config');
var fs = require('fs');
var crypto = require('crypto');
var ObjectId = require('mongodb').ObjectID;
var env = require('../config/env')[process.env.NODE_ENV];
const HELPER = require('../helpers');
const postmodel = require('../models/post').Post;
const commentsmodel = require('../models/comments').Comments;
var multer = require('multer');
var path = require('path');
var jwt = require('jsonwebtoken');
var moment = require('moment-timezone');
var striptags = require('striptags');
var sanitizeHtml = require('sanitize-html');


//MULTER OPTIONS


var upload = multer({ storage: config.multerStorage, limits: { fileSize: config.imgMaxSize }, fileFilter: config.multerfileFilter }).array('img', config.allowToUpload); //IMAGES ALLOWED TO UPLOAD(NUMBER)

const postCtl = {


	createPost(req, res, err) {

		upload(req, res, (err) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: HELPER.handleMulterErrors(err) });

			//if (HELPER.superUser(HELPER.fetchPayload(req).role)) req.body.active = 1;//IF I AM A UPERUSER MY POST WILL BE AUTOACTIVATED
			req.body.image = HELPER.getImgArray(req.files);
			req.body.owner = jwt.decode(req.headers['authorization']).id;
			req.body.lang = req.params.lang;

			new postmodel(req.body).save((err, inserted) => {

				if (err)
					return res.send({ 'statusText': err.code });

				else
					res.status(200).send({ status: 200, statusText: 'postcreated', insertedId: inserted });

			})
		});

	},


	fetchPost(req, res, err) {

		//FILTERS//
		var query = {};
		query._id = ObjectId(req.params.id);
		if(!HELPER.superUserOk(req)) query.owner = HELPER.fetchPayload(req).id;

		//QUERY//
		postmodel.findOne( query , function (err, item) {

			if (err)
				return res.status(400).send({ status: 400, statusText: err, text: "Error" });

			if (!item)
				return res.status(400).send({ status: 400, data : [], statusText: "POST NOT EXISTS OR IT NOT YOURS" });

			else
				item.image = HELPER.imgAbs(item.image),
				res.status(200).send({ status: 200, data: item });

		}).populate('owner');

	},



	fetchPosts(req, res, err) {


		//PARAMS//
		var query = {};
		var SKIP = parseInt(req.params.skip);
		var LIMIT = parseInt(req.params.limit);


		//FILTERS//
		if (!HELPER.fetchAll(req.params.category)) query.category = req.params.category;
		if (!HELPER.fetchAll(req.params.active)) query.active = req.params.active;
		query.lang = req.params.lang;
		query.owner = ObjectId(HELPER.fetchPayload(req).id);


		//QUERY//
		postmodel.find(query, function (err, news) {

		// JOIN PARAMETER IS DEFINED IN NEW MODEL AS REF
		}).skip(SKIP).limit(LIMIT).sort({ priority: -1, created_at: -1 }).populate('owner').exec((err, events) => {

			postmodel.count(query).exec((err, count) => {

				if (err)
					return res.status(400).send({ status: 400, statusText: err, text: "Error" });

				if (!events)
					return res.status(400).send({ status: 400, statusText: "NO NEWS" });

				else
					events.filter((el) => { return el.image = HELPER.imgAbs(el.image) });


				try {events.filter((el) => { return el.owner.pokerfaceformatted = HELPER.imgAbs(el.owner.pokerface) }) }


				catch (err) { console.log("PROPERTY OWNER UNDEFINED") }


				return res.status(200).send({ status: 200, data: events, count: count });

			})

		})

	},


	fetchPostsSuperUser(req, res, err) {


		//PARAMS//
		var query = {};
		var SKIP = parseInt(req.params.skip);
		var LIMIT = parseInt(req.params.limit);


		//FILTERS//
		if (!HELPER.fetchAll(req.params.active)) query.active = req.params.active;
		query.lang = req.params.lang;
		query.owner = { $ne : ObjectId(HELPER.fetchPayload(req).id) }


		//QUERY//
		postmodel.find(query, function (err, news) {

		// JOIN PARAMETER IS DEFINED IN NEW MODEL AS REF
		}).skip(SKIP).limit(LIMIT).sort({ priority: -1, created_at: -1 }).populate('owner').exec((err, events) => {

			postmodel.count(query).exec((err, count) => {

				if (err)
					return res.status(400).send({ status: 400, statusText: err, text: "Error" });

				if (!events)
					return res.status(400).send({ status: 400, statusText: "NO NEWS" });

				else
					events.filter((el) => { return el.image = HELPER.imgAbs(el.image) });


				try {events.filter((el) => { return el.owner.pokerfaceformatted = HELPER.imgAbs(el.owner.pokerface) }) }


				catch (err) { console.log("PROPERTY OWNER UNDEFINED") }


				return res.status(200).send({ status: 200, data: events, count: count });

			})

		})

	},



	updatePost(req, res, err) {

		//DATA//
		var data = {

			title: req.body.title,
			desc: HELPER.secureHtml(req.body.desc),
			category: req.body.category,
			lang: req.params.lang,
			anonym: req.body.anonym,
			comments: req.body.comments

		}

		//FILTERS//
		var query = {};
		query._id  = ObjectId(req.params.id);
		if(!HELPER.superUserOk(req)) query.owner = HELPER.fetchPayload(req).id;

		//QUERY//
		postmodel.findOneAndUpdate( query ,{ $set: data }, { new: true }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: err.code });

			if(!updated)
				return res.status(400).send({ status: 400, statusText: "NO SUCH POST" });

			else
				return res.status(200).send({ status: 200, statusText: 'updated', Updated: updated });

		})

	},


	setPriority(req, res, err) {

		var priority = req.body.priority;

		postmodel.findByIdAndUpdate(req.params.id, { $set: { priority: priority } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},


	setAnonym(req, res, err) {

		postmodel.findByIdAndUpdate(req.params.id, { $set: { anonym: req.body.anonym } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},


	setApprove(req, res, err) {

		postmodel.findByIdAndUpdate(req.params.id, { $set: { approve: req.body.approve } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'Top rate changed' });

		})

	},


	setProduction(req, res, err) {

		postmodel.findByIdAndUpdate(req.params.id, { $set: { production: req.body.production, productiontimestamp: new Date() } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},


	setGallery(req, res, err) {


		postmodel.findByIdAndUpdate(req.params.id, { $set: { gallery: req.body.gallery } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},


	setComments(req, res, err) {

		postmodel.findByIdAndUpdate(req.params.id, { $set: { comments: req.body.comments } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},


	activationPost(req, res, err) {

		postmodel.findByIdAndUpdate(req.params.id, { $set: { active: req.body.status } }, { new: true }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 200, 'statusText': err });

			else
				return res.status(200).send({ status: 200, statusText: 'updated', Updated: updated });

		})

	},


	uploadPostImages(req, res, err) {

		upload(req, res, err => {

			if (err)
				return res.status(400).send({ status: 400, statusText: HELPER.handleMulterErrors(err).trim(), allowednumber: config.allowToUpload, allowedsize: config.imgMaxSizeString });

			req.body.image = HELPER.getImgArray(req.files);

			postmodel.findByIdAndUpdate(req.params.id, { $push: { image: { $each: req.body.image } } }, (err) => {

				if (err)
					return res.send({ 'statusText': err.code });

				else
					res.status(200).send({ status: 200, statusText: 'uploaded' });
			})
		})

	},


	setMainImage(req, res, err) {

		var imgCut = req.body.path.split('/')[3];
		var id = req.params.id;

		postmodel.findByIdAndUpdate(id, { $pull: { image: imgCut } }).exec((err, event) => {

			postmodel.findByIdAndUpdate(id, { $push: { image: { $each: [imgCut], $position: 0 } } }, { new: true }, (err, ev) => {

				if (err)
					return res.send({ 'statusText': err });

				else
					res.status(200).send({ status: 200, statusText: 'updated' });
			})
		})
	},


	removePostImage(req, res, err) {

		var imgCut = req.body.path.split('/').slice(-1)[0];
		var img = `${config.postsImgDir}/${imgCut}`;

		postmodel.findByIdAndUpdate(req.params.id, { $pull: { image: imgCut } }, (err) => {

			if (err)
				return res.status(400).send({ 'statusText': err.code });

			else
				fs.exists(img, function (exists) {
					if (exists) {

						try { fs.unlinkSync(img) }

						catch (unlinkerr) { console.log(unlinkerr.Error) }

					}
					else {
						console.log('File not found');
					}

				});

			return res.status(200).send({ status: 200, statusText: 'deleted' });
		})

	},


	removePost(req, res, err) {

		var query = {};
		query._id = ObjectId(req.params.postid);
		if (!HELPER.superUser(HELPER.fetchPayload(req).role)) query.owner = ObjectId(HELPER.fetchPayload(req).id)

		postmodel.findOneAndRemove( query )

			.then( (data) => {


				if(!data)
					return res.status(400).send({ status: 400, statusText: 'postnotfounded' });


				else{

					Class.deleteImgFilesArray(data.image, config.postsImgDir);
					Class.removeComments(data._id);
					res.status(200).send({ status: 200, statusText: "deleted", id: data._id });

				}


			})

			.catch( (err) => {
				return res.status(400).send({ status: 400, statusText: err });
			})


	},



	removeComments(commentbox) {

		commentsmodel.remove({ commentbox: { $in : commentbox }},(err)=>{
			if(err)
				console.log("ERROR",err);
		});

	},



	createPostWithOneImage(req, res, encoding) {

		var imgPath = `${config.postsImgDir}/${HELPER.hashGen()}.${req.files.img.type.split('/')[1]}`;
		var imgName = req.fields.image = imgPath.split('/')[2];

		var post = new postmodel(req.fields)

		fs.rename(req.files.img.path, imgPath, function (err) {

			if (err)
				res.status(400).send({ statusText: err });

			post.save(function (err, inserted) {

				if (err)
					res.status(400).send({ status: 400, statusText: err, text: "Fill All Fields" });

				else
					res.status(200).send({ status: 200, statusText: 'Success', insertedId: inserted });
			});

		});
	},


	createPostAndPopulate(req, res, err) {

		upload(req, res, (err) => {

			req.body.image = HELPER.getImgArray(req.files);
			req.body.owner = jwt.decode(req.headers['authorization']).id;

			if (err)
				return res.send({ 'statusText': err.code });

			Class.Table(req).save((err, inserted) => {

				if (err)
					console.log(err),
					res.status(400).send({ status: 400, statusText: err, text: "Fill All Fields" });

				else
					Class.Table(req).populate('owner', (err, data) => {

						if (err)
							res.status(400).send({ status: 400, statusText: err });

						else
							inserted.owner = data.owner;
						data.image = HELPER.imgAbs(data.image),
							res.status(200).send({ status: 200, statusText: 'Success', insertedId: inserted });

					})

			});

		});

	},


	deleteImgFilesArray(imgArray, imgDir) {

		try {

			imgArray.forEach((img) => {

				img = `${imgDir}/${img}`;

				fs.exists(img, function (exists) {

					if (exists)
						fs.unlinkSync(img),
							console.log('success image file deleted')

					else
						console.log('File not found');

				});

			});

		}
		catch (errdelete) { console.log(errdelete) };
	}


}
var Class = postCtl;
module.exports = postCtl;