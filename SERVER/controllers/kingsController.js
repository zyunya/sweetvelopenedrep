var nodemailer = require('nodemailer');
var config = require('../config/config');
const kingmodel = require('../models/kings').Kings;
var ObjectId = require('mongodb').ObjectID;
var emailValidator = require("email-validator");
var bcrypt = require('bcrypt');
var path = require('path');
var HELPER = require("../helpers");
var crypto = require('crypto');
var jwt = require('jsonwebtoken');
var fs = require("fs");
__parentDir = path.dirname(module.parent.filename);
var env = require('../config/env')[process.env.NODE_ENV];

//GENERATE FILE WITH SECRET KEY(BUFFER) FRO JWT
//crypto.randomBytes(48, function(err, buffer) {
// var secret = buffer.toString('hex');
//fs.writeFile('secret_key.pub', secret, function (err) {
// if (err) throw err;
//console.log('Saved!');
// });
//});


const kingsCtl = {


	fetchProfile(req, res) {

		kingmodel
			.findOne({ _id: Class.idByToken(req.headers.authorization) })
			.populate("tps")
			.populate("style")
			.exec((err, kingprofile) => {

				if(kingprofile.active === 0)
					return res.status(401).send({ status: 401, statusText: "accountblocked" });

				if (err)
					return res.status(400).send({ status: 400, statusText: err, text: "Error" });

				if (!kingprofile)
					return res.status(400).send({ status: 400, statusText: "profilenotexist" });

				else {

					kingprofile.pokerface = HELPER.imgAbs(kingprofile.pokerface);
					return res.status(200).send({ status: 200, data: kingprofile });

				}

			});

	},


	editProfile(req, res) {

		req.body.login =  req.body.login.toLowerCase();
		var ID = Class.idByToken(req.headers.authorization);
		try { delete req.body.pokerface } catch (err) { "property not exists" };

		Class.checkLoginNotMine(req.body.login, ID).then((data) => {

			kingmodel.findByIdAndUpdate(ID, { $set: req.body } ).exec((err, event) => {

				if (err)
					return res.status(400).send({ 'statusText': err });

				else
					res.status(200).send({ status: 200, statusText: 'profileedited' });

			})

		}).catch(err => res.status(200).send({ 'statusText': err }));

	},


	resetPassword(req, res) {

		var ID = Class.idByToken(req.headers.authorization);
		var passData = req.body;

		/* CHECK IF FORM NEW PASSWORDS ARE EQUAL */

		if (passData.password !== passData.repeatpassword)
			return res.status(400).send({ statusText: "passwordsnotequal" });

		/* CHECK CURRENT PASSWORD AND DATABASE PASSWORD ARE EQUAL */

		Class.checkPasswordById(ID, passData.currentpassword).then((data) => {

			kingmodel.findByIdAndUpdate(ID, { password: bcrypt.hashSync(passData.password, 10) }).exec((err, event) => {

				if (err)
					return res.status(400).send({ 'statusText': err });

				else
					res.status(200).send({ status: 200, statusText: 'passwordchanged' });

			})

		}).catch(err => res.status(400).send({ 'statusText': err }));

	},


	resetAvatar(req, res, next) {

		var ID = Class.idByToken(req.headers.authorization);
		var imgPath = `${config.avatarsDir}/${HELPER.hashGen()}.${req.files.img.type.split('/').slice(-1)[0]}`;
		var imgName = req.fields.image = imgPath.split('/').slice(-1)[0];
		var oldImg = path.join(`${config.avatarsDir}/${req.fields.oldimg.split('/').slice(-1)[0]}`);

		Class.uploadOneImg(req.files, imgPath).then(data => {

			kingmodel.findByIdAndUpdate(ID, { $set: { pokerface: imgName } }, (err, event) => {

				if (err)
					return res.status(400).send({ 'statusText': err });

				else{
					if (req.fields.oldimg) Class.deleteImgFile(oldImg);
					res.status(200).send({ status: 200, statusText: data });
				}

			})

		}).catch(err => res.status(400).send({ 'statusText': err }))

	},



	usersList( req , res ){

		var  LIMIT  = parseInt(req.params.limit);
		var  MYID  = HELPER.fetchPayload(req).id;

		kingmodel.find( { _id: { $nin: MYID } , role : { $ne : "superuser" } } , 'login role created_at pokerface active about typo', function(err, users) {

			if(err) res.status(400).send({ status: 200, statusText: err });

			if(users){
				users.filter((el) => { return el.pokerfaceformatted = HELPER.imgAbs(el.pokerface) });
				res.status(200).send({ status: 200, statusText: "ok" , data : users });
			}

			else
				res.status(200).send({ status: 200, statusText: "ok" , data : [] });

		}).limit(LIMIT).sort({ created_at: -1 }).populate({ path: 'tps' });

	},



	usersListByName( req , res ){

		var  LIMIT  = parseInt(req.params.limit);
		var  MYID  = HELPER.fetchPayload(req).id;
		var  NAME  = req.params.name;

		kingmodel.find( { _id: { $ne: MYID } , role : { $ne : "superuser" } , login : { $regex : NAME ,  $options: '-i' }} , 'login role created_at pokerface active about typo', function(err, users) {

			if(err) res.status(400).send({ status: 200, statusText: err });

			if(users){

				users.filter((el) => { return el.pokerfaceformatted = HELPER.imgAbs(el.pokerface) });
				res.status(200).send({ status: 200, statusText: "ok" , data : users });

			}

			else
				res.status(200).send({ status: 200, statusText: "ok" , data : [] });

		}).limit(LIMIT).sort({ created_at: -1 }).populate({ path: 'tps' });

	},




	setUserActive(req, res, err) {

		kingmodel.findByIdAndUpdate(req.params.id, { $set: { active: req.body.active } }, (err, updated) => {

			if (err)
				return res.status(400).send({ status: 400, 'statusText': err.code });

			else
				return res.status(200).send({ status: 200, statusText: 'updated' });

		})

	},



	deleteAccount(req, res, err) {

		kingmodel.findByIdAndRemove(req.params.id, { $not: { role : "superuser" } }, (err, user) => {

			if (err)
				return res.status(400).send({ status: 400, statusText: err });

			if(user){

				Class.deleteImgFile(`${config.avatarsDir}/${user.pokerface}`);
				return res.status(200).send({ status: 200, statusText: 'profiledeleted' });

			}

		})

	},




	checkLoginNotMine(Login, ID) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ _id: { $ne: ObjectId(ID) }, 'login': Login  }, (err, login) => {

				if (err)
					reject(err);

				if (login)
					reject('userexists');

				else
					resolve(true);

			})

		})

	},



	checkPasswordById(ID, PASSWORD) {

		return new Promise((resolve, reject) => {

			kingmodel.findOne({ _id: ObjectId(ID) }, 'password', function (err, user) {

				if (err)
					reject(err)

				if (user)

					if (bcrypt.compareSync(PASSWORD, user.password))
						resolve(true)

					else
						reject("wrongpassword")

			})
		})

	},



	deleteImgFile(IMG) {

		if (IMG !== "") {

			fs.exists(IMG, function (exists) {

				if (exists){

					try { fs.unlinkSync(IMG); }

					catch (err) { console.log(err); }

				}

				else
					console.log('File not found', IMG);

			});
		}

	},


	uploadOneImg(SOURCE, DEST) {

		return new Promise((resolve, reject) => {

			if (HELPER.imageValidation(SOURCE) !== true)
				reject(HELPER.imageValidation(SOURCE));

			else {

				fs.rename(SOURCE.img.path, DEST, (err) => {

					if (err)
						reject(err);

					else
						resolve("downloaded");

				});

			}

		});

	},


	toObjectId(arr) {

		return arr.map(el => {
			return ObjectId(el)
		});

	},


	idByToken(token) {
		return ObjectId(jwt.decode(token).id);
	},


	imgPathAbs(arr) {

		return arr.filter(el => {
			return el.img = env.IMG_PATH + el.img
		})

	},


	updateSignUpKey(req, res) {

		var newKey = bcrypt.hashSync(new Date().toString(), 10);
		var password = req.body.password || null;

		if (password === config.keyUpdatePass) {

			fs.writeFile(config.signupKey, newKey, (err) => {

				if (err)
					return res.status(400).send({ statusText : "Error Occured" , error : err });

				else
					return res.status(200).send({ statusText : "Key Successfully Updated" ,signupkey : newKey });

			});

		}

		else
			return res.status(400).send({ statusText : "Wrong Password"  });

	},


	getCurrentSignUpKey(req, res) {
		var key = fs.readFileSync(config.signupKey).toString().trim();
		res.status(200).json({ signupkey : key });
	}

}



var Class = kingsCtl;

module.exports = kingsCtl;