var multer = require('multer');
var path = require('path');
var imgAllowedSize = 3048;
var allowedFormats = ["image/jpg", "image/JPEG", "image/jpeg" , "image/png" , "image/gif" , "image/GIF"];
var allowToUpload = 3;

const config = {

	/* SSL PATHS TO CERTIFICATES FOR HTTPS */
	SSL : {

		//CERT : '/etc/letsencrypt/live/chakirberg.com/fullchain.pem',
		//KEY  : '/etc/letsencrypt/live/chakirberg.com/privkey.pem'

		CERT : '/etc/letsencrypt/live/sweetvel.com/fullchain.pem',
		KEY  : '/etc/letsencrypt/live/sweetvel.com/privkey.pem'

	},

	/* SMTP NODE MAILER CONFIG */
	SMTP : {

		host: 'mail.sweetvel.com',
		port: 25,
		secure: false, // true for 465, false for other ports
		auth: {
				user: 'SECRET',
				pass: 'SECRET'// generated ethereal password
			},

		tls: {  rejectUnauthorized: false  }

	},

	adminEmail : "hello@sweetvel.com",
	keyUpdatePass : 'SECRET',
	TokenExp: 60 * 60 * 24 * 365,   //  ONE YEAR

	/*    CORE DIRS   */
	postsImgDir: './images/posts',
	avatarsDir: './images/avatars',
	commonimgDir : './images/common',
	keysDir   : './keys',

	/* ALLOWED ORIGINS */
	allowedOrigins : [

		'https://sweetvel.com',
		,'https://chakirberg.com',
		'http://localhost:4200',
		'http://192.168.0.109:4200',

	],

	/*    KEYS      */
	secretKey : "./keys/secret_key.key",
	signupKey : "./keys/signupkey.key",

	/* FILE UPLOAD SETTIGS */
	allowToUpload: allowToUpload,
	imgMaxSize: imgAllowedSize * 1000,  //IN KB
	imgMaxSizeString: `${imgAllowedSize} KB`,
	allowedImgFormats: allowedFormats,


	/* MULTER SETTINGS */
	multerStorage: multer.diskStorage({

		destination: function (req, file, cb) {
			cb(null, config.postsImgDir)
		},

		filename: function (req, file, cb) {
			cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
		},

	}),

	multerfileFilter: function (req, file, cb) {

		if (!allowedFormats.includes(file.mimetype)) {
			cb("wrongfileformat", false);
		} else {
			cb(null, true);
		}

	},


}

module.exports = config;
