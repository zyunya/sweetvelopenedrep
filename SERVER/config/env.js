/*   DEV ENV VARIABLES        */
var HOST_DEV      = "http://localhost:4200";
var HOST_API_DEV    = "http://192.168.0.109:4422/"; //SLASH IN THE END IS ALREADY DEFINED
var BROKEN_IMG_DEV = "http://localhost:4200/assets/img/chlogo2.png";
var IMG_PATH_DEV = "http://192.168.0.109:4422/"


/*   PROD ENV VARIABLES        */
var HOST_PROD   = "https://sweetvel.com";
var HOST_API_PROD = "https://media.sweetvel.com/"; //SLASH IN THE END IS ALREADY DEFINED
var IMG_PATH_PROD = "https://sweetvel.com/";  //SLASH IN THE END ALREADY DEFINED
var BROKEN_IMG_PROD = "https://sweetvel.com/assets/img/chlogo2.png";

var API_PREFIX  = 'api/v1';


const ENV = {

	dev : {

		DB_CONNECTION : "mongodb://localhost:27017/krabsburger",
		API_PREFIX : API_PREFIX,
		API_PORT : 4422,
		HOST : HOST_DEV,
		HOST_API : `${HOST_API_DEV}`,
		IMG_PATH : `${IMG_PATH_DEV}`,
		BROKEN_IMG : BROKEN_IMG_DEV,
		FINISH_SIGN_UP_PAGE :  `${HOST_DEV}/space/page/finishsignup`,

},

	prod : {

		DB_CONNECTION : "mongodb://SECRET:SECRET@localhost:27017/krabsburger",
		API_PREFIX : API_PREFIX,
		API_PORT : 4422,
		HOST :HOST_PROD,
		HOST_API : `${HOST_API_PROD}`,
		IMG_PATH : `${IMG_PATH_PROD}`,
		BROKEN_IMG : BROKEN_IMG_PROD,
		FINISH_SIGN_UP_PAGE :  `${HOST_PROD}/space/page/finishsignup`,

	}

}

module.exports = ENV;
