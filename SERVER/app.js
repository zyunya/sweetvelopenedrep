
var express = require('express');
var app = express();
var server = require('http').Server(app);
var cluster = require('cluster');
var sticky = require('sticky-session');
var numCPUs = require('os').cpus().length;
var HLSServer = require('hls-server');
const redis = require('socket.io-redis');
const sockets = require('socket.io')


var bodyParser = require('body-parser');
var mongoSanitize = require("express-mongo-sanitize");
var https = require('https');
var routes = require('./routes/index');
var env = require('./config/env')[process.env.NODE_ENV];
var config = require("./config/config");
var port = process.env.PORT || env.API_PORT;
var fs = require('fs');
var mongoose = require("mongoose");
var path = require('path');
mongoose.Promise = global.Promise;

/* MIDDLEWARES */
var corsOpt = require('./middleware/cors');
const SuperAdminMW = require('./middleware/superadmin');
const tokenMiddleWare = require('./middleware/token')

/* CONNECT DV */
mongoose.connect(env.DB_CONNECTION, { useMongoClient: true });

/* ENABLE CORS */
app.use(function (req, res, next) { corsOpt.corsOpt(req, res, next, config.allowedOrigins); })

/*ENABLE ERROR STATUS RESPONSE IN JSON FORMAT */
app.options('/*', function (req, res) { res.json({}) });

/* BODY PARSER MIDDLEWARE*/
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

/*PREVENT NOSQL INJECTIONS */
app.use(mongoSanitize());


/* SERVE STATIC IMAGES */
app.use('/', express.static(path.join(__dirname, config.postsImgDir)));
app.use('/', express.static(path.join(__dirname, config.avatarsDir)));
app.use('/', express.static(path.join(__dirname, config.commonimgDir)));


/* PREVENT UNAUTHORIZED ACCESS  TO ALL DASHBOARD APIS*/
app.use(`/${env.API_PREFIX}/dash`, function (req, res, next) { tokenMiddleWare.checkTOkenMiddleWaretoken(req, res, next); });
/* BLOCK USERMEDIA ACTIONS FOR UNAUTHORIZED USERS */
app.use(`/${env.API_PREFIX}/usermedia/actions`, function (req, res, next) { tokenMiddleWare.checkTOkenMiddleWaretoken(req, res, next); });
/* ALLOW ACESS  ONLY FOR SUPERADMIN*/
app.use(`/${env.API_PREFIX}/supermode`, function (req, res, next) { tokenMiddleWare.checkTOkenMiddleWaretoken(req, res, next); })
app.use(`/${env.API_PREFIX}/supermode`, function (req, res, next) { SuperAdminMW.checkSuperAdmin(req, res, next); })
/* EXPRESS ROUTER*/
app.use(`/${env.API_PREFIX}`, routes);


/* RUN SERVER IN DEV MODE */
if (process.env.NODE_ENV == "dev") {

	//PURE CLUSTER MODE
	if (cluster.isMaster) for (var i = 0; i < 1 /* < numCPUs */; i++) cluster.fork();

	else server.listen(port, function () { console.log(`LISTENING TO ${port}-${process.env.NODE_ENV}`); });;


	//CLUSTER MODE WITH STICKY SESSION FOR SOCKET IO
	/*
	if (!sticky.listen(server, port)) server.once('listening', () => console.log(`LISTENING TO ${port}-${process.env.NODE_ENV}`));

	else console.log('Worker ' + cluster.worker.id + ' running and listening on ' + port);
	*/
}


/* RUN SERVER IN PROD MODE USE PM2 CLUSTER MODE*/
if (process.env.NODE_ENV == "prod") {

	//SSL OPTIONS FOR HTTPS
	let options = { cert: fs.readFileSync(config.SSL.CERT), key: fs.readFileSync(config.SSL.KEY) };

	//CREATE HTTPS SERVER
	var server = https.createServer( options , app);

	server.listen(port, function () { console.log(`LISTENING TO ${port}-${process.env.NODE_ENV}`); });;

}


/* SOCKET */
const io = sockets(server, { transports: ['websocket', 'polling'] })
var Socket = require('./socket/index')(io);


// INSTALL REDIS IF NEEDED USE IT (apt-get install redis-server)
//io.adapter(redis({ host: 'localhost', port: 6379 }))
// HLS SERVER var hls = new HLSServer(server, {  path: '/streams', dir: './videos'  })







