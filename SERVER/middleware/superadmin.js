var jwt = require('jsonwebtoken');
const HELPER = require('../helpers');

module.exports = {

	checkSuperAdmin(req, res, next) {

		if (req.headers.authorization) {

			if (!HELPER.superUser(HELPER.fetchPayload(req).role))
				return res.status(401).send({ status: 401, statusText: 'This Operation Allowed only for SuperAdmin' });

			else
				next();
		}

		else {
			return res.status(401).send({ status: 401, statusText: 'NO TOKEN PROVIDED' });
		}

	}

}