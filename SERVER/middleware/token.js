var jwt = require('jsonwebtoken');
var fs = require("fs");
var path = require('path');
var secretKey = './keys/secret_key.key';

module.exports = {

	checkTOkenMiddleWaretoken(req, res, next) {

		var cert = fs.readFileSync(secretKey);
		var token = req.headers['authorization']  ;

		jwt.verify(token, cert, function (err, decoded) {
			if (err)
				return  res.status(401).send({ status: 401, statusText: 'Failed to authenticate token.' ,error : err});
			else
				next();
		});
	},
}