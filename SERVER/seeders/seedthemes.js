var seeder = require('mongoose-seed');
var env = require('../config/env')[process.env.NODE_ENV];

seeder.connect(env.DB_CONNECTION, { useMongoClient: true }, function () {

	seeder.loadModels(['../models/themes.js',]);

		seeder.clearModels(['themes'], function() {

			seeder.populateModels(data, function () {
				seeder.disconnect();
			});

		})

});

var data = [

	{
		'model': 'themes',
		'documents': [

			{
				number : 0,
				name  : "default",
				primarycolor : "white",
				secondarycolor: "#a6df36",
				altcolor : "#555"

			},

			{
				number : 1,
				name  : "dark",
				primarycolor : "#555",
				secondarycolor: "gold",
				altcolor : "white"

			},

			{
				number : 2,
				name  : "green",
				primarycolor : "yellowgreen",
				secondarycolor: "white",
				altcolor : "#555"

			},

		]

	}
];