var seeder = require('mongoose-seed');
var env = require('../config/env')[process.env.NODE_ENV];
var bcrypt = require('bcrypt');
const kingmodel = require('../models/kings').Kings;
var king = "sweetvel";

seeder.connect(env.DB_CONNECTION, { useMongoClient: true }, function () {


	seeder.loadModels(['../models/kings.js',]);

	kingmodel.findOne({ 'login': king }, function (err, kingprofile) {

		if (err)
			console.log(err);

		if (!kingprofile) {
			seeder.populateModels(data, function () {
				console.log(process.argv);
				console.log("USER CREATED");
				seeder.disconnect();
			});
		}

		else {
			console.log(process.argv);
			console.log("USER EXISTS YET");
			seeder.disconnect();
		}
	})

});

var data = [
	{
		'model': 'kings',
		'documents': [
			{
				'login': king ,
				'password': bcrypt.hashSync("Media$matrix&16", 10),
				'role': 'superuser'
			}
		]
	}
];