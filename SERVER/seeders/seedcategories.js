var seeder = require('mongoose-seed');
var env = require('../config/env')[process.env.NODE_ENV];

seeder.connect(env.DB_CONNECTION, { useMongoClient: true }, function () {

	seeder.loadModels(['../models/categories.js',]);

		seeder.clearModels(['categories'], function() {

			seeder.populateModels(data, function () {
				seeder.disconnect();
			});

		})

});

var data = [

	{
		'model': 'categories',
		'documents': [

			{name  : "IT",number : 0},
			{name  : "life",number : 1},
			{name  : "psychology",number : 2},
			{name  : "trash",number : 3},
			{name  : "interestingpeople",number : 4},
			{name  : "reveals",number : 5},
			{name  : "incrediblefacts",number : 6},
			{name  : "lifehacks",number :7},
			{name  : "sixsense",number :8},
			{name  : "futuretoday",number :9},
			{name  : "somepolitics",number :10},
			{name  : "crypto",number :11},
			{name  : "tech",number :12},
			{name  : "humor",number :13},
			{name  : "sport",number :14},
			{name  : "fashion",number :15},
			{name  : "presentations",number :16},
			{name  : "travel",number :17},
			{name  : "music",number :18},
			{name  : "movies",number :19},
			{name  : "art",number :20},
			{name  : "events",number :21},
			{name  : "other",number : 99}

		]

	}
];