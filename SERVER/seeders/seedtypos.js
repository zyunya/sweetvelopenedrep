var seeder = require('mongoose-seed');
var env = require('../config/env')[process.env.NODE_ENV];

seeder.connect(env.DB_CONNECTION, { useMongoClient: true }, function () {

	seeder.loadModels(['../models/typos.js',]);

		seeder.clearModels(['typos'], function() {

			seeder.populateModels(data, function () {
				seeder.disconnect();
			});

		})

});

var data = [

	{
		'model': 'typos',
		'documents': [

			{name  : " ",number : 100},
			{name  : "BLOG",number : 0},
			{name  : "JOURNAL",number : 1},
			{name  : "NEWS",number : 2},
			{name  : "DIARY",number : 3},
			{name  : "OBSERVER",number : 4},
			{name  : "CHANNEL",number : 5},
			{name  : "TV",number : 6},
			{name  : "MEDIA",number : 99},


		]

	}
];