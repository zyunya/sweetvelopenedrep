var middlewares = require('./middleware/middleware');
var livemedia = require('./eventhandlers/livemedia.event');
var superuseractions = require('./eventhandlers/superuser.action');
var HELPERS = require('../helpers');


class Socket {


	constructor(io) {

		/* NAMESPACE FOR GUEST USERS */
		this.guestnamespace = io.of('/guest')

		/* NAMESPACE FOR SIGNED IN USERS */
		this.kingnamespace = io.of('/king')

			.use((socket, next) => middlewares.tokenMiddleWare(socket, next))
			.use((socket, next) => middlewares.testmiddleware(socket, next));

		/* NAMESPACE FOR SUPER USERS */
		this.superusernamespace = io.of('/superuser')

			.use((socket, next) => middlewares.tokenMiddleWare(socket, next))
			.use((socket, next) => middlewares.superUserMiddleWare(socket, next));

		/* HANDLE GUESTS */
		this.guestConnection(io);
		/* HANDLE KINGS */
		this.kingConnection(io);
		/* HANDLE SUPERUSERS */
		this.superUserConnection(io);

	};


	/* GUESTS HANDLER*/
	guestConnection(io) {

		this.guestnamespace.on('connection', (socket) => {

			livemedia.countClients(this.guestnamespace, io.engine.clientsCount);

			/* ALL CONNECTED SOCKETS */
			//this.guestnamespace.clients((error, clients) => console.log(clients));

			socket.on('disconnect', (data) => {

				setTimeout(() => livemedia.countClients(this.guestnamespace, io.engine.clientsCount), 500);

			});

		});

	};


	/* KINGS HANDLER*/
	kingConnection(io) {

		this.kingnamespace.on('connection', (socket) => {

			this.token = socket.handshake.query.token;

			livemedia.handleComments(this.kingnamespace, socket, this.token);

			livemedia.handleDashboardConnection(this.kingnamespace, socket, this.token);

			/* CHECK SOCKETS IN THE ROOM */
			//this.kingnamespace.in(socket.handshake.query.boxid).clients((error, clients) => console.log(clients));

			/* ALL CONNECTED SOCKETS */
			//this.kingnamespace.clients((error, clients) => console.log(clients));

			socket.on('disconnect', (data) => { });

		})

	}


	/* SUPERUSERS HANDLER*/
	superUserConnection(io) {

		this.superusernamespace.on('connection', (socket) => {

			this.tokensp = socket.handshake.query.token;

			superuseractions.handleUsers(this.kingnamespace, socket );

			/* COUNT SUPER USERS CONNECTED TO SOCKET */
			//this.superusernamespace.clients((error, clients) => console.log(clients));

			socket.on('disconnect', (data) => { 	});

		})

	}


}


module.exports = (io) => { return new Socket(io) }