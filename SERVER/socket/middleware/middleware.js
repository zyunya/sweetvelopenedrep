var jwt = require('jsonwebtoken');
var fs = require("fs");
var path = require('path');
var secretKey = './keys/secret_key.key';
var HELPERS = require('../../helpers');
var cert = fs.readFileSync(secretKey);

class Middlewares {


	constructor(){

	}



	tokenMiddleWare(socket, next) {

			jwt.verify(socket.handshake.query.token, cert, function (err, decoded) {
				if (err)
					return next(new Error('authentication error'));
				else
					next();
			});

	};


	superUserMiddleWare(socket, next){

		var userRole = HELPERS.stringTokenPayload(socket.handshake.query.token).role;

		if(userRole === "superuser")
			next();
		else
			return next(new Error('not superuser'));

	}



	testmiddleware(socket, next){
		//console.log("TESTER");
		next();
	}

}


module.exports = new Middlewares();
