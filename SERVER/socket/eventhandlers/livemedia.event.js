var HELPERS = require('../../helpers');
var env = require('../../config/env')[process.env.NODE_ENV];
var ss = require('socket.io-stream');
var fs = require('fs');
var path = require("path");
var ffmpeg = require('fluent-ffmpeg');


class liveMedia {


	countClients(soc, count) {
		soc.emit('clients', count + new Date().getSeconds());
	}


	handleComments(namespace, soc, token) {

		var commentsRoomId;

		soc.on('joincommentroom', (id) => {

			soc.join(id);
			commentsRoomId = id;

		})

		soc.on('comment', (data) => {

			namespace.to(commentsRoomId).emit('comment', data);

			/* SEND TO ALL EXCEPT ME */
			//soc.broadcast.to(boxid).emit('comment', (payload));

		});

	}


	handleDashboardConnection(namespace, soc, token) {

		var dashboardID = HELPERS.stringTokenPayload(token).id;

		//console.log("JOINED" , dashboardID)
		soc.join(dashboardID);


	}


	handleChat(namespace, soc, token, myid) {

		soc.join(myid);

		soc.on('message', (data) => {
			//ACTiONS
		});

	}


	handleVideoStream(namespace, soc, token) {

		ss(soc).on('videostream', (stream, data) => {
			console.log("VIDEOSTREAM")
			var filename = path.basename("video.mp4");
			stream.pipe(fs.createWriteStream(filename, { 'flags': 'a' }));
			this.convertToHLS(filename)

		});

	}


	convertToHLS() { // do something when encoding is done }

		// Below is FFMPEG converting MP4 to HLS with reasonable options.
		// https://www.ffmpeg.org/ffmpeg-formats.html#hls-2
		ffmpeg('./video.mp4', { timeout: 432000 }).addOptions([
			'-profile:v baseline', // baseline profile (level 3.0) for H264 video codec
			'-level 3.0',
			'-s 640x360',          // 640px width, 360px height output video dimensions
			'-start_number 0',     // start the first .ts segment at index 0
			'-hls_time 10',        // 10 second segment duration
			'-hls_list_size 0',    // Maxmimum number of playlist entries (0 means all entries/infinite)
			'-f hls'               // HLS format
		]).output('./videos/output.m3u8').on('end', ()=>{ console.log("VIDEO CONVERTED")}).run()

		}

	}

module.exports = new liveMedia();