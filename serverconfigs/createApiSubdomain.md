Api is loded on sweetvel.com:4422/api/v1
but we want to fetch it by  https://media.sweetvel.com/api/v1

1. Install SSL for subdomain

# sudo certbot --authenticator webroot --webroot-path /var/www/chakirberg/dist --installer apache -d sweetvel.com,media.sweetvel.com --expand

2. Create Apache Vhost and make proxypass to nodejs api 

<!-- 
<VirtualHost *:443>

  SSLProxyEngine on
  SSLProxyVerify none
  SSLProxyCheckPeerCN off
  SSLProxyCheckPeerName off
  SSLProxyCheckPeerExpire off
  ProxyPass /  https://sweetvel.com:4422/
  ServerName media.sweetvel.com

  SSLCertificateFile /etc/letsencrypt/live/sweetvel.com-0001/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/sweetvel.com-0001/privkey.pem
  Include /etc/letsencrypt/options-ssl-apache.conf

</VirtualHost> 
-->

#NOW WE CAN FETCH ALL APIS BY media.sweetvel.com/api/v1
