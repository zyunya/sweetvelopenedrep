const path = require('path');
const nodeExternals = require('webpack-node-externals');

const input = `./dist-server/main.bundle`;
const output = `main.repacker.bundle.js`;
module.exports = {
	entry: input,
	output: {
		path: __dirname + '/dist-server/',
		filename: output,
	},

	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				query: {
					"plugins": ["transform-es2015-modules-commonjs"],
					presets: []
				}
			}
		]
	}

};