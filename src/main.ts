import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

//PROTOTYPE EXTENSIONS
import './app/prototypes';


if (environment.production) {
	enableProdMode();
}


platformBrowserDynamic().bootstrapModule(AppModule).then(() => {

	if('serviceWorker' in navigator && environment.production && !navigator.userAgent.toLowerCase().match("iphone|ipad"))
		navigator.serviceWorker.register('/ngsw-worker.js');

	})

	.catch(err => console.log(err));
