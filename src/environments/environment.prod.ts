// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

var API_URL = "https://media.sweetvel.com";
var API_PREFIX = "api/v1";


export const environment = {


	production: true,
	HOST: "https://sweetvel.com",
	IMG_PATH: "assets/img",
	API_URL : API_URL,
	API: {


		// SIGN OPERATIONS
		SIGN_UP: `${API_URL}/${API_PREFIX}/sign/signup`,
		SIGN_UP_USER: `${API_URL}/${API_PREFIX}/sign/signupuser`,
		SIGN_UP_CONFIRMATION: `${API_URL}/${API_PREFIX}/sign/signupconfirmation`,
		SIGN_IN: `${API_URL}/${API_PREFIX}/sign/signin`,


		// USER OPERATIONS
		FETCH_PROFILE: `${API_URL}/${API_PREFIX}/dash/fetchprofile`,
		EDIT_PROFILE: `${API_URL}/${API_PREFIX}/dash/editprofile`,
		RESET_PASSWORD: `${API_URL}/${API_PREFIX}/dash/resetpassword`,
		RESET_AVATAR: `${API_URL}/${API_PREFIX}/dash/resetavatar`,


		//REUQEST AND HELPERS
		YOUTUBE_FETCH: `${API_URL}/${API_PREFIX}/checkyoutubelink`,


		//USERMEDIA OPERATIONS
		MEDIA_FETCH_POST: `${API_URL}/${API_PREFIX}/usermedia/fetchpost`,
		MEDIA_FETCH_POSTS: `${API_URL}/${API_PREFIX}/usermedia/fetchposts`,
		MEDIA_FETCH_POSTS_SAVED: `${API_URL}/${API_PREFIX}/usermedia/fetchpostssaved`,
		MEDIA_FETCH_RELATED_POSTS: `${API_URL}/${API_PREFIX}/usermedia/fetchrelatedposts`,
		MEDIA_ADD_COMMENT: `${API_URL}/${API_PREFIX}/usermedia/actions/addcomment`,
		MEDIA_REMOVE_COMMENT: `${API_URL}/${API_PREFIX}/usermedia/actions/removecomment`,
		MEDIA_REMOVE_COMMENT_OWNER: `${API_URL}/${API_PREFIX}/usermedia/actions/removecommentowner`,
		MEDIA_FETCH_COMMENTS: `${API_URL}/${API_PREFIX}/usermedia/fetchcomments`,
		MEDIA_FETCH_DESKS: `${API_URL}/${API_PREFIX}/usermedia/fetchdesks`,


		//FOREIGNERMEDIA OPERATIONS
		FOREIGNER_MEDIA_FETCH_POSTS: `${API_URL}/${API_PREFIX}/foreignermedia/fetchposts`,
		FOREIGNER_MEDIA_FETCH_CATEGORIES: `${API_URL}/${API_PREFIX}/foreignermedia/fetchcategories`,


		//STATIC DATA
		STATIC_DATA_CATEGORIES: `${API_URL}/${API_PREFIX}/staticdata/fetchcategories`,
		STATIC_DATA_TYPOS: `${API_URL}/${API_PREFIX}/staticdata/fetchtypos`,
		STATIC_DATA_THEMES: `${API_URL}/${API_PREFIX}/staticdata/fetchthemes`,


		//SUPERMODE OPERATIONS
		KEY_UPDATE: `${API_URL}/${API_PREFIX}/supermode/superadminkeyupdater`,
		KEY_GET: `${API_URL}/${API_PREFIX}/supermode/getsignupkey`,
		USERS_LIST: `${API_URL}/${API_PREFIX}/supermode/userslist`,
		SET_USER_ACTIVE: `${API_URL}/${API_PREFIX}/supermode/setuseractive`,
		DELETE_ACCOUNT: `${API_URL}/${API_PREFIX}/supermode/deleteaccount`,
		FETCH_ALL_POSTS: `${API_URL}/${API_PREFIX}/supermode/fetchposts`,
		SET_PRIORITY: `${API_URL}/${API_PREFIX}/supermode/setpriority`,
		SET_APPROVE: `${API_URL}/${API_PREFIX}/supermode/setapprove`,
		ACTIVATION_POST: `${API_URL}/${API_PREFIX}/supermode/activationpost`,
		SUPERMODE_REMOVE_COMMENT: `${API_URL}/${API_PREFIX}/supermode/removecomment`,


		//POSTS OPERATIONS ADMIN
		FETCH_POSTS: `${API_URL}/${API_PREFIX}/dash/fetchposts`,
		REMOVE_POST: `${API_URL}/${API_PREFIX}/dash/removepost`,
		FETCH_POST: `${API_URL}/${API_PREFIX}/fetchpost`, /* FETCH ONE POST OR NEW*/
		UPDATE_POST: `${API_URL}/${API_PREFIX}/dash/updatepost`, /* UPDATE ONE POST OR NEW*/
		REMOVE_POST_IMG: `${API_URL}/${API_PREFIX}/dash/removepostimage`,
		UPLOAD_POST_IMG: `${API_URL}/${API_PREFIX}/dash/uploadpostimage`,
		SET_MAIN_POST_IMAGE: `${API_URL}/${API_PREFIX}/dash/setmainpostimage`,
		SET_ANONYM: `${API_URL}/${API_PREFIX}/dash/setanonym`,
		SET_GALLERY: `${API_URL}/${API_PREFIX}/dash/setgallery`,
		SET_PRODUCTION: `${API_URL}/${API_PREFIX}/dash/setproduction`,
		SET_COMMENTS: `${API_URL}/${API_PREFIX}/dash/setcomments`,
		CREATE_ESSENCE: `${API_URL}/${API_PREFIX}/dash/createpost`,


		//EXTERNAL APIS
		INSTA_GALLERY: "https://api.instagram.com/v1/users/self/media/recent/?access_token=5444876438.cc1af65.e5ea0fcfc2a141dba541de3fc4712672&COUNT=200",
		INSTA_PROFILE: "https://api.instagram.com/v1/users/self/?access_token=5444876438.cc1af65.e5ea0fcfc2a141dba541de3fc4712672&COUNT=200",
		YOUTUBE_CHANNEL : "https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails,statistics&id=UC-Jyb8ez9FhPTbXpJRPfJgQ&key=AIzaSyD6ESIpc0EUOasTgh7sjePF6b62-ebAwwg",
		YOUTUBE_CHANNEL_PLAYLIST : "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=UU-Jyb8ez9FhPTbXpJRPfJgQ&key=AIzaSyD6ESIpc0EUOasTgh7sjePF6b62-ebAwwg"


	}
}
