import { Component, OnInit, AfterViewInit, Renderer2, ViewContainerRef, ViewEncapsulation, ViewChild, HostListener, ElementRef, PLATFORM_ID, Inject } from '@angular/core';
import { DomSanitizer, DOCUMENT } from '@angular/platform-browser';
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { UsermediaService } from "../services/usermedia.service";
import { Router } from "@angular/router";
import { forkJoin } from "rxjs/observable/forkJoin";
import { MetatagsService } from "../services/metatags.service";
import { LocaldataService } from "../services/localdata.service";
import { DataService } from "../kingModule/services/data.service";
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { routerTransition3 } from "../animations";
import { HelpersService } from "../services/helpers.service";
import { ClipboardService } from "../services/clipboard.service";
import { MatSnackBar } from '@angular/material';
import * as smoothscroll from 'smoothscroll-polyfill';
import { TranslateService } from '@ngx-translate/core';
import { NullTemplateVisitor } from '@angular/compiler';


@Component({

	selector: 'app-postpage',
	templateUrl: './postpage.component.html',
	styleUrls: ['./postpage.component.scss'],
	providers: [UsermediaService, MetatagsService, ClipboardService, LocaldataService],
	encapsulation: ViewEncapsulation.None,
	animations: [routerTransition3()],

})


export class PostpageComponent implements OnInit {

	@ViewChild('navigationButtonsDOM') public navBtnsDOM: ElementRef;
	@ViewChild('mainImageDOM') public mainImageDOM: ElementRef;
	@ViewChild('contedit') public contedit: ElementRef;
	@ViewChild('postheader') public postHeader: ElementRef;
	@ViewChild('postblockhost') public postHostDom: ElementRef;
	@ViewChild('authorcard') public authorCard: ElementRef;

	public Themes: Array<any> = [];
	public currentTheme: number = 0;
	public urlParams: any;
	public postData: any;
	public relatedPosts: any;
	public imageMain: String;
	public errorResponse: Boolean = false;
	public waitPost: Boolean = true;
	public host: any;
	public instaembedTag: any;


	constructor(

		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(DOCUMENT) private document: any,

		private domSanitizer: DomSanitizer,
		private route: ActivatedRoute,
		public MEDIA: UsermediaService,
		public router: Router,
		public META: MetatagsService,
		public DataService: DataService,
		public HELPERS: HelpersService,
		public snackbar: MatSnackBar,
		public location: Location,
		private elementRef: ElementRef,
		public clip: ClipboardService,
		private renderer: Renderer2,
		public translate: TranslateService,
		public LocaldataService: LocaldataService

	) {

		this.route.params.subscribe(params => this.urlParams = params);
		this.Themes = this.DataService.themeDataResolved();
		//RELOAD STATE WHEN URL PARAMS CHANGED

	}


	ngOnInit() {

		/*DONT PUT ANYTHING BEFORE THIS SECTION BECAUSE NGONINIT IS SYNCHRONOUS NOW
		AND IF YOU WILL PUT SOMETHING BEFORE ,OPENGRAPH DATA WILL NOT BE FETCHED
		BECAUSE IT WILL WAIT BEFORE ALL PREVIOUS OPERATIONS WILL BE RESOLVED
		*/

		this.route.params.subscribe(params => {
			/* URL PARAMS LISTENER */
			this.fetchPageInfo();
			this.fetchRelatedPosts();
			this.scrollWindow();
			this.waitPost = true;

		});


		this.host = this.document.location;
		// SMOOTH SCROLL POLYFILL FOR IOS SAFARI SUPPORT
		if (isPlatformBrowser(this.platformId)) smoothscroll.polyfill();


	}


	ngAfterViewInit() {

	}


	ngOnDestroy() {

	}


	@HostListener('window:resize', ['$event'])
	onResize(event) {

	}


	@HostListener('window:scroll', ['$event'])
	onWindowScroll($event) {

		this.navigationButtons();
		this.handleHeader();
		this.handleAuthorCard();

	}



	public fetchPageInfo() {

		this.MEDIA.fetchPost(this.urlParams.id).subscribe(

			res => {

				this.META.addOgMetaTagsStatic(res.data);
				this.META.setTitle(res.data.title);
				this.postData = res.data;
				this.currentTheme = res.data.theme;
				this.imageMain = res.data.image[0];
				this.waitPost = false;
				//console.log(res)

			},

			err => { console.log(err.statusText); this.errorResponse = true; }

		)

	}


	public fetchRelatedPosts() {

		this.MEDIA.fetchRelatedPosts(this.urlParams.id, 6, this.urlParams.category).subscribe(

			res => { this.relatedPosts = res.data; },

			err => { console.log(err) }

		)

	}


	public changeArticle(id): void {
		// FAKED SELF NAVIGATION //
		//this.router.navigate(['forum']).then(()=>{this.router.navigate( ['/ch/'+id] ) })
		this.router.navigate(['/ch/' + id]);
	}

	/*
	public fetchPageInfos() {

		var fetchPost = this.MEDIA.fetchPost(this.urlParams.id);
		var fetchRelatedPosts = this.MEDIA.fetchRelatedPosts(this.urlParams.id, 5, this.urlParams.category);

		forkJoin([fetchPost, fetchRelatedPosts]).subscribe(

			res => {

				this.META.addOgMetaTags(res[0].data);
				this.postData = res[0].data;
				this.currentTheme = res[0].data.theme;
				this.relatedPosts = res[1].data;
				// SET OG METATAGS AND TITLE TAG
				this.META.setTitle(res[0].data.title || "");

			},

			err => console.log(err)

		)

	};
	*/

	public navigationButtons(): void {

		if (isPlatformBrowser(this.platformId)) {

			if (window.pageYOffset > 120)
				this.navBtnsDOM.nativeElement.style.transform = 'translateY(0%)';

			else
				this.navBtnsDOM.nativeElement.style.transform = 'translateY(100%)';

			if (window.pageYOffset > document.body.scrollHeight - 1000)
				this.navBtnsDOM.nativeElement.style.transform = 'translateY(100%)';

		}

	}


	public handleHeader(): void {

		var offsetTrue = this.HELPERS.isMobile() ? window.pageYOffset > 200 : window.pageYOffset > 400;

		this.postHeader.nativeElement.style.background = offsetTrue ? "rgba(255,255,255,0.95)" : "none";

	}


	public scrollBottom(): void {

		if (isPlatformBrowser(this.platformId)) {

			window.scroll({
				top: document.body.scrollHeight,
				left: 0,
				behavior: 'smooth'
			});

		}

	}


	public scrollTop(): void {

		if (isPlatformBrowser(this.platformId)) {

			window.scroll({
				top: 0,
				left: 0,
				behavior: 'smooth'
			});

		}

	}


	public scrollWindow(): void {

		if (isPlatformBrowser(this.platformId)) {

			//window.scroll({top: 0,left: 0,behavior: 'smooth'});
			window.scrollTo(0, 0);

		}

	};


	public openRelPost(ID) {
		this.router.navigate(["/ch", ID, this.urlParams.category]);
		if (isPlatformBrowser(this.platformId)) {
			setTimeout(() => this.fetchPageInfo(), this.fetchRelatedPosts(), this.scrollWindow(), 300);
		}
	}


	public setMainImage(img): void {
		this.imageMain = img;
	}


	public copyLink(): void {
		var loc = this.host;
		this.HELPERS.copyToClipBoard(loc);
		this.translate.get('copied').subscribe((res: string) => {
			this.snackbar.open(this.urlParams.id, res, { duration: 2000 });
		});
	}


	public handleAuthorCard(): void {

		if (isPlatformBrowser(this.platformId)) {

			var card = this.authorCard.nativeElement;
			var windowWidth = window.innerWidth;
			var ElemOffsetY = card.getBoundingClientRect().top <= 80;
			var WindowOffsetY = window.pageYOffset < 715;

			if (!this.HELPERS.isMobile() && ElemOffsetY) {

				card.style.position = "fixed";
				card.style.top = "70px";
			}

			if (WindowOffsetY) {
				card.style.position = "absolute";
				card.style.top = "0";

			}

		}

	}





}
