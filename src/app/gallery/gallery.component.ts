import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';
import { InitfetchService } from '../services/initfetchservice';
import { GlobalenvService } from '../services/globalenv.service';
import { HelpersService } from '../services/helpers.service';
import { RefreshpageService } from '../services/refreshpage.service';
import { forkJoin } from "rxjs/observable/forkJoin";
import { routerTransition} from "../animations";

@Component({
	selector: 'app-gallery',
	templateUrl: './gallery.component.html',
	styleUrls: ['./gallery.component.scss'],
	encapsulation: ViewEncapsulation.None,
	animations: [routerTransition()]
})
export class GalleryComponent implements OnInit {


	public imageInfo: Array<any>;
	public profileInfo: any;


	constructor(
		public InitfetchService: InitfetchService,
		public globalEnv : GlobalenvService,
		private Location: Location,
		public HELPERS : HelpersService,
		public RefreshpageService : RefreshpageService
	) {
		this.imageInfo = [];
		this.profileInfo = {};

		/* LISTEN TO REFRESH PAGE EVENT */
		this.RefreshpageService.postsAndCategoriesRefresh.subscribe(data => this.getInstaInfo() );
		this.HELPERS.scrollUp();

	}

	ngOnInit() {

		//window.scrollTo(0,0);
		this.getInstaInfo();

	}


	public getInstaInfo(): void {

		var instaGallery = this.InitfetchService.fetchGallery();
		var instaProfile  = this.InitfetchService.fetchProfile();

		this.imageInfo = [];
		this.profileInfo = {};

		forkJoin([instaGallery, instaProfile]).subscribe(

			res => {

					//console.log(res);
					this.imageInfo = res[1].data;
					this.profileInfo = res[0].data;

			},

			err => console.log(err)

			);
	}



	public openInstaProfile() : void {
		this.HELPERS.setInstaProfileHref();
	}


	public openInstaMedia(href) : void {
		this.HELPERS.setInstaMediaHref(href);
	}



	public checkArray( arr ) : Boolean{

		if( arr.length > 0 )
			return true;

		else
			return false;

	}

}
