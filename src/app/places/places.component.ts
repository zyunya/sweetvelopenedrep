import { Component, OnInit } from '@angular/core';
import { routerTransition2 } from "../animations";

@Component({
	selector: 'app-places',
	templateUrl: './places.component.html',
	styleUrls: ['./places.component.scss'],
	animations: [routerTransition2()],
})
export class PlacesComponent implements OnInit {

	constructor() { }

	ngOnInit() {
	}

}
