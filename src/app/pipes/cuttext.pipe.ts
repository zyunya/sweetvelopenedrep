import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'cuttext', pure: false })
export class CutTextPipe implements PipeTransform {
	constructor() {
	}

	transform(txt: String, len: any) {
		if (txt) {
			if (txt.length > len)
				return txt.substr(0, len) + "...";

			else return txt;
		}
	}
}