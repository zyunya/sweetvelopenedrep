import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({ name: 'safestyle', pure: false })
export class SafeStylePipe implements PipeTransform {
	constructor(private sanitizer: DomSanitizer) {
	}

	transform(content) {
		return this.sanitizer.bypassSecurityTrustStyle(content);
	}
}