import { Pipe, PipeTransform, Inject, PLATFORM_ID } from '@angular/core';
import { i18nService } from "../services/i18n.service";
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import * as moment from 'moment-timezone';


@Pipe({ name: 'converttime', pure: false })
export class ConvertTimePipe implements PipeTransform {
	constructor(public i18n: i18nService, @Inject(PLATFORM_ID) private platformId: Object, ) {

	}

	transform(time) {

		if (isPlatformBrowser(this.platformId)) {

			var timeZone = 'Europe/Kiev';
			var currentLang = this.i18n.getDefaultLang();
			var formatedDate = moment(time).unix();
			var currentDate = moment().unix();
			var DIFF = 60 * 15; //6 MINUTES

			if ((formatedDate + DIFF) > currentDate)
				return moment(time).tz(timeZone).locale(currentLang).fromNow();

			else
				return moment(time).tz(timeZone).locale(currentLang).format('LL H : mm');

		}

	}

}