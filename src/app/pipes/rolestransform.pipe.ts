import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'rolestransform', pure: false })
export class RolestransformPipe implements PipeTransform {
	constructor() {
	}

	transform(txt: String) {

		if (txt) {
			switch (txt.toLowerCase()) {

				case "superuser": return "Chief Editor";
				case "admin": return "Chief Contributor";
				default: return "Пользователь"

			}
		}

	}

}