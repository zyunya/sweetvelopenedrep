import { NgModule } from '@angular/core';
import { ConnectionGuard } from './services/connection.guard';
import { ConnectionstableGuard } from './services/connectionstable.guard';
import { RouterModule, Routes, Router, NavigationStart ,RouteReuseStrategy } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ForumComponent } from './forum/forum.component';
import { PostsComponent } from './posts/posts.component';
import { GalleryComponent } from './gallery/gallery.component';
import { PostpageComponent } from './postpage/postpage.component';
import { ForeignerComponent } from './foreigner/foreigner.component';
import { PlacesComponent } from './places/places.component';
import { YoutubeComponent } from './youtube/youtube.component';
import { NoconnectionComponent } from './noconnection/noconnection.component';
import { OptionsComponent } from './options/options.component';
import { InfoComponent } from './info/info.component';
import { SavedComponent } from './saved/saved.component';
import { mainRoutes } from './services/globalenv.service';



export const AppRoutes: Routes = [
//AUTH GUARDS BLOCK HTTP REQUESTS AND ALSO OPENGRAPH
	{
		path: '', component: HomeComponent ,

		children: [

			{ path: '', component: PostsComponent }, //TEMPORARY DESABLED , canActivate: [ConnectionGuard]  IN ORDER TO TEST GOOGLE CRAWLERS
			{ path: mainRoutes.nested.gallery, component: GalleryComponent  },
			{ path: mainRoutes.nested.youtube, component: YoutubeComponent  },
			{ path: mainRoutes.nested.places, component: PlacesComponent , canActivate: [ConnectionGuard] },
			{ path: mainRoutes.nested.info, component: InfoComponent  },
			{ path: mainRoutes.nested.options, component: OptionsComponent },
			{ path: mainRoutes.nested.saved, component: SavedComponent  },
			{ path: mainRoutes.nested.networkerror, component: NoconnectionComponent , canActivate : [ConnectionstableGuard] }

		]

	},

	{ path: ':name', component: ForeignerComponent  },
	{ path: ':name/:id', component: PostpageComponent   },

];


@NgModule({

	imports: [RouterModule.forRoot(AppRoutes)],
	exports: [RouterModule],
	providers: [

		ConnectionGuard ,
		ConnectionstableGuard

		],

})



export class AppRoutingModule {

	constructor(public router: Router ) {

	}
}


export const RoutingComponents = [

	HomeComponent,
	ForumComponent,
	PostsComponent,
	GalleryComponent,
	PostpageComponent,
	ForeignerComponent,
	PlacesComponent,
	YoutubeComponent,
	NoconnectionComponent,
	InfoComponent,
	OptionsComponent,
	SavedComponent

];