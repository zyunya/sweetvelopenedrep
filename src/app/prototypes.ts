	//export { } // to make it a module


	/* INTERFACES FOR PROTOTYPES */

	interface Array<T> {
		addArray(arr : Array<any>) : void; // in order to return array use Array<any.
	}


	interface String {
		chuck() : String; // in order to return array use Array<any.
	}


	interface Element {
		distanceToBottom() : number; // in order to return array use Array<any.
	}


	/* EXTENDING PROTOTYPES */
	 // CLASSIC WAY OF ADDING TO PROTOTYPE
	Element.prototype.distanceToBottom = function () {

		return this.getBoundingClientRect().bottom - window.innerHeight ;

	}


	Array.prototype.addArray = function (newArr) {

		this.push.apply(this , newArr);

	}


	// NEW WAY OF ADDING TO PROTOTYPE
	Object.assign(String.prototype, {

			chuck() { return this.toUpperCase() }

	});

