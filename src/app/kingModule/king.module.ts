import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http'
import { AppRoutingModule, RoutingComponents } from './king.routing'
import { MaterialModule } from '../app.material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//COMPONENTS
import { PostshandlerComponent } from './postshandler/postshandler.component';
import { EmojiComponent } from '../sharedcomponents/emoji/emoji.component';
import { SmarteditorComponent } from '../sharedcomponents/smarteditor/smarteditor.component';
import { MobilemenuComponent } from "../sharedcomponents/mobilemenu/mobilemenu.component";
import { SnackbarComponent } from "../sharedcomponents/snackbar/snackbar.component";
import { PaginatorComponent } from "../sharedcomponents/paginator/paginator.component";
import { CommentsComponent } from "../sharedcomponents/comments/comments.component";


/* TRANSLATION MODULES */
import { i18nModule  } from '../i18n.module' ;
import { i18nService } from '../services/i18n.service';

//PIPES
import { SafeStylePipe } from "../pipes/safestyle.pipe";
import { SafeUrlPipe } from "../pipes/safeurl.pipe";
import { CutTextPipe } from "../pipes/cuttext.pipe";
import { ConvertTimePipe } from "../pipes/converttime.pipe";
import { SanitizeHtmlPipe } from "../pipes/sanitize-html.pipe";
import { RemovehtmltagsPipe } from "../pipes/removehtmltags.pipe";
import { RolestransformPipe } from "../pipes/rolestransform.pipe";

//SERVICES
import { HelpersService } from "../services/helpers.service";
import { ClipboardService } from "../services/clipboard.service";
import { DataService } from "./services/data.service";
import { StaticDataService } from "../services/staticdata.service";
import { GlobalenvService  } from '../services/globalenv.service';
import { SignupuserComponent } from './signupuser/signupuser.component';
import { UsershandlerComponent } from './usershandler/usershandler.component';
import { ModalService } from "../modal/modal.service";


@NgModule({

	imports: [
		CommonModule,
		AppRoutingModule,
		MaterialModule,
		FormsModule,
		ReactiveFormsModule,
		i18nModule
	],

	exports : [
		CutTextPipe,
		ConvertTimePipe,
		SanitizeHtmlPipe,
		SafeStylePipe,
		SafeUrlPipe,
		RemovehtmltagsPipe,
		RolestransformPipe,
		MobilemenuComponent,
		PaginatorComponent,
		CommentsComponent
		],

	declarations: [
		RoutingComponents,
		PostshandlerComponent,
		EmojiComponent,
		SafeStylePipe,
		SafeUrlPipe,
		CutTextPipe,
		ConvertTimePipe,
		SanitizeHtmlPipe,
		RemovehtmltagsPipe,
		RolestransformPipe,
		SmarteditorComponent,
		MobilemenuComponent,
		SnackbarComponent,
		PaginatorComponent,
		SignupuserComponent,
		UsershandlerComponent,
		CommentsComponent
		],

	providers: [
		ModalService,
		HelpersService,
		DataService,
		RemovehtmltagsPipe,
		RolestransformPipe,
		ClipboardService,
		StaticDataService,
		GlobalenvService,
		]

})
export class KingModule { }
