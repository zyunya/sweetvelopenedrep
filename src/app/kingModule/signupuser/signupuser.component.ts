import { Component, OnInit } from '@angular/core';
import { SignService } from "../services/signoperations.service";
import { GlobalenvService } from '../../services/globalenv.service';
import { MetatagsService } from "../../services/metatags.service";
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, FormBuilder } from '@angular/forms';
import { routerTransition} from "../../animations";


@Component({
	selector: 'app-signupuser',
	templateUrl: './signupuser.component.html',
	styleUrls: ['./signupuser.component.scss'],
	providers: [SignService , MetatagsService ],
	animations: [routerTransition()]
})


export class SignupuserComponent implements OnInit {


	public statusText: any;
	public Form: any;
	public formData: any = {}


	constructor(

		public SignService: SignService,
		public globalEnv: GlobalenvService,
		public formBuilder: FormBuilder,
		public MetatagsService : MetatagsService,
		public translate : TranslateService,
		public snackbar: MatSnackBar,

	) {

	}


	ngOnInit() {

		this.MetatagsService.setTitle( "Sweetvel | Sign Up" );
		this.Form = new FormGroup({

			login          : new FormControl('' ,[ Validators.required , Validators.minLength(3)]),
			email          : new FormControl('' ,[ Validators.required , Validators.minLength(5)]),
			password       : new FormControl('', [ Validators.required, Validators.minLength(3)] ),
			repeatpassword  : new FormControl('', [ Validators.required, Validators.minLength(3)] ),
			acceptterms     : new FormControl('' ,[Validators.required]),

		});

	}


	public signUpUser(): void {

		this.RefreshFormData();

		if (this.Form.valid) {

			this.SignService.signUpUser(this.formData).subscribe(

				res => { this.statusText = this.translate.instant(res.statusText); this.Form.reset();  /* console.log( res ) */ },

				err => { this.snackbar.open( this.translate.instant(err.statusText) , 'Ok', {duration: 2500})  }

			);

		}

		else
			this.snackbar.open( this.translate.instant('fillallfields') , 'Ok', {duration: 2500});

	}


	public RefreshFormData() : void{
		Object.keys(this.Form.controls).forEach( el => { this.formData[el] = this.Form.controls[el].value })
	}


}
