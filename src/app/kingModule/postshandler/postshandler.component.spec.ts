import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostshandlerComponent } from './postshandler.component';

describe('PostshandlerComponent', () => {
  let component: PostshandlerComponent;
  let fixture: ComponentFixture<PostshandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostshandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostshandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
