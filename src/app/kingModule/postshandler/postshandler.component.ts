import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { DataService } from "../services/data.service";
import { StaticDataService } from "../../services/staticdata.service";
import { CrudService } from "../services/crudoperations.service";
import { MatSelectChange } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from "../../services/auth.service";
import { HelpersService } from "../../services/helpers.service";
import { MatSnackBar } from '@angular/material';
import { kingRoutes } from '../../services/globalenv.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
	selector: 'app-postshandler',
	templateUrl: './postshandler.component.html',
	styleUrls: ['./postshandler.component.scss'],
	providers: [CrudService],
	encapsulation: ViewEncapsulation.None
})


export class PostshandlerComponent implements OnInit {

	@ViewChild('colorPreviewDOM') public colorPreviewElem: ElementRef;
	@ViewChild('inputFileDOM') public inputFileElem: ElementRef;

	public imagePreviewDOM: any;
	public postData: any;
	public themeData: any = this.DataService.themeData();
	public categoryData: any;
	public userData: any;
	public formData: any;
	public postForm: any;
	public statusMsg : String;
	public activeLinkIndex = 0;

	constructor(

		public DataService: DataService,
		public StaticData : StaticDataService,
		public sanitizer: DomSanitizer,
		public CrudService: CrudService,
		private router: Router,
		private AuthService: AuthService,
		public Helpers: HelpersService,
		public snackbar: MatSnackBar,
		public translate : TranslateService

	) {

		this.postData = this.userData = {};
		this.postData.theme = 1;
		this.formData = new FormData();

		this.postForm = new FormGroup({

				title: new FormControl('', [Validators.required]),
				desc: new FormControl('', []),
				category: new FormControl('', [Validators.required]),
				anonym: new FormControl('', [ ]),
				comments: new FormControl('', [ ])
				//type: new FormControl('', [Validators.required]),
				//theme: new FormControl('', [Validators.required])

		});
	}

	async ngOnInit() {

		this.categoryData = await this.StaticData.fetchCategories().toPromise();

	}

	public colorPreview(el): void {
		this.colorPreviewElem.nativeElement.style.backgroundColor = this.themeData[el.value].color;
	}


	public imagePreview(el) {
		var image = el.target.files[0];
		var reader = new FileReader();
		reader.onload = (e) => {
			var dataUrl = reader.result;
			this.imagePreviewDOM = this.sanitizer.bypassSecurityTrustStyle(`url(${dataUrl})`)
		};
		reader.readAsDataURL(el.target.files[0]);
	}


	public setImage2(el): void {
		this.postData.img = el.target.files[0];
	}

	public setImage(el): void {

		//console.log(el.target.files.length)
		for (let i = 0; i < el.target.files.length; i++) {
			this.formData.append("img", el.target.files[i]);
		}
	}


	public createKingWork(): void {

		if (this.postForm.valid) {

			this.Helpers.setTempData(this.postForm.controls);
			Object.keys(this.postForm.controls).forEach((el) => { this.formData.append(el, this.postForm.controls[el].value); });

			this.CrudService.createEssence(this.formData).subscribe(

				res => {

					if(res.insertedId !== undefined)
						this.router.navigate([`${kingRoutes.main.dash}/${kingRoutes.nested.editpost}/${res.insertedId._id}`]);

					if (res.status === 200){

						this.DataService.emitEvent(res.insertedId);
						this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500});
						this.clearFrom(this.postForm.controls,true);//CLEAR FORM DATA BEFORE SEND

					}

				},

				err => {

					console.error("ERROR", err);
					this.snackbar.open( err.statusText , 'Ok', {duration: 2500});
					this.clearFrom(this.postForm.controls,false);//CLEAR FORM DATA BEFORE SEND

				}

			);

		}

		else this.snackbar.open( this.translate.instant("fillallfields") , 'Ok', { duration: 3000 });
	}


	public clearFrom(form,reset : Boolean) {

		Object.keys(form).forEach((el) => { this.formData.delete(el); });
		this.formData.delete('img');
		this.imagePreviewDOM = '';

		if(reset)
			this.postForm.reset();

	}


	public recoverFormData() {

		var recovered = this.Helpers.getTempData();

		if (recovered) {
			Object.keys(this.postForm.controls).forEach(el => {
				this.postForm.controls[el].setValue(recovered[el].value);
			});
		}

	}


	public saveFormData(){
		this.Helpers.setTempData(this.postForm.controls);
	}

	public handleEmoji(e) : void{
		var field = this.postForm.controls.desc.value;
		this.postForm.controls.desc.setValue(field +  e);
	}


}
