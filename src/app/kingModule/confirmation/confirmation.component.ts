import { Component, OnInit } from '@angular/core';
import { SignService } from "../services/signoperations.service";
import { GlobalenvService } from '../../services/globalenv.service';
import { MetatagsService } from "../../services/metatags.service";
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from "../../animations";
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, FormBuilder } from '@angular/forms';


@Component({
	selector: 'app-confirmation',
	templateUrl: './confirmation.component.html',
	styleUrls: ['./confirmation.component.scss'],
	animations: [routerTransition()],
	providers: [SignService , MetatagsService ]
})
export class ConfirmationComponent implements OnInit {

	public signdata: any = {};
	public statusText: any;
	public Form: any;
	public formData: any = {}

	constructor(
		public SignService: SignService,
		public globalEnv: GlobalenvService,
		public formBuilder: FormBuilder,
		public MetatagsService : MetatagsService,
		public translate : TranslateService
	) { }


	ngOnInit() {

		this.MetatagsService.setTitle( "Sweetvel | Sign Up" );
		this.Form = new FormGroup({

			email: new FormControl( '' ,[ Validators.required]),
			key:   new FormControl('',[ Validators.required, Validators.minLength(5)] ),
			acceptterms: new FormControl( '' ,[Validators.required]),

		});

	}


	public signUp(): void {

		this.RefreshFormData();

		var strings = {

			string1 : this.translate.instant('goandfinishsignup'),
			string2 : this.translate.instant('linkexpiration'),
			string3 : this.translate.instant('dontforgetchangecredits'),
			string4 : this.translate.instant('emailcheckspam'),
			string5 : this.translate.instant('checkyouremail'),
			string6 : this.translate.instant('greetingmessage'),
			stringlogin     : this.translate.instant('login'),
			stringpassword : this.translate.instant('password'),
			stringmailimg  : this.globalEnv.Images.main

		}

		this.formData.strings = strings;

		if (this.Form.status === 'VALID') {

			this.SignService.signUp(this.formData).subscribe(

				res => { this.statusText = this.translate.instant(res.statusText); /* console.log( res ) */ },

				err => { this.statusText =  this.translate.instant(err.statusText); /* console.log( err ) */ }
			);

		}

		else
			this.statusText = this.translate.instant("fillallfields");

	}


	public RefreshFormData() : void{
		Object.keys(this.Form.controls).forEach( el => { this.formData[el] = this.Form.controls[el].value })
	}



}
