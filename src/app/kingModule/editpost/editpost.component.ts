import { Component, OnInit, AfterViewInit, ViewEncapsulation, ViewChild, ElementRef,Inject } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute } from "@angular/router";
import { CrudService } from "../services/crudoperations.service";
import { DataService } from "../services/data.service";
import { HelpersService } from "../../services/helpers.service";
import { StaticDataService } from "../../services/staticdata.service";
import { DomSanitizer } from '@angular/platform-browser';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { AuthService } from "../../services/auth.service";
import { AuthGuard } from '../../services/auth-guard.guard';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';


@Component({
	selector: 'app-editpost',
	templateUrl: './editpost.component.html',
	styleUrls: ['./editpost.component.scss'],
	encapsulation: ViewEncapsulation.None,
	providers: []
})

export class EditpostComponent implements OnInit {

	public profileInfo : any  = this.AuthService.tokenData();
	public postForm: any;
	public formData: any = {};
	public urlParams: any;
	public imagePreviewDOM;
	public categoryData: any;
	public themeData: any = this.DataService.themeData();
	public statusMsg: String;
	public statusUploadText: String;
	public imgUploadedPreview: Array<any>;
	public imgUploaded: any;
	public imgFormData: any;
	public FILES: any = [];
	public dom: Document;
	public postEditStatus : Boolean;
	public imgUploadStatus : Boolean;

	//@ViewChild('colorPreviewDOM') public colorPreviewElem: ElementRef;
	@ViewChild('inputFileDOM') public inputFileElem: ElementRef;
	@ViewChild('sweditor') public sweditor: ElementRef;
	@ViewChild('sweditorDIV') public sweditorDIV: ElementRef;

	constructor(

		private route: ActivatedRoute,
		public CrudService: CrudService,
		public DataService: DataService,
		public StaticData : StaticDataService,
		public sanitizer: DomSanitizer,
		public AuthService : AuthService,
		public snackbar: MatSnackBar,
		public HELPERS : HelpersService,
		public translate : TranslateService,

	) {


		this.postForm = new FormGroup({

			title: new FormControl('', [Validators.required]),
			desc: new FormControl('', []),
			category: new FormControl('', [Validators.required]),
			anonym: new FormControl('', [Validators.required]),
			comments: new FormControl('', [Validators.required])
			//type: new FormControl('', [Validators.required]),
			//theme: new FormControl('', [Validators.required])

		});

		this.route.params.subscribe(params => this.urlParams = params);

		this.imgUploadedPreview = [];

	}

	async ngOnInit() {

		this.categoryData = await this.StaticData.fetchCategories().toPromise();
		this.fetchItem();
		this.imgFormData = new FormData();

	}

	ngAfterViewInit() {

	}


	/////HTTP CRUD OPERATIONS////////////////////////////////HTTP CRUD OPERATIONS///////////////

	public fetchItem() {

		this.CrudService.fetchItem(this.urlParams).subscribe(

			res => {

				Object.keys(this.postForm.controls).forEach(el => {

					this.postForm.controls[el].setValue(res.data[el]);
					this.formData = res.data;
					//this.colorPreview(res.data.theme);

				});

			},

			err => { console.error(err) }
		)

	}


	public updatePost(): void {

		if (this.postForm.valid) {

			this.updateForm();
			this.postEditStatus = true;

			this.CrudService.updatePost(this.formData, this.urlParams.id).subscribe(

				res => {	this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500}) ;this.postEditStatus = false;	},

				err => { 	this.snackbar.open( this.translate.instant(err.statusText) , 'Ok', {duration: 2500}); this.postEditStatus = false;	}

			)

		}

		else 	this.snackbar.open( this.translate.instant('fillallfields') , 'Ok', {duration: 2500});
	}


	public uploadPostImages(): void {

		this.imgUploadStatus = true;

		if (this.imgFormData.length !== undefined) {

			this.CrudService.uploadPostImages(this.addImagesToFormData(this.imgFormData), this.urlParams.id).subscribe(

				res => {

					this.clearImageForm();
					this.fetchItem();
					this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500});
					this.imgUploadStatus = false;

				},

				err => {

					//console.log(err);
					this.imgUploadStatus = false;
					this.clearImageForm();
					this.snackbar.open( this.handleImageUploadError(err.statusText,err.allowednumber,err.allowedsize) , 'Ok', {duration: 2500});

				}
			);

		}
	}


	public setMainImage(img): void {

		this.CrudService.setMainPostImage({ path: img }, this.urlParams.id).subscribe(

			res => { this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500}); this.fetchItem(); },

			err => console.log(err)

		);

	}


	public deletePostImage(img, ind): void {

		//console.log(img, ind);
		var data = { path: img, index: ind };

		this.CrudService.removePostImg(data, this.urlParams.id).subscribe(

			res =>	{ this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500}); this.fetchItem(); },

			err =>	{ console.log(err); }

		)
	}

	/////HELPERS/////////////////////////////////////////////////HELPERS//////////////////////////////////////////

	public colorPreview(el): void {
		//setTimeout( ()  => 	this.colorPreviewElem.nativeElement.style.backgroundColor = this.themeData[el].color)
	}


	public imagePreview(el) {

		var files = el.target.files;
		Object.keys(files).forEach( elem => {

			const file = files[elem];
			const reader = new FileReader();

			reader.onload = (e) => {

				this.imgUploadedPreview[elem] = reader.result ;

			}

			reader.readAsDataURL(el.target.files[elem]);
		})

		this.imgFormData = Array.from(files).concat(this.FILES);
		Array.from(el.target.files, x => this.FILES.unshift(x));
		//console.log("PREVIEW_EVENT",this.imgFormData)
	}


	public removeImageFromPreview(ind): void {
		delete this.imgFormData[ind];
		this.imgUploadedPreview.splice(ind, 1);
	}


	public clearImageForm(): void {
		this.imgFormData = [];
		this.imgUploadedPreview = [];
		this.FILES = [];
	}


	public addImagesToFormData(objImg): any {
		var ReadyImgData = new FormData();
		this.imgFormData.map(x => ReadyImgData.append('img', x));
		return ReadyImgData;
	}


	public updateForm(): any {

		Object.keys(this.postForm.controls).forEach((el) => {
			this.formData[el] = this.postForm.controls[el].value;
		});

	}


	public getEditorData(e) : void{

		//this.postForm.controls.desc.setValue(e);
		//console.log(e)
		console.log(this.postForm.controls.desc.value)

	}


	public clearFrom() {
		Object.keys(this.postForm.controls).forEach((el) => { this.formData.delete(el); });
	}


	public clearFromWithFiles() {
		Object.keys(this.postForm.controls).forEach((el) => { this.formData.delete(el); });
		this.formData.delete('img');
	}


	public handleEmoji(e) : void{
		this.HELPERS.copyToClipBoard( e );
		this.snackbar.open( e , this.translate.instant('copied'), {duration: 1000});
	}


	public copyLink( val ) : void{
		this.HELPERS.copyToClipBoard( val );
		this.snackbar.open( val , 'Ok', {duration: 2000});
	}

	public checkObject(obj) : boolean{
		if(Object.keys(obj).length > 0 )
			return true;

		else
			return false;
	}

	public checkArray(arr) : boolean{
		if(arr.length > 0 )
			return true;
		else
			return false;
	}

	public handleImageUploadError(errtype,allowednumber,allowedsize) : string{

		if(errtype === "maximumfilesize")
			return `${this.translate.instant(errtype)} ${allowedsize}`;

		else if (errtype === "maximumfilestoupload")
			return `${allowednumber} ${this.translate.instant(errtype)}`;

		else
			return `${this.translate.instant(errtype)}`;

	}




}
