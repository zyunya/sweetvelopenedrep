import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SupermodeoperationsService } from '../services/supermodeoperations.service';
import { SnackbarComponent } from "../../sharedcomponents/snackbar/snackbar.component";
import { GlobalenvService } from '../../services/globalenv.service';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material';
import { SocketService } from '../../services/socket.service';
import { AuthService } from '../../services/auth.service';


@Component({
	selector: 'app-usershandler',
	templateUrl: './usershandler.component.html',
	styleUrls: ['./usershandler.component.scss'],
	providers: [SupermodeoperationsService,SocketService ,AuthService],
	encapsulation: ViewEncapsulation.Emulated
})


export class UsershandlerComponent implements OnInit {

	@ViewChild('removeaccountsnackbar') public removeAccountSnack : SnackbarComponent;

	public usersData: Array<any> = [];
	public usersLimit : number = 0;
	public defaultLimit : number = 20;
	public dataLoaded : Boolean = false;
	public search : any = "";
	public deleteAccountSetters : { id : string , index : number } ;


	constructor(

		public SupermodeoperationsService: SupermodeoperationsService,
		public GlobalenvService: GlobalenvService,
		public translate : TranslateService,
		public snackbar: MatSnackBar,
		public SOCKET : SocketService,
		public AuthService : AuthService

	) { }


	ngOnInit() {
		this.getUsersList();
		this.SOCKET.connectNameSpace(`superuser?token=${this.AuthService.getToken()}`)
	}


	ngOnDestroy() {
		this.SOCKET.disconnect();
	};


	public getUsersList(): void {

		this.usersLimit += this.defaultLimit;
		this.dataLoaded = false;

		this.SupermodeoperationsService.getUsersList(this.usersLimit).subscribe(

			res => { this.usersData = res.data; this.dataLoaded = true; /* console.log(res) */ },

			err => { console.error(err) }

		)

	}


	public getUsersListByName( ): any {

		this.usersLimit  = this.defaultLimit;
		this.dataLoaded = false;

		this.SupermodeoperationsService.getUsersListByName(this.defaultLimit , this.search).subscribe(

			res => { this.usersData = res.data; this.dataLoaded = true; /* console.log(res) */ },

			err => { console.error(err) }

		)

	}


	public setUserActive(user , active): void {

		this.SupermodeoperationsService.setUserActive(user._id ,active).subscribe(

			res => {

				if(res.status === 200){

					user.active = active;
					if(active === 0) this.SOCKET.emit("blockaccount" , user);
					this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500});

				}

			},

			err => { console.error(err) }

		)
	}


	public deleteAccount(): any {

		this.SupermodeoperationsService.deleteAccount(this.deleteAccountSetters.id).subscribe(

			res => { if(res.status === 200)	this.usersData.splice(this.deleteAccountSetters.index,1) },

			err => { console.error(err) }

		)
	}


	////////////////////DELETE ACCOUNT SNACK BAR HANDLER ///////////////////////////////

	public removeUserHandler( id : string, index : number ) : void {

		this.removeAccountSnack.open();
		this.deleteAccountSetters = { id : id , index : index};

	}

	public imgErrorHandler(event) {
		event.target.src = this.GlobalenvService.Images.broken;
	}

}
