import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsershandlerComponent } from './usershandler.component';

describe('UsershandlerComponent', () => {
  let component: UsershandlerComponent;
  let fixture: ComponentFixture<UsershandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsershandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsershandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
