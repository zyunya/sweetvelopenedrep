import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsblockComponent } from './postsblock.component';

describe('PostsblockComponent', () => {
  let component: PostsblockComponent;
  let fixture: ComponentFixture<PostsblockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsblockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsblockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
