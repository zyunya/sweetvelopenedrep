import { Component, OnInit,Inject, AfterViewInit, ViewEncapsulation, ViewChildren, ViewChild, QueryList, ElementRef, PLATFORM_ID  } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { CrudService } from "../services/crudoperations.service";
import { HelpersService } from "../../services/helpers.service";
import { AuthService } from "../../services/auth.service";
import { DataService } from "../services/data.service";
import { i18nService } from "../../services/i18n.service";
import { MatSnackBar } from '@angular/material';
import { SnackbarComponent } from "../../sharedcomponents/snackbar/snackbar.component";
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


@Component({

	selector: 'app-postsblock',
	templateUrl: './postsblock.component.html',
	styleUrls: ['./postsblock.component.scss'],
	providers: [CrudService],
	encapsulation: ViewEncapsulation.None,

})
export class PostsblockComponent implements OnInit {


	public postsData: any;
	public defaultLimit: Number | any = 8;
	public pageStep: Array<any>;
	public currentPage: any = 0;
	public currentPageFormatted: Number = 1;
	public defaultCategory: String | Number = 'ALL';
	public defaultActive: String | Number = 'ALL';
	public renderImage: Boolean = false;
	public postsLoaded : Boolean = true;
	public userData : any = this.AuthService.userData();


	constructor(

		public CrudService: CrudService,
		public Helpers: HelpersService,
		public DataService: DataService,
		public snackbar: MatSnackBar,
		public route: ActivatedRoute,
		public translate : TranslateService,
		public i18n : i18nService,
		public AuthService : AuthService,
		@Inject(PLATFORM_ID) private platformId: Object

	) {

		/* CHANGE LANG DETECTION */
		this.i18n.liveLang.subscribe( res => { this.fetchPosts() })

	}

	ngOnInit() {

		this.route.queryParams.subscribe(params => {

			this.currentPage = params.page;
			this.currentPageFormatted = (this.currentPage / this.defaultLimit) + 1 || 1;
			this.fetchPosts();

		});


	}


	ngAfterViewInit() {


	};


	public fetchPosts(): void {

		this.postsLoaded = false;

		this.CrudService.fetchPosts(this.currentPage , this.defaultLimit, this.defaultCategory, this.defaultActive).subscribe(

			res => {

				this.postsData = res.data;
				this.pageStep = this.pageCounter(res.count);
				this.Helpers.noImageHandler(res.data);
				this.postsLoaded = true;

			},

			err => console.log(err)
		)
	};



	public setPage(page, pagepoint): void {
		this.currentPage = page;
		this.DataService.emitEvent(pagepoint);
		this.currentPageFormatted = pagepoint;
	};


	public pageCounter(counter): Array<any> {
		var arr = [];
		for (var i = 0; i < counter / this.defaultLimit; i++) arr.push(i);
		return arr;
	};


}
