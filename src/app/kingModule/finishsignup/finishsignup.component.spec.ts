import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinishsignupComponent } from './finishsignup.component';

describe('FinishsignupComponent', () => {
  let component: FinishsignupComponent;
  let fixture: ComponentFixture<FinishsignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinishsignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishsignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
