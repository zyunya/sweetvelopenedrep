import { Component, OnInit } from '@angular/core';
import { SignService } from '../services/signoperations.service';
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from '@ngx-translate/core';
import { kingRoutes } from '../../services/globalenv.service';

@Component({
	selector: 'app-finishsignup',
	templateUrl: './finishsignup.component.html',
	styleUrls: ['./finishsignup.component.scss'],
	providers : [SignService]
})
export class FinishsignupComponent implements OnInit {

	public statusText : string  = "Loading ...";
	public kingRoutes : any = kingRoutes;

	constructor(
		public SignService: SignService,
		public route : ActivatedRoute,
		public translate : TranslateService
		) { }

	ngOnInit() {
		this.signUpConfirmation();
	}

	public signUpConfirmation() : void{

		this.SignService.signUpConfirmation(this.route.snapshot.params.token).subscribe(

			res => { this.statusText = this.translate.instant(res.statusText); },

			err => { this.statusText = this.translate.instant(err.statusText); }
		)
	}

}
