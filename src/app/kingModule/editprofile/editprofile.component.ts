import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { CrudService } from "../services/crudoperations.service";
import { StaticDataService } from "../../services/staticdata.service";
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';


@Component({
	selector: 'app-editprofile',
	templateUrl: './editprofile.component.html',
	styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {


	@ViewChild("themepreview") public themepreview : ElementRef;

	/* PASSWORD BLOCK */

	public passwordForm: FormGroup;
	public passwordEditedForm: any = {};
	public passwordStatusText: String;
	public changePasswordPreloader : Boolean;

	/* PROFILE INFO BLOCK */

	public profileForm: FormGroup;
	public checked : Boolean;
	public profileInfo: any = {};
	public profileEditedForm: any = {};
	public profileStatusText: String;
	public editProfilePreloader : Boolean;
	public typosList : any;
	public themesList : any;

	public dataLoaded : boolean = false;
	public errorHandler : any;

	constructor(

		public CrudService: CrudService,
		public StaticData : StaticDataService,
		public snackbar: MatSnackBar,
		public translate : TranslateService

	) {

		this.passwordForm = new FormGroup({

			currentpassword: new FormControl('', [Validators.required, Validators.minLength(5)]),
			password: new FormControl('', [Validators.required, Validators.minLength(5) ]),
			repeatpassword: new FormControl('', [Validators.required, Validators.minLength(5)]),

		});

		this.profileForm = new FormGroup({

			login: new FormControl('', [Validators.required,Validators.minLength(5)]),
			firstname: new FormControl('', []),
			lastname: new FormControl('', []),
			about: new FormControl('', []),
			lang: new FormControl('', []),
			instagram: new FormControl('', []),
			facebook: new FormControl('', []),
			vkontakte: new FormControl('', []),
			twitter: new FormControl('', []),
			linkedin: new FormControl('', []),
			theme: new FormControl('', []),
			style: new FormControl('', []),
			typo: new FormControl('', []),
			//anonym: new FormControl('', []),

		});

	}


	async ngOnInit() {

		this.fetchProfile();
		this.typosList = await this.StaticData.fetchTypos().toPromise();
		this.themesList  = await this.StaticData.fetchThemes().toPromise();

	};


	public fetchProfile(): void {

		this.dataLoaded = false;

		this.CrudService.fetchProfileInfo().subscribe(

			res => {

					this.setFormValues(this.profileForm.controls, res.data ) ;
					this.profileEditedForm = res.data;
					this.dataLoaded = true;
					//console.log(  res.data  )

				},

			err => {

				console.error(err);
				this.dataLoaded = true;
				if(err.status === 401) this.errorHandler = err;

				}
		);

	};


	public editProfile(): void {

		if (this.profileForm.valid) {

			this.editProfilePreloader = true;
			this.updateProfileForm();

			this.CrudService.editProfile(this.profileEditedForm).subscribe(

				res => {

					this.editProfilePreloader = false;
					this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500})
					/* console.log( res ) */
					},

				err => { this.snackbar.open( err.statusText , 'Ok', {duration: 2500}); this.editProfilePreloader = false;/* console.log( err )  */ }

			);

		}

		else
			this.profileStatusText = this.translate.instant("fillallfields");

	};


	public editPassword(): void {

		if(this.passwordForm.valid){

			this.changePasswordPreloader = true;
			this.updatePasswordForm();

			this.CrudService.resetPassword( this.passwordEditedForm ).subscribe(

				res => { this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500}); this.changePasswordPreloader = false;/* console.log('SUCCESS',res); */ },

				err => { this.snackbar.open( this.translate.instant(err.statusText) , 'Ok', {duration: 2500}); this.changePasswordPreloader = false; /* console.log('ERROR',err); */ }
			)

		}

	};


	public changeTheme(e) : void{

		let theme = this.themesList.find(x => x.number === e.value );
		this.themepreview.nativeElement.style.backgroundColor = theme.primarycolor;

	}


	public setFormValues(form, values): void {
		Object.keys(form).forEach(el => {
			form[el].setValue(values[el]);
		});
	};


	public updateProfileForm(): void {
		Object.keys(this.profileForm.controls).forEach( el => {
			this.profileEditedForm[el] = this.profileForm.controls[el].value;
		});
	}


	public updatePasswordForm(): void {
		Object.keys(this.passwordForm.controls).forEach( el => {
			this.passwordEditedForm[el] = this.passwordForm.controls[el].value;
		});
	}

	public checkObject(obj) : boolean{
		if(Object.keys(obj).length > 0 )
			return true;
		else
			return false;
	}


	public setLowerCase(e) : void {
		e.target.value = 	e.target.value.toLowerCase();
	}

}
