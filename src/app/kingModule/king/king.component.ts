import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { SignService } from "../services/signoperations.service";
import { AuthService } from '../../services/auth.service';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators, FormBuilder } from '@angular/forms';
import { Router, NavigationStart } from '@angular/router';
import { GlobalenvService } from '../../services/globalenv.service';
import { MetatagsService } from "../../services/metatags.service";
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from "../../animations";
import { MatSnackBar } from '@angular/material';


@Component({
	selector: 'app-king',
	templateUrl: './king.component.html',
	styleUrls: ['./king.component.scss'],
	providers: [SignService, MetatagsService],
	encapsulation: ViewEncapsulation.Emulated,
	animations: [routerTransition()]
})

export class KingComponent implements OnInit {

	public credits: any;
	public errMsg: String;
	public Form;
	public accountsData = this.AuthService.tokenDataArray();

	constructor(

		public SignService: SignService,
		public formBuilder: FormBuilder,
		public Router: Router,
		public globalEnv: GlobalenvService,
		public MetatagsService: MetatagsService,
		public translate: TranslateService,
		public AuthService: AuthService,
		public snackbar: MatSnackBar,

	) {
		this.credits = {};
	}


	ngOnInit() {

		this.MetatagsService.setTitle("Sweetvel | Log In");
		this.Form = this.formBuilder.group({

			login: [Validators.required],
			password: [Validators.required],

		});

	}



	public signIn() {

		if (this.Form.status === 'VALID') {

			this.SignService.signIn(this.credits).subscribe(

				res => {

					if (res.status == 200) {

						/* CHECK IF ACCOUNT SIGNED YET */
						if (this.AuthService.checkSignedAccount(res.token))
							this.snackbar.open(this.translate.instant('accountauthorized'), 'Ok', { duration: 2500 });

						else
							/* ADD NEW SIGNED USER TO ARRAY */
							this.AuthService.saveToken(res.token);
						/* REFRESH SIGNEd USERS ARRAY */
						this.accountsData = this.AuthService.tokenDataArray();


					}

					else
						this.snackbar.open(this.translate.instant('somethingwentwrong'), 'Ok', { duration: 2500 });

				},

				err => this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 })

			)

		}

	}


	public removeSignedAccount(ind): void {

		this.AuthService.removeTokenAccount(ind)
		this.accountsData = this.AuthService.tokenDataArray();

	}


}
