import { Component, OnInit, ViewChild, ViewChildren, Input, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { CrudService } from "../../services/crudoperations.service";
import { SupermodeoperationsService } from "../../services/supermodeoperations.service";
import { HelpersService } from "../../../services/helpers.service";
import { AuthService } from "../../../services/auth.service";
import { DataService } from "../../services/data.service";
import { i18nService } from "../../../services/i18n.service";
import { MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SnackbarComponent } from "../../../sharedcomponents/snackbar/snackbar.component";


@Component({
	selector: 'app-postcard',
	templateUrl: './postcard.component.html',
	styleUrls: ['./postcard.component.scss'],
	providers : [SupermodeoperationsService]
})


export class PostcardComponent implements OnInit {


	@ViewChildren('pc') public CardDom: any;
	@ViewChild('postsnackbar') public postSnackbar: SnackbarComponent;
	@Input("data") public postsData: any;
	@Input("postsloaded") public postsLoaded: any;
	public postDeleteSetters: { id: string, index: number };
	public userData: any = this.AuthService.userData();
	public priorityData = this.DataService.priorityData();

	constructor(

		public CrudService: CrudService,
		public SuperModeService: SupermodeoperationsService,
		public Helpers: HelpersService,
		public DataService: DataService,
		public snackbar: MatSnackBar,
		public route: ActivatedRoute,
		public translate: TranslateService,
		public i18n: i18nService,
		public AuthService: AuthService,
		@Inject(PLATFORM_ID) private platformId: Object

	) {
		this.i18n.liveLang.subscribe(res => {  })
	}


	ngOnInit() {

	}

	ngOnChanges(): void {

	}


	public removePost(): void {

		this.CrudService.removePost(this.postDeleteSetters.id).subscribe(

			res => {

				if (res.status === 200)
					this.CardDom._results[this.postDeleteSetters.index].nativeElement.style.webkitFilter = "blur(2px) grayscale(50%)"

				else
					console.error(res);

			},

			err => { console.error(err) }

		)

	};



	////////////////////POST DELETE SNACK BAR HANDLERS ///////////////////////////////

	public removePostHandler(id: string, index: number): void {

		this.postSnackbar.open();
		this.postDeleteSetters = { id: id, index: index };

	}


	public destroyElem(index): void {
		console.log(this.CardDom._results[index].nativeElement);
	};


	/*          SUPER MODE ACTIONS          */

	public setPriority(post, prio) {

		this.SuperModeService.setPriority(post._id, prio).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.priority = prio; /* console.log( res ); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.error(err); }

		)
	}


	public setApprove(post, approve) {

		this.SuperModeService.setApprove(post._id, approve).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.approve = approve; /* console.log(res); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.log(err); }

		)
	}


	public setActivation(post, status) {

		this.SuperModeService.activationPost(post._id, status).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.active = status; /* console.log( res ); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.log(err); }

		)
	}


	/*          USER ACTIONS             */

	public setAnonym(post, anonym) {

		this.CrudService.setAnonym(post._id, anonym).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.anonym = anonym; /* console.log(res); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.log(err); }

		)
	}




	public setGallery(post, gallery) {

		this.CrudService.setGallery(post._id, gallery).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.gallery = gallery; /* console.log( res ); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.log(err); }

		)
	}



	public setProduction(post, production) {

		this.CrudService.setProduction(post._id, production).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.production = production; /* console.log(res); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.log(err); }

		)
	}



	public setComments(post, comments) {

		this.CrudService.setComments(post._id, comments).subscribe(

			res => { this.snackbar.open(this.translate.instant(res.statusText), 'Ok', { duration: 2500 }); if (res.status === 200) post.comments = comments;  /* console.log( res ); */ },

			err => { this.snackbar.open(this.translate.instant(err.statusText), 'Ok', { duration: 2500 }); console.error(err); }

		)
	}



	public openPost(href): void {

		if (isPlatformBrowser(this.platformId)) {
			var link = `//${location.host}/ch/${href}`;
			window.open(link);
		}

	};


}
