import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from "../services/data.service";
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-postlist',
	templateUrl: './postlist.component.html',
	styleUrls: ['./postlist.component.scss'],
	encapsulation: ViewEncapsulation.None
})

export class PostlistComponent implements OnInit {

	public searchBar: any;
	public routeLinks: any[];
	public currentPage : Number = 1;
	public userRole : String = this.AuthService.userRole();

	constructor(

		public DataService: DataService,
		public AuthService: AuthService

	) {

		this.DataService.onGetData.subscribe(res => {	this.currentPage = res;})

		this.routeLinks = [

			{label: '',link: './'}

		];
	}

	ngOnInit() {
	}



}
