import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { CrudService } from '../services/crudoperations.service';
import { Router } from "@angular/router";
import { HelpersService } from "../../services/helpers.service";
import { MatSnackBar } from '@angular/material';
import { kingRoutes } from '../../services/globalenv.service';
import { i18nService } from '../../services/i18n.service';
import { TranslateService } from '@ngx-translate/core';
import { SocketService } from '../../services/socket.service';
import { MetatagsService } from "../../services/metatags.service";

@Component({
	selector: 'app-kingdashboard',
	templateUrl: './kingdashboard.component.html',
	styleUrls: ['./kingdashboard.component.scss'],
	providers : [CrudService,AuthService,SocketService , MetatagsService ],
	encapsulation: ViewEncapsulation.Emulated
})
export class KingdashboardComponent implements OnInit {

	public userData : any;
	public kingRoutes : any = kingRoutes;
	public accountsData = this.AuthService.tokenDataArray();

	constructor(

		public AuthService: AuthService,
		public CrudService : CrudService,
		public Router       : Router,
		public snackbar: MatSnackBar,
		public HELPERS : HelpersService,
		public i18n : i18nService,
		public translate : TranslateService,
		public SOCKET :  SocketService,
		public META : MetatagsService

		) {
			//console.log(+true,+false) ;console.log(!!1,!!0)
		 }


	ngOnInit() {

		this.fetchProfile();
		this.SOCKET.connectNameSpace(`king?token=${this.AuthService.getToken()}`)
		this.handleBlockedAccount();
		this.META.setTitle("DASHBOARD")

	}


	ngOnDestroy() {
		this.SOCKET.disconnect();
	};


	public fetchProfile() : void {

		this.CrudService.fetchProfileInfo().subscribe(

			res => { this.userData = res.data  ;  /* console.log(  res.data );  */ },

			err => { this.AuthService.handleAuthStatus(err.status); console.error(err); }

		);

	};


	public resetAvatar(e , oldimg) : void {

		var avatar = new FormData();
		avatar.append( 'img', e.target.files[0] );
		avatar.append( 'oldimg', oldimg );

		this.CrudService.resetAvatar(avatar).subscribe(

			res =>{ if( res.status === 200 ) this.fetchProfile() ; this.snackbar.open( this.translate.instant(res.statusText) , 'Ok', {duration: 2500});  },

			err => { this.snackbar.open(  this.translate.instant(err.statusText) , 'Ok', {duration: 2500}); }
		)

	};


	public changeAccount(token) : void{

		this.AuthService.authOK(token , this.kingRoutes.main.loading);
		setTimeout(() => this.Router.navigate([this.kingRoutes.main.dash]),500 );

	}


	public logOut() : void{

		this.AuthService.removeToken();
		this.Router.navigate([kingRoutes.main.signin]);


	};


	public handleBlockedAccount() : void{

		this.SOCKET.on('accountblocked', (data) => {

			this.snackbar.open(  this.translate.instant("accountblocked") , 'Ok', {duration: 2500});
			setTimeout(() => this.logOut() , 3000);

		});

	}



}
