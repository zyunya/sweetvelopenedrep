import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KingdashboardComponent } from './kingdashboard.component';

describe('KingdashboardComponent', () => {
  let component: KingdashboardComponent;
  let fixture: ComponentFixture<KingdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KingdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KingdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
