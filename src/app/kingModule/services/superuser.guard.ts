import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild , ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Router, NavigationStart } from '@angular/router';
import { kingRoutes } from '../../services/globalenv.service';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class SuperuserGuard implements CanActivate, CanActivateChild {

	public token: any = this.AuthService.getToken();

	constructor(
		public AuthService: AuthService,
		public Router: Router,

	) {

	}

	canActivate(): any {

		if (!this.isSuperUser()) {

			this.Router.navigate([kingRoutes.main.dash]);
			return false;

		}

		else
			return true;

	};


	canActivateChild() {

		if (!this.isSuperUser()) {

			this.Router.navigate([kingRoutes.main.dash]);
			return false;

		}

		else
			return true;

	}


	public getRole(): String {
		return jwt.decode(this.token).role || "undefined";
	}


	public isSuperUser(): Boolean {

		if (this.getRole() === "superuser")
			return true;
		else
			return false;

	}


}