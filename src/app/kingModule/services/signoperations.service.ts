
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';


@Injectable()
export class SignService {

	constructor(
		public http: Http,

		) {

	}

	public signUp(data): Observable<any> {
		return this.http.post(environment.API.SIGN_UP ,data )
			.map(this.extractData)
			.catch(this.handleError);
	};

	public signUpUser(data): Observable<any> {
		return this.http.post(environment.API.SIGN_UP_USER ,data )
			.map(this.extractData)
			.catch(this.handleError);
	};

	public signUpConfirmation(token): Observable<any> {
		return this.http.get(`${environment.API.SIGN_UP_CONFIRMATION}/${token}` )
			.map(this.extractData)
			.catch(this.handleError);
	};

	public signIn(data): Observable<any> {
		return this.http.post(environment.API.SIGN_IN,data)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public saveToken(token){
		localStorage.setItem('auth',token);
	}

	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}


}
