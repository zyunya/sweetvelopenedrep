
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth.service';
import { i18nService } from '../../services/i18n.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';


@Injectable()
export class SupermodeoperationsService {

	public token: String = this.AuthService.getToken();

	constructor(
		public http: Http,
		public AuthService: AuthService,
		public i18n: i18nService
	) {

	}


	/*POSTS OPERATIONS */
	public fetchAllPosts(skip, limit, active): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(`${environment.API.FETCH_ALL_POSTS}/${skip}/${limit}/${active}/${this.i18n.getDefaultLang()}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public activationPost(id, status): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.ACTIVATION_POST}/${id}`, { status: status }, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public setPriority(id, prio): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_PRIORITY}/${id}`, { priority: prio }, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public setApprove(id, approve): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_APPROVE}/${id}`, { approve: approve }, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	/*USERS OPERATIONS */
	public getUsersList(limit): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(`${environment.API.USERS_LIST}/${limit}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public getUsersListByName(limit, name): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(`${environment.API.USERS_LIST}/${limit}/${name}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}


	public deleteAccount(id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.delete(`${environment.API.DELETE_ACCOUNT}/${id}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public getSignUpKey(): Observable<any> {

		let Head = new Headers({ Authorization: this.token });
		return this.http.get(environment.API.KEY_GET, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);

	}

	public updateSignUpKey(password): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(environment.API.KEY_UPDATE, { password: password }, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public setUserActive(id, active): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_USER_ACTIVE}/${id}`, { active: active }, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}

}
