
import { Injectable, EventEmitter, Output } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../environments/environment'; import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

@Injectable()
export class DataService {

	public onGetData = new EventEmitter();
	public servePostsData: any;


	constructor(
		public http: Http,
		private domSanitizer: DomSanitizer,
	) {
		this.servePostsData = {};

	}


	public themeData(): any {

		return [

			{ name: "Purple", number: 0, color: "#673ab7" },
			{ name: "Fancy", number: 1, color: "#EC407A" },
			{ name: "Yell", number: 2, color: "#FFEB3B" },
			{ name: "Light Green", number: 3, color: "#CDDC39" },
			{ name: "Snow", number: 4, color: "white" },
			{ name: "Strict", number: 5, color: "#656565" }

		]
	}


	public themeDataResolved(): Array<any> {

		return [

		{ toolbar: "#673ab7", container: "white"},
		{ toolbar: "#EC407A", container: "white"},
		{ toolbar: "#FFEB3B", container: "white"},
		{ toolbar: "#CDDC39", container: "white"},
		{ toolbar: "lightblue", container: "white"},
		{ toolbar: "#656565", container: "white"}

		];

	}


	public priorityData() : Array<any> {

		return [

			{ name : "High" , value : 2},
			{ name : "Medium" , value : 1},
			{ name : "Usual" , value : 0}

		]
	}




	public setPostsData(data) {

		this.servePostsData = data;
		this.onGetData.emit("HELLO");

	}


	public emitEvent(ln) {
		this.onGetData.emit(ln);
	}


	public extractData(res: Response) {
		let body = res.json();
		return body;
	}


	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}


}