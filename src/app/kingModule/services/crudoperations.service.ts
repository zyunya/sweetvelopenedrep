
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth.service';
import { i18nService } from '../../services/i18n.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';


@Injectable()
export class CrudService {

	public token : String = this.AuthService.getToken();

	constructor(
		public http: Http,
		public AuthService  : AuthService,
		public i18n : i18nService
		) {

	}

	//USER CRUD OPERATIONS

	public fetchProfileInfo(): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(environment.API.FETCH_PROFILE,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public editProfile(data): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.EDIT_PROFILE}` , data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public resetAvatar(data): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.RESET_AVATAR}` , data, {headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public resetPassword(data): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.RESET_PASSWORD}` , data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	//POSTS CRUD OPERATIONS

	public fetchPosts(skip,limit,category,active): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(`${environment.API.FETCH_POSTS}/${skip}/${limit}/${category}/${active}/${this.i18n.getDefaultLang()}`,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public fetchItem(data): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.get(`${environment.API.FETCH_POST}/${data.id}`,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public createEssence(data): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.post(`${environment.API.CREATE_ESSENCE}/${this.i18n.getDefaultLang()}`, data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public updatePost(data,id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.UPDATE_POST}/${id}/${this.i18n.getDefaultLang()}`, data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public uploadPostImages(data,id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.UPLOAD_POST_IMG}/${id}`, data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public setMainPostImage(data,id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_MAIN_POST_IMAGE}/${id}`, data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public removePost(id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.delete(`${environment.API.REMOVE_POST}/${id}`,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public removePostImg(data,id): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.REMOVE_POST_IMG}/${id}`,data,{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public setAnonym( id , anonym ): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_ANONYM}/${id}`,{ anonym : anonym },{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public setProduction( id , production ): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_PRODUCTION}/${id}`,{ production : production },{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public setComments( id , comments ): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_COMMENTS}/${id}`,{ comments : comments },{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}

	public setGallery( id , gallery ): Observable<any> {
		let Head = new Headers({ Authorization: this.token });
		return this.http.put(`${environment.API.SET_GALLERY}/${id}`,{ gallery : gallery },{headers : Head})
			.map(this.extractData)
			.catch(this.handleError);
	}


	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}

}