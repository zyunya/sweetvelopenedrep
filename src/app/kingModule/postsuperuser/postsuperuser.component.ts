import { Component, OnInit } from '@angular/core';
import { SupermodeoperationsService } from "../services/supermodeoperations.service";
import { HelpersService } from "../../services/helpers.service";
import { ActivatedRoute } from '@angular/router';
import { i18nService } from "../../services/i18n.service";


@Component({
	selector: 'app-postsuperuser',
	templateUrl: './postsuperuser.component.html',
	styleUrls: ['./postsuperuser.component.scss'],
	providers : [SupermodeoperationsService]
})


export class PostsuperuserComponent implements OnInit {


	public postsData: any;
	public postsLoaded : Boolean = true;
	public defaultLimit: Number | any = 8;
	public defaultActive: Number | any = "ALL";
	public pageStep: Array<any>;
	public currentPage: any = 0;
	public currentPageFormatted: Number = 1;


	constructor(

		public SuperMode: SupermodeoperationsService ,
		public Helpers: HelpersService,
		public route : ActivatedRoute,
		public i18n : i18nService


	) {
		 /* CHANGE LANG DETECTION */
		this.i18n.liveLang.subscribe( res => { this.fetchAllPosts(); })
	}

	ngOnInit() {

		this.route.queryParams.subscribe(params => {

			this.currentPage = params.page;
			this.currentPageFormatted = (this.currentPage / this.defaultLimit) + 1 || 1;
			this.fetchAllPosts();

		});

	}


	//POSTS FOR SUPER ADMIN //
	public fetchAllPosts(): void {

		this.postsLoaded = false;

		this.SuperMode.fetchAllPosts(this.currentPage, this.defaultLimit , this.defaultActive ).subscribe(

			res => {

				this.postsData = res.data;
				this.pageStep = this.pageCounter(res.count);
				this.Helpers.noImageHandler(res.data);
				this.postsLoaded = true;

			},

			err => console.log(err)
		)
	};


	public pageCounter(counter): Array<any> {
		var arr = [];
		for (var i = 0; i < counter / this.defaultLimit; i++) arr.push(i);
		return arr;
	};

}
