import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsuperuserComponent } from './postsuperuser.component';

describe('PostsuperuserComponent', () => {
  let component: PostsuperuserComponent;
  let fixture: ComponentFixture<PostsuperuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsuperuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsuperuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
