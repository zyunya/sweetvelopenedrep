import { Component, OnInit } from '@angular/core';
import { SupermodeoperationsService } from '../services/supermodeoperations.service';
import { BasicImplementation , FormData } from './keyhandler.interface';
import { ClipboardService } from "../../services/clipboard.service";
import { MatSnackBar } from '@angular/material';


@Component({
	selector: 'app-keyhandler',
	templateUrl: './keyhandler.component.html',
	styleUrls: ['./keyhandler.component.scss'],
	providers: [SupermodeoperationsService]
})


export class KeyhandlerComponent implements BasicImplementation  {

	public keys: any = {};
	public formdata :  FormData = {} ;
	public statusTxt  : String;

	constructor(
		public SupermodeoperationsService: SupermodeoperationsService,
		public ClipboardService : ClipboardService,
		public snackbar: MatSnackBar,
	) {
		this.keys.signupkey = "Loading ..."
	}


	ngOnInit() {

		this.getSignUpKey();

	}


	public getSignUpKey(): void {

		this.SupermodeoperationsService.getSignUpKey().subscribe(

			data => { this.keys.signupkey = data.signupkey; /* console.log(data) */ },

			err => console.error(err)
		);

	}


	public updateSignUpKey(): void {

		this.SupermodeoperationsService.updateSignUpKey(this.formdata.password).subscribe(

			data => { this.snackbar.open(data.statusText, 'Ok', { duration: 2000 });this.keys.signupkey = data.signupkey /* console.log(data) */ },

			err => { this.snackbar.open(err.statusText, 'Ok', { duration: 2000 });  /* console.error(err) */ }
		);

	}


	public copyToClipBoard() : void {
		this.ClipboardService.copy(this.keys.signupkey);
		this.snackbar.open("", 'Скопировано', { duration: 2000 });
	}

}


