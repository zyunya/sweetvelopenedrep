import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyhandlerComponent } from './keyhandler.component';

describe('KeyhandlerComponent', () => {
  let component: KeyhandlerComponent;
  let fixture: ComponentFixture<KeyhandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KeyhandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KeyhandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
