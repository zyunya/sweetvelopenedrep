import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router, NavigationStart } from '@angular/router';

/* GUARDS */
import { AuthGuard } from '../services/auth-guard.guard';
import { HomeGuard } from '../services/home-guard.guard';
import { SuperuserGuard } from './services/superuser.guard';

/* ROUTE STRINGS */
import { kingRoutes } from '../services/globalenv.service';

/* COMPONENTS */
import { KingComponent } from './king/king.component';
import { KingdashboardComponent } from './kingdashboard/kingdashboard.component';
import { PostshandlerComponent } from './postshandler/postshandler.component';
import { PostsblockComponent } from './postsblock/postsblock.component';
import { PostsuperuserComponent } from './postsuperuser/postsuperuser.component';
import { PostlistComponent } from './postlist/postlist.component';
import { PostcardComponent } from './sharedcomponents/postcard/postcard.component';
import { EditpostComponent } from './editpost/editpost.component';
import { EditprofileComponent } from './editprofile/editprofile.component';
import { KeyhandlerComponent } from './keyhandler/keyhandler.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { FinishsignupComponent } from './finishsignup/finishsignup.component';
import { SignupuserComponent } from './signupuser/signupuser.component';
import { UsershandlerComponent  } from  './usershandler/usershandler.component';
import { LoadingComponent  } from  './loading/loading.component';


export const AppRoutes: Routes = [


	{ path: kingRoutes.main.signin , component: KingComponent,canActivate: [HomeGuard] },
	{ path: kingRoutes.main.signup, component: ConfirmationComponent },
	{ path: kingRoutes.main.signupuser, component: SignupuserComponent },
	{ path: `${kingRoutes.main.finishsignup}/:token`, component: FinishsignupComponent },
	{ path : kingRoutes.main.loading ,component         : LoadingComponent },

	{
	  path:kingRoutes.main.dash, component: KingdashboardComponent ,canActivate: [AuthGuard],
	  children : [

			{path : '',component : PostlistComponent ,
				children : [

					{path : '',component :  PostsblockComponent , canActivate: [] },
					{path : kingRoutes.nested.postsuperuser , component :  PostsuperuserComponent , canActivate : [SuperuserGuard] }

					]
			},

			{path : kingRoutes.nested.posthandler ,component    : PostshandlerComponent },
			{path : kingRoutes.nested.keyhandler,component      : KeyhandlerComponent ,canActivate: [SuperuserGuard]},
			{path : kingRoutes.nested.usershandler,component     : UsershandlerComponent ,canActivate: [SuperuserGuard]},
			{path : kingRoutes.nested.editprofile ,component      : EditprofileComponent },
			{path : `${kingRoutes.nested.editpost}/:id`,component  : EditpostComponent},
			{path : kingRoutes.nested.list ,component            : PostlistComponent }

		]
	},

];


@NgModule({

	imports: [RouterModule.forRoot(AppRoutes)],
	exports: [RouterModule],
	providers: [AuthGuard,HomeGuard,SuperuserGuard],

})

export class AppRoutingModule {

	constructor(router: Router) {

	}
}

export const RoutingComponents = [

	KingComponent,
	KingdashboardComponent,
	PostsblockComponent,
	PostsuperuserComponent,
	PostlistComponent,
	PostcardComponent,
	EditpostComponent,
	EditprofileComponent,
	ConfirmationComponent,
	KeyhandlerComponent,
	FinishsignupComponent,
	SignupuserComponent,
	UsershandlerComponent,
	LoadingComponent

];