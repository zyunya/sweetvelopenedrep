
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
import { kingRoutes } from './globalenv.service';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import { Router, NavigationStart } from '@angular/router';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthService {


	public tokensArray: Array<any> = [];


	constructor(

		@Inject(PLATFORM_ID) private platformId: Object,
		public http: Http,
		public Router: Router

	) {

	}


	public saveToken(token) {

		if (isPlatformBrowser(this.platformId)) {

			this.tokensArray = JSON.parse(localStorage.getItem("auth")) ? JSON.parse(localStorage.getItem("auth")) : [];
			this.tokensArray.push(token);
			localStorage.setItem('auth', JSON.stringify(this.tokensArray));

		}

	}


	public getToken(): any {

		if (isPlatformBrowser(this.platformId)) {
			var token = localStorage.getItem('authOK');
			return token;
		}

	}


	public getTokenArray(): any {

		if (isPlatformBrowser(this.platformId)) {

			try {
				var token = localStorage.getItem('auth');
				var tokenArray = JSON.parse(token)
				return tokenArray;
			}

			catch (err) {
				console.log(err);
				localStorage.removeItem('auth');
				return [];
			}

		}

	}


	public tokenDataArray(): Array<any> {

		var dataUser = [];
		var user = [];

		if (this.getTokenArray()) {

			this.getTokenArray().forEach((el, ind) => {

				user[ind] = jwt.decode(el);
				user[ind].token = el;
				dataUser.push(user[ind]);

			});

		}

		return dataUser;

	}


	public checkToken(): boolean {
		if (isPlatformBrowser(this.platformId)) {
			var token = localStorage.getItem('auth');
			if (token === null || token === undefined)
				return false;
			else
				return true;
		}
	}


	public checkSignedAccount(token): Boolean {

		if (isPlatformBrowser(this.platformId)) {

			var checkSigned = false;
			var newSignedLogin = jwt.decode(token).login;
			this.tokenDataArray().forEach((el) => {
				checkSigned = el.login === newSignedLogin ? true : false;
			});

			return checkSigned;

		}

	}


	public userID(): any {

		if (isPlatformBrowser(this.platformId)) {
			if (this.checkToken())
				return jwt.decode(this.getToken()) ? jwt.decode(this.getToken()).id : null
			else
				return null
		}
	}


	public userRole(): any {
		if (isPlatformBrowser(this.platformId)) {
			if (this.checkToken())
				return jwt.decode(this.getToken()) ? jwt.decode(this.getToken()).role : null
			else
				return null
		}
	}


	public tokenExpired(): Boolean {
		return new Date() > new Date(jwt.decode(this.getToken()).exp * 1000);
	}


	public removeToken() {
		if (isPlatformBrowser(this.platformId)) {
			localStorage.removeItem('authOK');
		}
	}


	public removeTokenAccount(index) {

		if (isPlatformBrowser(this.platformId)) {

			var accoutsArray = this.getTokenArray()
			accoutsArray.splice(index, 1);
			localStorage.setItem('auth', JSON.stringify(accoutsArray));

		}

	}


	public extractData(res: Response) {
		let body = res.json();
		return body;
	}


	public tokenData(): void {

		return jwt.decode(this.getToken());
	}


	public userData(): void {
		return jwt.decode(this.getToken());
	}


	public authOK(token, page): void {
		localStorage.setItem('authOK', token);
		setTimeout(() => this.Router.navigate([page]));
	}


	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}


	public handleAuthStatus(status) {
		if (status === 401 || status === 400) {
			this.removeToken();
			this.Router.navigate([kingRoutes.main.signin]);
		}
	}

}