import { Injectable, Inject, PLATFORM_ID, EventEmitter } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Router, NavigationEnd } from "@angular/router";
import * as stringify from "json-stringify-safe";


@Injectable()
export class LocaldataService {


	constructor(
		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(Router) public router: Router
	) { }


	public addLocalPost(postid): void {

		if (isPlatformBrowser(this.platformId)) {

			if (Array.isArray(this.getLocalPosts())) {

				if (this.checkIfHavePost(postid)) {

					try {

						let newdata = this.getLocalPosts();
						newdata.splice(newdata.indexOf(postid), 1)
						let data = JSON.stringify(newdata);
						localStorage.setItem("localposts", data);
						//console.log("dataAfterRemove" , data);

					}

					catch (err) { console.log(err) };

				}

				else {

					try {

						let newdata = this.getLocalPosts();
						newdata.push(postid);
						let data = JSON.stringify(newdata);
						localStorage.setItem("localposts", data);
						//console.log("dataAfterAdd" , data);

					}

					catch (err) { console.log(err) };

				}

			}


		}

	}



	public getLocalPosts(): Array<String> {

		if (isPlatformBrowser(this.platformId)) {

			var data = localStorage.getItem("localposts") || "[]";
			return JSON.parse(data);

		}

	}


	public getLocalPostsString(): String {

		if (isPlatformBrowser(this.platformId)) {

			var data = localStorage.getItem("localposts");
			return data;

		}

	}



	public checkIfHavePost(id): Boolean {

		if (isPlatformBrowser(this.platformId)) {

			try {

				if (this.getLocalPosts().includes(id))
					return true

				else
					return false

			}

			catch (err) { console.log(err) }

		}

	}


	public saveCurrentRoute(): void {

		this.router.events.subscribe((event) => {

			if (event instanceof NavigationEnd)
				localStorage.setItem("currenturl", event.url);

		})

	}


	public getLastUrl(): String {
		return localStorage.getItem("currenturl") ? localStorage.getItem("currenturl") : "/";
	}


	public clearLocalData(): void {

		if (isPlatformBrowser(this.platformId)) {
			localStorage.removeItem("localposts");
			localStorage.removeItem("chlang")
		}

	}


}
