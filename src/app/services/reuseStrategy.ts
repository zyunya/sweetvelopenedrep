/* REUSE STRATEGY PROVIDES A SERVICE WHICH FULLY RELOADS AND REFRESHES COMPONENT ON ROUTE PARAMS CHANGES */
/* this class have to be included to router module as provider ,providers : [{provide: RouteReuseStrategy, useClass: CustomReuseStrategy}] */

import { ActivatedRouteSnapshot, RouteReuseStrategy, DetachedRouteHandle } from '@angular/router';

export class CustomReuseStrategy implements RouteReuseStrategy {


	shouldDetach(route: ActivatedRouteSnapshot): boolean {
		return false
	}

	store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): boolean {
		return false;
	}

	shouldAttach(route: ActivatedRouteSnapshot): boolean {
		return false;
	}

	retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
		return false;
	}

	shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
		return false;
	}
}