
import { Injectable, Inject, PLATFORM_ID, EventEmitter } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { ClipboardService } from "../services/clipboard.service";
import * as stringify from "json-stringify-safe";




@Injectable()
export class HelpersService {

	public defaultImg: String = "../assets/img/chlogo.png";
	public copyEvent = new EventEmitter();
	public Scroller: number = 0;

	constructor(
		@Inject(PLATFORM_ID) private platformId: Object,
		public ClipboardService: ClipboardService
	) {

	}


	public noImageHandler(data): void {
		data.forEach(el => { if (el.image.length === 0) el.image[0] = this.defaultImg });
	};

	//SAVE FORM DATA OBJECT TO LOCAL STORAGE INORDER TO RECOVER FIELDS IF WE LOST IT

	public setTempData(obj): void {
		var data = stringify(obj);
		localStorage.setItem('tempdata', data);
	};


	public getTempData(): any {
		var data = localStorage.getItem('tempdata');
		if (data !== null)
			return JSON.parse(data);
		else
			return false;
	};


	public setInstaProfileHref() {
		if (isPlatformBrowser(this.platformId)) {
			setTimeout(() => { window.location.href = "https://instagram.com/chakirberg"; }, 25);
			window.location.href = "instagram://user?username=chakirberg";
		}
	};


	public setInstaMediaHref(mediaHref) {
		var link = mediaHref.split("/").splice(-2)[0];
		if (isPlatformBrowser(this.platformId)) {
			setTimeout(() => { window.location.href = `https://instagram.com/p/${link}`; }, 25);
			window.location.href = `instagram://media?id=${link}`;
		}
	};


	public setFBProfileHref() {
		if (isPlatformBrowser(this.platformId)) {
			setTimeout(() => { window.location.href = "https://facebook.com/chakirberg"; }, 25);
			//window.location.href = "fb://profile/chakirberg";
		}
	}


	public copyToClipBoard(val): void {
		this.ClipboardService.copy(val);
	}


	public isIOS(): Boolean {

		if (isPlatformBrowser(this.platformId)) {
			var ios = window.navigator.userAgent.toLowerCase().match("iphone|ipad")
			if (ios)
				return true
			else
				return false
		}

	}


	public isAndroid(): Boolean {

		if (isPlatformBrowser(this.platformId)) {
			var android = window.navigator.userAgent.toLowerCase().match("android")
			if (android)
				return true
			else
				return false
		}

	}


	public isMobile(): Boolean {

		if (isPlatformBrowser(this.platformId)) {
			var mobile = window.navigator.userAgent.toLowerCase().match("iphone|ipad|ipod|android|blackberry|windows mobile|nokia|opera mini")
			if (mobile)
				return true
			else
				return false
		}

	}


	public standAlone(): Boolean {

		if (isPlatformBrowser(this.platformId)) {

			var IOS = window.navigator['standalone'];
			var ANDROID = window.matchMedia('(display-mode: standalone)').matches;

			if (IOS || ANDROID)
				return true
			else
				return false

		}

	}


	public IOSstatusBarRender() {

		if (isPlatformBrowser(this.platformId)) {

			if (this.isIOS() && this.standAlone())
				return true;
			else
				return false;

		}

	}


	public iosApp(){

		if (isPlatformBrowser(this.platformId)) {

			if (this.isIOS() && this.standAlone())
				return true;
			else
				return false;

		}

	}


	public scrollUp(): void {

		if (isPlatformBrowser(this.platformId)) {
			window.scrollTo(0, 0);
		}

	}



	public scrollHorizontal(direction , container): void {

		var scrollOffset = 80;

		if (direction == "right") {

			if (this.Scroller < (container.offsetWidth / 2) - scrollOffset) {
				this.Scroller += scrollOffset;
				container.scroll({ top: 0, left:this.Scroller, behavior: 'smooth' });
			}

		}
		else if (direction == "left") {

			if (this.Scroller > 0) {
				this.Scroller -= scrollOffset,
				container.scroll({ top: 0, left: this.Scroller, behavior: 'smooth' })
			}

		}


	}


	public BrowserOk() : Boolean{

		if (isPlatformBrowser(this.platformId))
			return true;
		else
			return false;

	}


	public ServerOk() : Boolean{

		if (isPlatformServer(this.platformId))
			return true;
		else
			return false;

	}


}