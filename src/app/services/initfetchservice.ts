
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment'; import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

@Injectable()
export class InitfetchService {

	public _instaProfile: Observable<any> = null;
	public _instaGallery: Observable<any> = null;
	public _youtubeChannel: Observable<any> = null;
	public _youtubePlaylist: Observable<any> = null;

	constructor(public http: Http) {

	}

	public fetchGallery(): Observable<any> {

		if (!this._instaGallery) {
			this._instaGallery = this.http.get(environment.API.INSTA_PROFILE)
				.map(this.extractData)
				.publishReplay(1)
				.refCount()
				.catch(this.handleError);
		}

		return this._instaGallery;
	}

	public fetchProfile(): Observable<any> {

		if (!this._instaProfile) {
			this._instaProfile = this.http.get(environment.API.INSTA_GALLERY)
				.map(this.extractData)
				.publishReplay(1)
				.refCount()
				.catch(this.handleError);
		}

		return this._instaProfile;
	}

	public fetchYoutubeChannel(): Observable<any> {
		if (!this._youtubeChannel) {
			this._youtubeChannel = this.http.get(environment.API.YOUTUBE_CHANNEL)
				.map(this.extractData)
				.publishReplay(1)
				.refCount()
				.catch(this.handleError);
		}

		return this._youtubeChannel;
	}

	public fetchYoutubeChannelPlaylist(): Observable<any> {

		this._youtubePlaylist = this.http.get(environment.API.YOUTUBE_CHANNEL_PLAYLIST)
			.map(this.extractData)
			.catch(this.handleError);

		return this._youtubePlaylist;
	}

	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error);
	}

}