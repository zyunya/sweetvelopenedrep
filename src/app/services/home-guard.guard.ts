import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { Router, NavigationStart } from '@angular/router';
import { kingRoutes } from './globalenv.service';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class HomeGuard implements CanActivate{

	public token: any = this.AuthService.getToken();
	public tokenIat: any;
	public tokenExp: any;
	public currentTime: any;

	constructor(
		public AuthService: AuthService,
		public Router: Router,

	) {

	}
	canActivate(): boolean {

		if (this.actualToken()) {
			this.Router.navigate([kingRoutes.main.dash]);
			this.actualTokenMsg();
			return true;
		}
		else {
			this.actualTokenMsg();
			return true;
		}
	};

	public tokenValid(token, val): number {
		return jwt.decode(token) !== null ? jwt.decode(token)[val] * 1000 : 0;
	}

	public actualToken(): boolean {
		this.token  = this.AuthService.getToken();
		this.tokenExp = this.tokenValid(this.token, 'exp');
		this.tokenIat = this.tokenValid(this.token, 'iat');
		this.currentTime = new Date().getTime();
		return  this.currentTime > this.tokenExp  ? false : true;
	}

	public actualTokenMsg(): void {
		this.currentTime > this.tokenExp ? console.log("EXPIRED") : console.log("NOT EXPIRED");
		console.log("\n Current Time : "+new Date(this.currentTime),"\n Token Exp : "+ new Date(this.tokenExp))
	}
}