import { Injectable } from '@angular/core';


@Injectable()
export class loadscriptsService {


	constructor() {

	}


	public loadInstaEmbed(): void {
		this.loadScript("//instagram.com/embed.js","instaEmbed");
	}


	public loadTwitterEmbed(): void {
		this.loadScript("https://platform.twitter.com/widgets.js","twitterEmbed");
	}


	public loadCustomScript(url): void {
		this.loadScript(url,"custom");
	}


	public loadAllEmbed(): void {

		//this.loadInstaEmbed();
		this.loadTwitterEmbed();

	}


	public loadScript(url,id): void {

		this.handleExistanse(id);
		let body = document.getElementsByTagName("head")[0];
		let script = document.createElement('script');
		script.innerHTML = '';
		script.src = url;
		script.id = id;
		script.async = true;
		script.defer = true;
		body.appendChild(script);


	}


	public handleExistanse(id): void {
		if (document.getElementById(id))
			 document.getElementById(id).remove();
	}


}

