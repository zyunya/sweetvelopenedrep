import { Injectable , Inject , PLATFORM_ID  } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router, NavigationStart } from '@angular/router';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable()
export class ConnectionGuard implements CanActivate{

	constructor(
		@Inject(PLATFORM_ID) private platformId: Object,
		public Router: Router,

	) {

	}
	canActivate(): boolean {

	if (isPlatformBrowser(this.platformId)) {

		if (!navigator.onLine) {
			this.Router.navigate(["/networkerror"]);
			return true;
		}

		else {
			return	true;
			}

		}

	};

}


