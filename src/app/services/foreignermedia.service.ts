import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { i18nService } from "../services/i18n.service";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

@Injectable()
export class ForeignermediaService {

	constructor(public http: Http , public i18n : i18nService) {

	}

	public fetchPosts( name , skip , limit , category): Observable<any> {
		return this.http.get(`${environment.API.FOREIGNER_MEDIA_FETCH_POSTS}/${name}/${skip}/${limit}/${category}/none`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public fetchCategories( name ): Observable<any> {
		return this.http.get(`${environment.API.FOREIGNER_MEDIA_FETCH_CATEGORIES}/${name}`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public extractData(res: Response) {
		let body = res.json();
		return body;
	}


	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}

}