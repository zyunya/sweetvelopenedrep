import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { Title } from '@angular/platform-browser';
import { environment } from "../../environments/environment";
import { RemovehtmltagsPipe } from '../pipes/removehtmltags.pipe';

@Injectable()
export class MetatagsService {

  constructor(
	  public meta : Meta , public title : Title,
	  public removeTags : RemovehtmltagsPipe
	  ) { }

  	public addOgMetaTags(data): void {

		var url = data.url || '';
		var category = data.category || "" ;
		var desc = data.desc|| '';
		var image = data.image || '';

		this.meta.updateTag({ property: 'og:title', content: data.title });
		this.meta.updateTag({ property: 'og:description', content:desc.substr( 0 , 70 ) + "..."  });
		this.meta.updateTag({ property: 'og:image', content: image });
		this.meta.updateTag({ property: 'og:url', content: url });
		this.meta.updateTag({ property: 'og:type', content : "website" });


	}


	public addOgMetaTagsStatic( data ) : void {

		var desc = data.desc || "";
		desc = this.removeTags.transform(desc) || "";

		this.meta.updateTag({ property: 'og:title',       content: data.title });
		this.meta.updateTag({ property: 'og:description', content : desc.substr( 0 , 100 ) + " ..... -Продолжение на сайте- " });
		this.meta.updateTag({ property: 'og:image',     content: data.mainimage });
		this.meta.updateTag({ property: 'og:image:secure_url', content: data.mainimage });
		this.meta.updateTag({ property: 'og:image:width',  content: "400" });
		this.meta.updateTag({ property: 'og:image:height', content: "300" });
		this.meta.updateTag({ property: 'og:url',        content : `${environment.HOST}/ch/${data._id}` });
		this.meta.updateTag({ property: 'og:type',       content : "website" });

	}


	public addOgMetaTagsStaticForeigner( data ) : void {

		this.meta.updateTag({ property: 'og:title',       content: data.title.toUpperCase() });
		this.meta.updateTag({ property: 'og:description', content : data.desc.substr( 0 , 100 ) });
		this.meta.updateTag({ property: 'og:image',     content: data.mainimage });
		this.meta.updateTag({ property: 'og:image:secure_url', content: data.mainimage });
		this.meta.updateTag({ property: 'og:image:width',  content: "400" });
		this.meta.updateTag({ property: 'og:image:height', content: "300" });
		this.meta.updateTag({ property: 'og:url',          content : `${environment.HOST}/${data.name}` });
		this.meta.updateTag({ property: 'og:type',        content : "website" });

	}


	public addOgMetaTagsStandard( data ) : void {

		this.meta.updateTag({ property: 'og:title',       content: data.title });
		this.meta.updateTag({ property: 'og:description', content : data.desc });
		this.meta.updateTag({ property: 'og:image',     content: data.mainimage });
		this.meta.updateTag({ property: 'og:image:secure_url', content: data.mainimage });
		this.meta.updateTag({ property: 'og:url',          content : data.url });
		this.meta.updateTag({ property: 'og:type',        content : "website" });

	}



	public setTitle( tit ){
		this.title.setTitle( tit );
	}

}
