
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment'; import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { publishReplay, map, tap, refCount } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';


@Injectable()
export class StaticDataService {


	_categories: Observable<any> = null;

	constructor(public http: Http) {

	}



	/* FETCH CATEGORIES CACHE TRUE */
	public fetchCategories(): Observable<any> {

		if (!this._categories) {

			this._categories = this.http.get(environment.API.STATIC_DATA_CATEGORIES)

					.map((res: Response) => res.json())
					.publishReplay(1)
					.refCount()

		}

		return this._categories;

	}

	public fetchTypos(): Observable<any> {
		return this.http.get(environment.API.STATIC_DATA_TYPOS)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public fetchThemes(): Observable<any> {
		return this.http.get(environment.API.STATIC_DATA_THEMES)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error);
	}

}