
import { Injectable ,Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import * as io from 'socket.io-client';
import * as ss from 'socket.io-stream';
import { environment } from '../../environments/environment';


@Injectable()
export class SocketService {

	public socket : any;


	constructor(@Inject(PLATFORM_ID) private platformId: Object) {

	}

	public connect(url): void {
		if (isPlatformBrowser(this.platformId)) {
			this.socket =  io(url);
		}
	}

	public connectNameSpace(namespace): void {
		if (isPlatformBrowser(this.platformId)) {
			this.socket =  io(`${environment.API_URL}/${namespace}`,{transports: [ 'websocket', 'polling' ] });
		}
	}

	public emit(event,data) {
		if (isPlatformBrowser(this.platformId)) {
			this.socket.emit(event,data);
		}
	}

	public emitStream = (event,file) =>{
		if (isPlatformBrowser(this.platformId)) {

			var stream = ss.createStream();
			ss(this.socket).emit('videostream', stream, file );
			ss.createBlobReadStream(file).pipe(stream);

		}
	}

	public on(event,callback) {
		if (isPlatformBrowser(this.platformId)) {
			this.socket.on(event , (data) => { callback(data) ;})
		}
	}

	public disconnect() {
		if (isPlatformBrowser(this.platformId)) {
			this.socket.disconnect();
		}
	}


}