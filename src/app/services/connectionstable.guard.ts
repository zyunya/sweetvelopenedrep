import { Injectable , Inject , PLATFORM_ID , OnInit  } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Router, NavigationStart } from '@angular/router';
import { isPlatformBrowser, isPlatformServer , Location } from '@angular/common';

@Injectable()
export class ConnectionstableGuard implements CanActivate{

	public isRoot : Boolean;

	constructor(
		@Inject(PLATFORM_ID) private platformId: Object,
		public router: Router,
		public location : Location

	) {

	}


	ngOnInit() {

		this.router.events.subscribe(event => {	});

	}

	canActivate(): boolean {

		if (isPlatformBrowser(this.platformId)) {

			if (navigator.onLine) {
				this.location.back();
			}
			return true;

		}

	};

}
