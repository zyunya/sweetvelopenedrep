import { Injectable, Inject, PLATFORM_ID,EventEmitter } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';


@Injectable()
export class i18nService {

	public timestamp = new Date();
	public allowedLangs: Array<String> = ['en', 'ru'];
	public noLang: string = "en";
	public liveLang : EventEmitter<string> = new EventEmitter();

	constructor(
		public http: Http,
		public translate: TranslateService,
		@Inject(PLATFORM_ID) private platformId: Object
	) {

	}


	public defaultLang(): void {
		if (isPlatformBrowser(this.platformId)) {
			if (this.checkLocalStorage())
				this.translate.setDefaultLang(localStorage.getItem('ch-lang'))
			else
				this.translate.setDefaultLang(this.getClientLang());
		}
	}

	public getDefaultLang(): string {
		if (isPlatformBrowser(this.platformId)) {
			if (this.checkLocalStorage())
				return localStorage.getItem('ch-lang')
			else
				return this.getClientLang();
		}
	}


	public changeLang(lang): void {
		if (isPlatformBrowser(this.platformId)) {
			this.translate.use(lang);
			localStorage.setItem('ch-lang', lang)
			this.emitLang(lang);
		}
	}


	public getClientLang(): string {
		if (isPlatformBrowser(this.platformId)) {
			var lang = navigator.language.substr(0, 2).toLocaleLowerCase();
			if (this.allowedLangs.includes(lang))
				return lang
			else
				return this.noLang;
		}
	}


	public checkLocalStorage(): Boolean {
		if (isPlatformBrowser(this.platformId)) {
			if (localStorage.getItem('ch-lang'))
				return true
			else
				return false;
		}

	}


	public emitLang(lang) : void{
		this.liveLang.emit(lang)
	}


}