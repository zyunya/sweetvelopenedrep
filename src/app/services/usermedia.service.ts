
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment'; import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import { i18nService } from "../services/i18n.service";
import { AuthService } from '../services/auth.service';
import { HelpersService }from '../services/helpers.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

@Injectable()
export class UsermediaService {

	public timestamp = new Date();
	public token: String = this.AuthService.getToken();
	public _desks: Observable<any> = null;
	public _posts: Observable<any> = null;

	constructor(

		public http: Http,
		public i18n: i18nService,
		public AuthService: AuthService,
		public HELPERS : HelpersService

	) {

	}

	public fetchPosts(SKIP, LIMIT, CATEGORY, cache?): Observable<any> {

		/* HANDLE CACHING */
		!cache ? this._posts = null : null;
		var Lang = this.HELPERS.ServerOk() ? 'ru' :  this.i18n.getDefaultLang() ;

		/* CHECK IF CACHE EXISTS OR NOT */
		if (!this._posts) {

			this._posts = this.http.get(`${environment.API.MEDIA_FETCH_POSTS}/${SKIP}/${LIMIT}/${CATEGORY}/${Lang}`)
				.map(this.extractData)
				.publishReplay(1)
				.refCount()

		}

		return this._posts;
	}

	public fetchPostsSaved(SKIP, LIMIT, POSTSARR): Observable<any> {
		return this.http.get(`${environment.API.MEDIA_FETCH_POSTS_SAVED}/${SKIP}/${LIMIT}/${POSTSARR}`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public fetchRelatedPosts(ID, LIMIT, CATEGORY): Observable<any> {
		return this.http.get(`${environment.API.MEDIA_FETCH_RELATED_POSTS}/${ID}/${LIMIT}/${CATEGORY}/${this.i18n.getDefaultLang()}`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public fetchDesks(LIMIT): Observable<any> {

		if (!this._desks) {

			this._desks = this.http.get(`${environment.API.MEDIA_FETCH_DESKS}/${LIMIT}`)
				.map((res: Response) => res.json())
				.publishReplay(1)
				.refCount()

		}

		return this._desks;

	}

	public fetchPost(ID): Observable<any> {
		return this.http.get(`${environment.API.MEDIA_FETCH_POST}/${ID}?tsp=${this.timestamp}`)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public fetchCategories(): Observable<any> {
		return this.http.get(environment.API.STATIC_DATA_CATEGORIES)
			.map(this.extractData)
			.catch(this.handleError);
	}

	public addComment(data): Observable<any> {
		let Head = new Headers({ "Authorization": this.token });
		return this.http.post(environment.API.MEDIA_ADD_COMMENT, data, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public removeComment(id): Observable<any> {
		let Head = new Headers({ "Authorization": this.token });
		return this.http.delete(`${environment.API.MEDIA_REMOVE_COMMENT}/${id}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}


	public removeCommentOwner(id): Observable<any> {
		let Head = new Headers({ "Authorization": this.token });
		return this.http.delete(`${environment.API.MEDIA_REMOVE_COMMENT_OWNER}/${id}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}

	public removeCommentSuperUser(id): Observable<any> {
		let Head = new Headers({ "Authorization": this.token });
		return this.http.delete(`${environment.API.SUPERMODE_REMOVE_COMMENT}/${id}`, { headers: Head })
			.map(this.extractData)
			.catch(this.handleError);
	}


	public fetchComments(id, limit, skip): Observable<any> {
		return this.http.get(`${environment.API.MEDIA_FETCH_COMMENTS}/${id}/${limit}/${skip}`)
			.map(this.extractData)
			.catch(this.handleError);
	}


	public extractData(res: Response) {
		let body = res.json();
		return body;
	}

	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}

}