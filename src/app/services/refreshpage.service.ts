import { Injectable , Inject , PLATFORM_ID , EventEmitter } from '@angular/core';


@Injectable()
export class RefreshpageService {

	public postsAndCategoriesRefresh : EventEmitter<any> = new EventEmitter();

	constructor(

		) {

	}


	public refreshPostsAndCategories() : void {
		this.postsAndCategoriesRefresh.emit();
	}


}