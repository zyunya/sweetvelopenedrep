
import { Injectable, Inject, PLATFORM_ID, EventEmitter } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Injectable()
export class GlobalenvService {


	constructor(@Inject(PLATFORM_ID) private platformId: Object) {

	}


	public Strings: Object | any = {

		title: "SWEETVEL",
		type: "MEDIA",
		mainTitle: "MEDIA",
		email: "hello@sweetvel.com",
		host : "https://sweetvel.com"
	}

	public Images: Object | any = {

		broken: "https://sweetvel.com/assets/img/icons/brokenimg.png",
		main: "https://sweetvel.com/assets/img/mailimg.png"

	}

	public Config: Object | any = {


	}


	public Socials : Object | any = {

		facebook  : "https://facebook.com/sweetvelmedia",
		instagram : "https://instagram.com/sweetvel_ru",
		youtube   : "https://www.youtube.com/channel/UC-Jyb8ez9FhPTbXpJRPfJgQ"

	}


}


export const mainRoutes = {

	main  : {

		foreigner: ":name",
		postpage: ":name/:id"

	},

	 nested :  {

		gallery: "gallery",
		youtube: "youtube",
		places: "places",
		networkerror: "networkerror",
		info : "info",
		options : "options",
		saved : "saved"

	}

}



export const kingRoutes  = {

	main :  {

		signin: "space/page/signin",
		signup: "space/page/signup",
		signupuser: "space/page/signupuser",
		dash: "space/page/welcome",
		finishsignup: "space/page/finishsignup",
		loading: "space/page/loading",

	},

	nested : {

		posthandler: `posthandler`,
		postsuperuser : `postsuperuser`,
		livestream: `livestream`,
		keyhandler: `keyhandler`,
		usershandler: `usershandler`,
		editprofile: `editprofile`,
		editpost: `editpost`,
		list: `list`

	}

}