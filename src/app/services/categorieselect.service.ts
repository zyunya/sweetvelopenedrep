import { Injectable , Inject , PLATFORM_ID , EventEmitter } from '@angular/core';


@Injectable()
export class CategorieselectService {

	public category : EventEmitter<any> = new EventEmitter();

	constructor(

		) {

	}


	public selectCategory(id) : void {
		this.category.emit(id);
	}


}