import { BrowserModule } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { HttpModule, Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppRoutingModule, RoutingComponents } from './app.routing';

import { MyHammerConfig } from './hammerconfig';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { MaterialModule } from './app.material';
import { KingModule } from './kingModule/king.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdsenseModule } from 'ng2-adsense';
import { ShareButtonModule } from '@ngx-share/button';


/* TRANSLATION ENTITIES */
import { i18nModule  } from './i18n.module' ;
import { i18nService } from './services/i18n.service';


/* ENVIRONMENT */
import { environment } from "../environments/environment";


/* COMPONENTS */
import { AppComponent } from './app.component';
import { ModalComponent } from "./modal/modal.component";
import { FooterComponent } from "./sharedcomponents/footer/footer.component";
import { HeaderComponent } from "./sharedcomponents/header/header.component";
import { CommentsComponent } from "./sharedcomponents/comments/comments.component";
import { AdwunitComponent } from './sharedcomponents/adwunit/adwunit.component';


/* TYPO COMPONENTS */
import { MagazineComponent } from "./typocomponents/magazine/magazine.component";
import { BioComponent } from "./typocomponents/bio/bio.component";
import { TypofooterComponent } from "./typocomponents/typofooter/typofooter.component";
import { DesksComponent } from "./typocomponents/desks/desks.component";


//  SERVICES
import { AuthService } from './services/auth.service';
import { HelpersService } from './services/helpers.service';
import { UsermediaService } from './services/usermedia.service';
import { StaticDataService } from './services/staticdata.service';
import { GlobalenvService, mainRoutes } from './services/globalenv.service';
import { CategorieselectService } from './services/categorieselect.service';
import { RefreshpageService } from './services/refreshpage.service';
import { InitfetchService } from './services/initfetchservice';
import { mediaService } from './sharedcomponents/media/media.service';


//  DIALOGS
import { AuthorshipDialog } from "./dialogs/authorship/dialog.authorship";
import { RulesDialog } from "./dialogs/rules/dialog.rules";
import { AddtohomescreenDialog } from "./dialogs/addtohomescreen/dialog.aths";
import { SearchComponent } from "./dialogs/search/search.component";
import { EditorgalleryDialog } from "./dialogs/editorgallery/editorgallery.dialog";
import { MediaComponent } from './sharedcomponents/media/media.component';


//DIRECTIVES
import { PinchZoomDirective } from './sharedcomponents/media/pinchzoom.directive';
import { MediaSourceDirective } from './sharedcomponents/media/mediasource.directive';
import { BrokenimgDirective } from './directives/brokenimg.directive';


@NgModule({

	declarations: [

		ModalComponent,
		AddtohomescreenDialog,
		EditorgalleryDialog,
		AppComponent,
		RoutingComponents,
		AuthorshipDialog,
		RulesDialog,
		SearchComponent,
		MediaComponent,
		DesksComponent,

		/* DIRECTIVES */
		PinchZoomDirective,
		MediaSourceDirective,
		BrokenimgDirective,

		/* SHARED COMPONENTS */
		FooterComponent,
		HeaderComponent,
		AdwunitComponent,

		/* TYPO COMPONENTS */
		MagazineComponent,
		BioComponent,
		TypofooterComponent

	],

	imports: [

		BrowserModule,
		BrowserModule.withServerTransition({ appId: 'universal' }),
		i18nModule,
		//AdsenseModule.forRoot({
		//	adClient: 'ca-pub-6335801035797676',
		//	adSlot: 7259870550,
		//}),
		AppRoutingModule,
		MaterialModule,
		HttpModule,
		HttpClientModule,
		KingModule,
		FormsModule,
		ReactiveFormsModule,
		ShareButtonModule.forRoot(),
		/*
		serviceWorker Regestartion now at main.ts
		ServiceWorkerModule.register('/ngsw-worker.js'),
		*/

	],


	entryComponents: [

		AuthorshipDialog,
		RulesDialog,
		AddtohomescreenDialog,
		EditorgalleryDialog,
		SearchComponent,
		MediaComponent,

	],

	providers: [

		AuthService,
		StaticDataService,
		InitfetchService,
		UsermediaService,
		HelpersService,
		GlobalenvService,
		CategorieselectService,
		RefreshpageService,
		mediaService,
		i18nService
		//{provide: HAMMER_GESTURE_CONFIG, useClass: MyHammerConfig} THIS NEEDED TO INJECT HAMMER EVENTS  OT ANGULAR CORE EVENTS

	],

	bootstrap: [AppComponent],


})

export class AppModule { }




