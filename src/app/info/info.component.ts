import { Component, OnInit } from '@angular/core';
import { HelpersService } from '../services/helpers.service';
import { routerTransition } from "../animations";


@Component({
	selector: 'app-info',
	templateUrl: './info.component.html',
	styleUrls: ['./info.component.scss'],
	animations: [routerTransition()]
})
export class InfoComponent implements OnInit {

	constructor(public HELPERS: HelpersService) { }

	ngOnInit() {
		this.HELPERS.scrollUp();
	}

}
