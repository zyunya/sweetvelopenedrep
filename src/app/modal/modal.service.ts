import { ViewContainerRef, ComponentFactoryResolver, Injectable, Inject, ReflectiveInjector, EventEmitter } from '@angular/core'
import { Observer } from 'rxjs/Observer';



@Injectable()
export class ModalService {

	public rootViewContainer: any;
	public components: Array<any> = [];
	public closeEmitter  = new EventEmitter();
	public modalDataEmitter  = new EventEmitter();
	constructor(

		@Inject(ComponentFactoryResolver) public factoryResolver,
		@Inject(ViewContainerRef) public ViewContainerRef

	) {
		this.setRootViewContainerRef(this.ViewContainerRef); //DEFINE PARENT ELEMENT AS CONTAINER
	}


	public setRootViewContainerRef(viewContainerRef) {
		this.rootViewContainer = viewContainerRef
	}


	public openModal(dynamicComponent) {

			var comp = this.factoryResolver.resolveComponentFactory(dynamicComponent);
			var Component = this.rootViewContainer.createComponent(comp);
			Component.instance.MODAL = Component;
			//this.components.push(dynamicComponent);


	}


	public openModalData(dynamicComponent , data) {

			var comp = this.factoryResolver.resolveComponentFactory(dynamicComponent);
			var Component = this.rootViewContainer.createComponent(comp);
			Component.instance.MODAL = Component;
			Component.instance.MODAL.data = data;
			//this.components.push(dynamicComponent);


	}


	public remove() : any {
		this.closeEmitter.emit("CLOSED");
	}


	public closeModalHandler(refer){
		this.closeEmitter.subscribe(item => refer.destroy() );
	}


	public ModalDataPasser(data) : void{
		this.modalDataEmitter.emit(data);

	}


}



/* OLD METHODS


	public addDynamicComponent(dynamicComponent) {
		//	const factory = this.factoryResolver.resolveComponentFactory(dynamicComponent)
		//	const component = factory.create(this.rootViewContainer.parentInjector)
		//	this.rootViewContainer.insert(component.hostView);


		//	const componentFactory = this.factoryResolver.resolveComponentFactory(dynamicComponent);
		//const component = this.rootViewContainer.createComponent(componentFactory);
		//	this.components.push(component);

		var comp = this.factoryResolver.resolveComponentFactory(dynamicComponent);
		var expComponent = this.rootViewContainer.createComponent(comp);
		expComponent.instance.MODAL = expComponent;
	}

	public removeDynamicComponent(dynamicComponent) {

		// Find the component
		const component = this.components.find((component) => component.instance instanceof dynamicComponent);
		const componentIndex = this.components.indexOf(component);

		if (componentIndex !== -1) {
			// Remove component from both view and array
			this.rootViewContainer.remove(this.rootViewContainer.indexOf(component));
			this.components.splice(componentIndex, 1);
		}
	}


*/