import { Component, OnInit, Inject, Input, Output, EventEmitter, ComponentRef, SimpleChanges, ViewContainerRef, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { trigger, style, state, transition, animate, group } from '@angular/animations';
import { ModalService } from "../modal/modal.service";


@Component({
	selector: 'app-modal',
	templateUrl: './modal.component.html',
	styleUrls: ['./modal.component.scss'],
	providers: [],
	animations: [

		trigger('dialog', [
			transition(':enter', [
				style({
					opacity: 0
				}),
				animate("0.25s ease-in-out", style({
					opacity: 1
				}))
			]),
			transition(':leave', [
				style({
					opacity: 1
				}),
				animate("0.2s ease-in-out", style({
					opacity: 0
				}))
			])
		])

	]

})


export class ModalComponent implements OnInit {
	public modality = true;
	@Input("close") public close = false;
	public _ref: any;
	public scroller: any;


	constructor(
		private _elementRef: ElementRef,
		public renderer: Renderer2,
		public ModalService: ModalService

	) { }


	ngOnInit() {
		this.initCrossPlatformModalHandler();
	}


	ngOnChanges(changes: SimpleChanges) {

	}


	ngOnDestroy() {
		this.destroyCrossPlatformModalHandler();
	}


	public preventClick(): void {
		event.stopPropagation();
	}


	public emitClose() {
		this.modality = !this.modality;
		setTimeout(() => this.ModalService.remove(), 200);
		event.preventDefault();
	}



	public initCrossPlatformModalHandler(): void {

		this.scroller = window.pageYOffset;
		this.renderer.setStyle(document.body, "top", -this.scroller + 'px');

		/*  Fix classes are located in  assets/style/modalcrossplatform.scss  */

		if (navigator.userAgent.toLowerCase().match("iphone | ipad"))
			this.renderer.addClass(document.body, "modalFixerIOS")

		else
			this.renderer.addClass(document.body, "modalFixerOTHERS");

	}


	public destroyCrossPlatformModalHandler(): void {

		this.renderer.removeClass(document.body, "modalFixerIOS");
		this.renderer.removeClass(document.body, "modalFixerOTHERS");
		window.scrollTo(0, this.scroller);

	}



}

