import { HammerGestureConfig } from '@angular/platform-browser';
import { HammerInstance } from "@angular/platform-browser/src/dom/events/hammer_gestures";
import * as Hammer from 'hammerjs';

export class MyHammerConfig extends HammerGestureConfig {
	events: string[] = ['pinch pan']; //necessary for ng2 to recognize "pinch pan" event
	buildHammer(element: HTMLElement): HammerInstance {
		let mc = new Hammer(element, { domEvents: true});
//		let mc = new Hammer(element, { domEvents: true ,  touchAction: "auto"});
		mc.get('pinch').set({ enable: true });
		mc.get('pinch').recognizeWith(mc.get('pan'));
		return mc;
	}
}
