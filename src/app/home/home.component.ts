import {
	Component,
	OnInit, Inject,
	ViewEncapsulation,
	ViewChild,
	ViewContainerRef,
	ViewChildren,
	HostListener,
	QueryList,
	ElementRef,
	AfterViewInit,
	Renderer2,
	PLATFORM_ID
} from '@angular/core';

import { Router, NavigationEnd } from "@angular/router";
import { MetatagsService } from "../services/metatags.service";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HelpersService } from "../services/helpers.service";
import { UsermediaService } from "../services/usermedia.service";
import { RefreshpageService } from '../services/refreshpage.service';
import { GlobalenvService } from '../services/globalenv.service';
import { CategorieselectService } from '../services/categorieselect.service';
import { ModalService } from "../modal/modal.service";
import { SearchComponent } from "../dialogs/search/search.component";
import { i18nService } from '../services/i18n.service';
import { AuthService } from '../services/auth.service';
import { SocketService } from '../services/socket.service';
import { environment } from '../../environments/environment';
import { DOCUMENT } from '@angular/platform-browser';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/catch';
import { trigger, style, state, transition, animate, group } from '@angular/animations';
import { mainRoutes , kingRoutes } from '../services/globalenv.service';


@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
	providers: [MetatagsService, ModalService , SocketService],
	encapsulation: ViewEncapsulation.None,
	animations: []
})


export class HomeComponent implements OnInit {

	public socket;
	public mainRoutes : any = mainRoutes;
	public kingRoutes : any = kingRoutes;

	@ViewChild('navfooter') public navFooter: ElementRef;
	@ViewChild('routeroutlet') public RouterOut: ElementRef;
	@ViewChild('live', { read: ElementRef }) public live: ElementRef;
	@ViewChildren('menuitem', { read: ElementRef }) public menuitem: QueryList<ElementRef>;
	@ViewChild('statusbar') public statusBar: ElementRef;

	public mobileMenuStatus : Boolean = false;
	public statusLock: Boolean = false;
	public clients : any = 1;


	public ogTags: any = {

		title: this.globalEnv.Strings.title,
		subtitle: this.globalEnv.Strings.type,
		desc: this.globalEnv.Strings.mainTitle,
		image: this.globalEnv.Images.main,
		url: this.globalEnv.Strings.host,
		type: "website"

	}


	constructor(

		@Inject(ModalService) public service,
		@Inject(ViewContainerRef) public viewContainerRef,
		@Inject(DOCUMENT) private document: Document,

		public router : Router,
		public META: MetatagsService,
		public HELPERS: HelpersService,
		public dialog: MatDialog,
		public renderer: Renderer2,
		public globalEnv: GlobalenvService,
		public userMedia: UsermediaService,
		public CategorieselectService: CategorieselectService,
		public RefreshpageService : RefreshpageService,
		public i18n : i18nService,
		public AuthService : AuthService,
		public SOCKET : SocketService

	) {


	}


	@HostListener('window:scroll', ['$event'])
	onWindowScroll($event) {
		//this.stickyNavFooterUsual();
	}



	ngOnInit() {

		this.META.setTitle(this.ogTags.title + " - " + this.ogTags.desc);
		this.META.addOgMetaTags(this.ogTags);
		this.stickyNavFooter();
		this.mobileMenuStatus = true;

		this.SOCKET.connectNameSpace('guest');
		this.SOCKET.on('clients' , (data) => { this.clients = data })

	}

	ngAfterViewInit() {

		//this.socket.on('connect',() => { console.log("SOCKETIOCONNECTED")});
		//this.socket.on('disconnect',() => { console.log("SOCKETIODISCONNECTED")});

	}


	ngOnDestroy() {
		this.document.body.style.height = "auto";
		this.SOCKET.disconnect();
	}


	public skk() : void{
		//this.socket.emit('newuser' ,'user')
	}


	public openSearch(): void {
		//this.hideNav(); //USE IT ONLY IF NAV INFO BLOCK IS ACTIVE BY DEFAULT
		this.service.openModal(SearchComponent);
	}


	public fullScreen() {

		var elem = document.body;
		elem.style.overflowY = "scroll";

		if (elem.requestFullscreen) {
			elem.requestFullscreen();
		} else if (elem.webkitRequestFullscreen) {
			elem.webkitRequestFullscreen();
		}

	}


	public setSelected(tar): void {
		this.resetStyles();
		tar.target.style.color = "gold";
		tar.target.style.borderBottom = "2px solid white";
	}


	public resetStyles(): void {
		this.menuitem.forEach((r) => {
			r.nativeElement.style.color = "white";
			r.nativeElement.style.borderBottom = "0px";
		});
	}


	public hideNav(): void {

		this.statusLock = false;
		//this.Navi.nativeElement.style.top = "-110vh";
		this.document.body.style.height = "auto";

	}


	public showNav(): void {

		this.statusLock = true;
		//this.Navi.nativeElement.style.top = "0";
		this.document.body.style.height = "100vh";

	}


	public swipe(): void {
		alert("swipe");
	}


	public stickyNavFooter(): void {

		Observable.fromEvent(this.document.body, 'scroll').subscribe((e: any) => {

			//var offSet =  this.RouterOut.nativeElement.getBoundingClientRect().top;
		//	this.navFooter.nativeElement.classList[ offSet < 50 ? "add" : "remove"]("sticky");

		});

	}


	public stickyNavFooterUsual(){

		var offSet =  this.RouterOut.nativeElement.getBoundingClientRect().top;
		this.navFooter.nativeElement.classList[ offSet < 50 ? "add" : "remove"]("sticky");

	}


	public tapMenuSound(): void {
		var audio = new Audio("../assets/sounds/TWINGY.WAV");
		audio.play();
	}


	public RefreshPage() : void{
		this.RefreshpageService.refreshPostsAndCategories();
	}


}
