import { Component, OnInit, OnChanges, Input, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthorshipDialog } from "../../dialogs/authorship/dialog.authorship";
import { RulesDialog } from "../../dialogs/rules/dialog.rules";
import { HelpersService } from '../../services/helpers.service';
import { GlobalenvService , mainRoutes , kingRoutes } from '../../services/globalenv.service';
import { ModalService } from "../../modal/modal.service";


@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {


	@Input() public col: any;
	@Input() public fontcol: any;
	@ViewChild("footerDOM") public footer: ElementRef;
	public mainRoutes : any = mainRoutes;
	public kingRoutes  : any = kingRoutes;

	constructor(

		public dialog: MatDialog,
		public HELPERS: HelpersService,
		public globalEnv : GlobalenvService,
		public modalservice : ModalService

	) { }

	ngOnInit() {

	}

	ngOnChanges() {
		this.footer.nativeElement.style.background = this.col;
		this.footer.nativeElement.style.color = this.fontcol;
	}


	public openAuthorshipDialog(): void {
		this.modalservice.openModal(AuthorshipDialog)
	}


	public openInstaProfile() : void{
		this.HELPERS.setInstaProfileHref();
	}


	public openFBProfile() : void{
		this.HELPERS.setFBProfileHref();
	}


}
