import { Component, OnInit , Input } from '@angular/core';
import { HelpersService } from "../../services/helpers.service";
import { Location } from '@angular/common';
import { Router, NavigationStart } from "@angular/router";

@Component({
	selector: 'app-mobilemenu',
	templateUrl: './mobilemenu.component.html',
	styleUrls: ['./mobilemenu.component.scss'],
	providers: [HelpersService]
})
export class MobilemenuComponent implements OnInit {


	@Input("back") public backBtn : Boolean;
	public menuAllowedRoutes : String = "kingcastle|ch";
	public previousUrl : string;


	constructor(

		public HELPERS: HelpersService ,
		public location : Location,
		public router   : Router

		) {

			this.router.events.subscribe((event) => {

				if (event instanceof NavigationStart) {
					this.previousUrl = this.location.path();
				}

			})

		}


	ngOnInit() {

	}


	public back() : void {
		if(this.previousUrl)
			this.router.navigate([this.previousUrl]);
	}

}
