import { Component, OnInit , ViewChild , ElementRef , Input} from '@angular/core';

@Component({
	selector: 'ch-snackbar',
	templateUrl: './snackbar.component.html',
	styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

	@ViewChild("chsnackbar") public chsnackbar : ElementRef;
	@Input('autohide') public autohide : Number;


	constructor(chsnackbar: ElementRef) { }


	ngOnInit() {

	}


	public open() : void{
		this.chsnackbar.nativeElement.style.bottom = "0px";
		this.autoHideHandler();
	}


	public close() : void{
		this.chsnackbar.nativeElement.style.bottom = "-60px";
	}


	public autoHideHandler() : void{

		if(this.autohide !== undefined)
			setTimeout( () => this.close() , this.autohide || 2300 );

	}

}
