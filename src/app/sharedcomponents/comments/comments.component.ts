import { Component, OnInit, AfterViewInit, Input, ViewChild, HostListener, ViewChildren, QueryList, ElementRef, ChangeDetectorRef } from '@angular/core';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsermediaService } from "../../services/usermedia.service";
import { mainRoutes } from '../../services/globalenv.service';
import { SocketService } from '../../services/socket.service';
import { routerTransition2 } from "../../animations";


@Component({
	selector: 'ch-comments',
	templateUrl: './comments.component.html',
	styleUrls: ['./comments.component.scss'],
	providers: [SocketService, AuthService],
	animations: [routerTransition2()],
})


export class CommentsComponent implements OnInit {


	@ViewChild("commentsblock") comblock: ElementRef;
	@ViewChild('txtarea') public txtarea: ElementRef;
	@Input('boxid') public boxid: string;
	@Input('boxidowner') public boxidowner: string;
	@Input('writer') public writer: Boolean = false;

	public commentinput: string;
	public commentForm: FormGroup;
	public comments: Array<any> = [];
	public dataLoaded: Boolean = false;
	public commentSended: Boolean = true;


	/* COMMENTS OPTIONS */
	public Limit: number = 10;
	public Skip: number = 0;


	constructor(

		public AuthService: AuthService,
		public fb: FormBuilder,
		public UsermediaService: UsermediaService,
		private cdr: ChangeDetectorRef,
		public SOCKET: SocketService

	) {
		this.commentForm = fb.group({ commentinput: ['', Validators.required] });
	}


	ngOnInit() {

		this.SOCKET.connectNameSpace(`king?token=${this.AuthService.getToken()}`)
		this.SOCKET.emit('joincommentroom', this.boxid);
		this.SOCKET.on('comment', (data) => this.comments.unshift(data));
		this.fetchComments();

	};


	ngAfterViewInit() {

	}

	ngOnDestroy() {
		this.SOCKET.disconnect();
	};


	@HostListener('window:scroll', ['$event'])
	onWindowScroll($event) {

		if (this.comblock.nativeElement.distanceToBottom() < -120 && this.Skip <= this.comments.length) this.fetchComments();

	}

	public addComment(): void {

		if (this.commentForm.status === "VALID") {

			this.commentSended = false;

			let comment = this.commentForm.controls.commentinput.value;

			this.UsermediaService.addComment({ comment: comment, commentbox: this.boxid }).subscribe(

				res => {

					if (res.status === 200)

						this.SOCKET.emit('comment', res.data),
						this.commentSended = true;
						this.commentForm.reset(),
						this.textAreaDefaultSize();

				},

				err => console.error(err)

			)

		}

	}


	/*     REMOVE COMMENTS HANDLERS   */


	public removeCommentNotOwner(id, ind): void {

		this.UsermediaService.removeComment(id).subscribe(

			res => { if (res.status === 200) this.comments.splice(ind, 1); /* console.log(res) */ },

			err => { console.error(err); }

		)

	}


	public removeCommentOwner(id, ind): void {

		this.UsermediaService.removeCommentOwner(id).subscribe(

			res => { if (res.status === 200) this.comments.splice(ind, 1); /* console.log(res) */ },

			err => { console.error(err); }

		)

	}


	public removeCommentSuperUser(id, ind): void {

		this.UsermediaService.removeCommentSuperUser(id).subscribe(

			res => { if (res.status === 200) this.comments.splice(ind, 1); /* console.log(res) */ },

			err => { console.error(err); }

		)

	}


	public removeComment(id, ind) : void{

		if(this.AuthService.userRole() === 'superuser' )
			this.removeCommentSuperUser(id , ind)


		else if(this.AuthService.userID() === this.boxidowner)
			this.removeCommentOwner(id , ind)


		else
			this.removeCommentNotOwner(id , ind)



	}


	/*       FETCH COMMENTS      */

	public fetchComments(): void {

		this.dataLoaded = false;

		this.UsermediaService.fetchComments(this.boxid, this.Limit, this.Skip).subscribe(

			res => { this.comments.addArray(res.data); this.dataLoaded = true; },

			err => console.error(err)

		)

		this.Skip += this.Limit;

	}


	public textareaAutoResize(): void {

		var el = this.txtarea.nativeElement;
			setTimeout( () =>{
				el.style.cssText = 'height:auto;padding:0';
				// for box-sizing other than "content-box" use:
				// el.style.cssText = '-moz-box-sizing:content-box';
				el.style.cssText = 'height:' + el.scrollHeight + 'px';
			})


	}



	public textAreaDefaultSize(): void {

		this.txtarea.nativeElement.style.height = "50px";

	}


}
