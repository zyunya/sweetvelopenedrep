import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { HelpersService } from "../../services/helpers.service";
import { RefreshpageService } from '../../services/refreshpage.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
	selector: 'ch-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})


export class HeaderComponent implements OnInit {


	public statusLock: any;
	public categoriesScroller: number = 0;
	@Input("width") public width: String;
	@Input("title") public title: String;
	@Input("typo") public typo: String;
	@Input("fb") public fb: String;
	@Input("insta") public insta: String;
	@Input("tw") public tw: String;
	@Input("vk") public vk: String;
	@Input("tg") public tg: String;
	@Input("ld") public ld: String;
	@Input("theme") public theme: any;
	@Input("categories") public categories: any;
	@ViewChild("headerdesktop") public headerDesktop : ElementRef;
	@ViewChild('headercategoriescontainer') public categoriesContainer: ElementRef;


	constructor(

		public HELPERS: HelpersService,
		public RefreshpageService: RefreshpageService,
		public router: Router,

	) { }


	ngOnInit() {

		this.headerDesktop.nativeElement.style.width =  `${this.width}%`

	}


	ngOnChanges() {

	}


	public RefreshPage(): void {
		this.RefreshpageService.refreshPostsAndCategories();
	}


	public fetchCategory(id): void {
		this.router.navigate([], { queryParams: { category: id }} );
	};



	public scrollHorizontal(direction): void {
		this.HELPERS.scrollHorizontal(direction , this.categoriesContainer.nativeElement )
	}

}
