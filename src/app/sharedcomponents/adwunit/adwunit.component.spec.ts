import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdwunitComponent } from './adwunit.component';

describe('AdwunitComponent', () => {
  let component: AdwunitComponent;
  let fixture: ComponentFixture<AdwunitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdwunitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdwunitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
