import { Component, Inject, OnInit, ViewEncapsulation, ViewChild, ElementRef, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

@Component({
	selector: 'adwunit',
	templateUrl: './adwunit.component.html',
	styleUrls: ['./adwunit.component.scss'],
	encapsulation: ViewEncapsulation.Emulated
})


export class AdwunitComponent implements OnInit {

	@ViewChild('imgslide') public imgslide: ElementRef;
	public slides = [

		"../../assets/img/adv/adv2.jpg",
		"../../assets/img/adv/adv0.jpg",
		"../../assets/img/adv/adv3.jpg",

	]


	constructor(@Inject(PLATFORM_ID) private platformId: Object, ) { }


	ngOnInit() {
		this.startSlide();
	}


	public startSlide(): void {

		var i = 150;
		var step = 150;

		if (isPlatformBrowser(this.platformId)) {

			setInterval(() => {

				if (i >= step * this.slides.length) i = 0;
				this.imgslide.nativeElement.style.transform = `translateY(${-i}px)`;
				i += step;

			}, 5000);

		}

	}


}
