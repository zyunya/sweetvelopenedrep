import { ViewContainerRef, ComponentFactoryResolver, ElementRef,PLATFORM_ID,Injectable, Inject, ReflectiveInjector, EventEmitter } from '@angular/core'
import { isPlatformBrowser, isPlatformServer } from '@angular/common';


@Injectable()
export class mediaService {


	public rootViewContainer: any;


	constructor(

		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(ComponentFactoryResolver) public factoryResolver,
		@Inject(ViewContainerRef) public ViewContainerRef,

	) {

		this.rootViewContainer  = this.ViewContainerRef;

	}


	public openMediaDynamic(component , data) {

			var comp = this.factoryResolver.resolveComponentFactory( component );
			var Component = this.rootViewContainer.createComponent(comp);
			Component.instance.MEDIA = Component;
			Component.instance.MEDIA.customdata = data;

	}


	public closeMediaHandler(refer){
			setTimeout( () =>	refer.destroy() ,300 );
	}

}