import { Directive, ElementRef, Inject , ViewContainerRef,HostListener } from '@angular/core';
import { mediaService } from './media.service';
import { MediaComponent } from './media.component';

@Directive({
	selector: '[media-source]',
	providers : [mediaService ]
})

export class MediaSourceDirective {

	constructor(

		@Inject(mediaService) public mediaService,
		@Inject(ViewContainerRef) public viewContainerRef,

		){

	}

	@HostListener('click', ['$event'])
	onClick(event) {

		if(event.target.nodeName === "IMG")
			this.mediaService.openMediaDynamic( MediaComponent , event.target.src );

	}


}
