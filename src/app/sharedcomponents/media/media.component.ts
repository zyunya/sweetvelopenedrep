import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { mediaService } from './media.service';
import { trigger, style, state, transition, animate, group } from '@angular/animations';


@Component({
	selector: 'app-media',
	templateUrl: './media.component.html',
	styleUrls: ['./media.component.scss'],
	providers: [mediaService],
	animations: [

		trigger('gallery', [
			transition(':enter', [
				style({
					opacity: 0
				}),
				animate("0.3s ease-in-out", style({
					opacity: 1
				}))
			]),
			transition(':leave', [
				style({
					opacity: 1
				}),
				animate("0.3s ease-in-out", style({
					opacity: 0
				}))
			])
		])

	]
})
export class MediaComponent implements OnInit {

	MEDIA: any;
	public imgActive: String;
	public GalleryVisibility = true;
	public scroller: any;

	constructor(
		public router: Router,
		private route: ActivatedRoute,
		public mediaService: mediaService,
		public renderer: Renderer2
	) {

	}

	ngOnInit() {

		this.initCrossPlatformModalHandler();
		//this.route.params.subscribe( params => this.imgActive = params.img );
		this.imgActive = this.MEDIA.customdata;

	}


	ngOnDestroy() {
		this.destroyCrossPlatformModalHandler();
	}


	public closeMedia(): void {
		this.GalleryVisibility = false;
		this.mediaService.closeMediaHandler(this.MEDIA);
	}


	public initCrossPlatformModalHandler(): void {

		this.scroller = window.pageYOffset;
		this.renderer.setStyle(document.body, "top", -this.scroller + 'px');

		if (navigator.userAgent.toLowerCase().match("iphone | ipad"))
			this.renderer.addClass(document.body, "modalFixerIOS")

		else
			this.renderer.addClass(document.body, "modalFixerOTHERS");

	}


	public destroyCrossPlatformModalHandler(): void {

		this.renderer.removeClass(document.body, "modalFixerIOS");
		this.renderer.removeClass(document.body, "modalFixerOTHERS");
		window.scrollTo(0, this.scroller);

	}

}
