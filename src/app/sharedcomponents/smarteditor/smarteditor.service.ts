
import { Injectable, Inject, Renderer2, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { MatSnackBar } from '@angular/material';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';

@Injectable()
export class SmarteditorService {

	constructor(

		public http: Http,
		public renderer: Renderer2,
		public snackbar : MatSnackBar,
		@Inject(PLATFORM_ID) private platformId: Object

	) {

	}


	public youtubeFetch(url): Observable<any> {
		return this.http.post(environment.API.YOUTUBE_FETCH, { urlstr: url })
			.map(this.extractData)
			.catch(this.handleError);
	}


	public extractData(res: Response) {
		let body = res.json();
		return body;
	}


	public handleError(error: Response) {
		let errMsg = (error.status ? `${error.status} - ${error.statusText}` : 'Server error')
		return Observable.throw(error.json());
	}


	//// HANDLER HELPERS////////////////

	public doesImageExist(url) {

		return new Promise((resolve, reject) => {

			var imageData = new Image();

			imageData.onload = () => resolve(url);

			imageData.onerror = () => reject("Wrong Image");

			imageData.src = url;

		})

	}


	//////////////////////////////// HELPERS /////////////////////////////////////




	public paste(event): void {
		//MANUALLY PASTE CLIPBOARD DATA
		event.preventDefault();
		// get text representation of clipboard
		var text = event['clipboardData'].getData("text/plain");

		// insert text manually
		if (isPlatformBrowser(this.platformId))
			document.execCommand("insertHTML", false, text);

	}

	//////// NODES OPERATIONS ///////////////////



	public getChildNumber(node, parent) {
		var CHILDREN = parent.childNodes;
		//return Array.prototype.indexOf.call(node.parentNode.childNodes, node);
		return Array.from(CHILDREN).indexOf(node)
	}


	public removeSpecificNode = (el, index) => {

		if (el !== null) {
			var children = el.childNodes;
			if (children.length > 0) {
				el.removeChild(children[index]);
			}
		}

	}

	/* EXTRA OPTIONS */

	public extraOptionsTextStyle(style): void {

		if (isPlatformBrowser(this.platformId)) {

			document.designMode = "on";
			document.execCommand(style, false, null);
			document.designMode = "off";

		}

		event.preventDefault();
		event.stopPropagation();

	}


	public extraOptionsTextColor(color): void {

		if (isPlatformBrowser(this.platformId)) {

			document.designMode = "on";
			document.execCommand("foreColor", false, color);
			document.designMode = "off";

		}

		event.preventDefault();
		event.stopPropagation();

	}



	public transformTo(tag): void {

		if (isPlatformBrowser(this.platformId)) {
			document.execCommand('formatBlock', true, `${tag}`);
		}

		event.preventDefault();
		event.stopPropagation();

	}




	public transformToLink(): void {

		if (isPlatformBrowser(this.platformId)) {

			var txt = window.prompt("Link", "");
			if (txt !== null)
				document.execCommand('createLink', true, txt);

		}

		event.preventDefault();
		event.stopPropagation();

	}

	public transformToImageFromLink(): void {

		if (isPlatformBrowser(this.platformId)) {

			var img = window.getSelection();

			this.doesImageExist(img).then(

				data => {

					document.execCommand("insertHTML", true,
								`<figure>
								<img src =  '${data}' >
								IMAGE
								</figure>
								<p><br></p>`
							);

				},

				err =>  this.snackbar.open( err , 'Ok', {duration: 2500})
			)

		}

	}


	public transformToDevider() : void{

		if (isPlatformBrowser(this.platformId)) {
			document.execCommand("insertHTML" ,true , `<hr>`);
		}

	}


	public transformToImage(img): void {


		if (isPlatformBrowser(this.platformId)) {

			var currentNode = window.getSelection().anchorNode;
			var imglink = img;

			try {currentNode.parentElement.removeChild(currentNode) } catch(err) { console.log(err)};

				this.doesImageExist(imglink).then(

					data => {

							document.execCommand("insertHTML", true,
								`<figure>
								<img src =  '${imglink}' >
								IMAGE
								</figure>
								<p><br></p>`
							);

					},

					err => alert(err)

				)

		}

		event.preventDefault();
		event.stopPropagation();

	}


	public extraOptionsMakeList(): void {

		if (isPlatformBrowser(this.platformId)) {

			document.execCommand("insertOrderedList", false, "false")

		}

		event.preventDefault();
		event.stopPropagation();

	}


	/// EDITABLE BLOCK MAIN FORMATTIN ///

	public mainFormatting(editable): void {

		if (isPlatformBrowser(this.platformId)) {

			this.handleFormatting(editable);

		}

	};


	public handleFormatting(parent): void {

		parent.addEventListener('keydown', (e) => {

			var currentNode = window.getSelection().anchorNode;

			if (e.keyCode === 13) {


				if (currentNode.nodeName === "#text" && currentNode.parentElement.nodeName === "FIGURE") {

					try{

						e.preventDefault();
						var paragraph = document.createElement("p");
						paragraph.innerHTML = "<br>";
						parent.insertBefore(paragraph, currentNode.parentElement.nextSibling);
						window.getSelection().collapse(paragraph, paragraph.textContent.length);

					}

					catch(err) { console.log(err) }

				}

				else if (currentNode.nodeName === "FIGURE") e.preventDefault();

				else setTimeout( () => document.execCommand('formatBlock', false, "P"),1000 );

			}

			//if(e.keyCode === 8 && currentNode.nodeName === "FIGURE" && currentNode.textContent == "")
					//parent.removeChild(currentNode)

		})


	}


	public handleNextLine(parent): void {

		parent.addEventListener('keypress', (e) => {

			var currentNode = window.getSelection().anchorNode;

			if (e.keyCode == '13') {

				var paragraph = document.createElement("p");

				if (currentNode.nodeName === "FIGURECAPTION") {

					e.preventDefault();
					parent.insertBefore(paragraph, currentNode.parentElement.nextSibling);
					setTimeout(function () { paragraph.focus(); console.log(paragraph) }, 1500);

				}

				else if (currentNode.nodeName === "#text" && currentNode.parentElement.nodeName === "FIGURECAPTION") {

					e.preventDefault();
					parent.insertBefore(paragraph, currentNode.parentElement.parentElement.nextSibling);

				}

				else if (currentNode.nodeName === "FIGURE")
					e.preventDefault();

				else
					document.execCommand('formatBlock', false, 'p');


			}

		});
	};


	public handleFigureCaptionInput(parent): void {

		parent.addEventListener('keydown', (e) => {

			var currentNode = window.getSelection().anchorNode;

			if (
				currentNode.nodeName === "FIGURECAPTION" ||
				(currentNode.nodeName === "#text" &&
					currentNode.parentElement.nodeName === "FIGURECAPTION")
			) {

				if (e.keyCode === 8 && currentNode.textContent.length < 1) event.preventDefault();

			}

		})

	};



}
