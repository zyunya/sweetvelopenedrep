# SMART(FLASH) EDITOR COMPNENT

# DEPENDENCIES

`ANGULAR  RENDERER2`

`assets/style/helpers/scss`

`in order to use this component in other projects include also style dependencies`

#FORM CONTROLS

`supports formControlName attribute`

`you can pass default editors data with  [defaultdata] use it with *ngIf in order to pass async data`

#EXPAMPLE

`<smarteditor *ngIf="asyncdata" [defaultdata] = "asyncdata" formControlName="myformcontrolfield" ></smarteditor>`