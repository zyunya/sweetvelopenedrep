import { Component, OnInit, Inject, PLATFORM_ID, QueryList, ViewContainerRef, Renderer2, ViewChild, forwardRef, Input, ViewChildren, ElementRef, OnChanges, HostListener, SimpleChange, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { SmarteditorService } from './smarteditor.service';
import { SnackbarComponent } from "../../sharedcomponents/snackbar/snackbar.component";
import { loadscriptsService } from "../../services/loadscripts.service";
import { i18nService } from '../../services/i18n.service';


export const EPANDED_TEXTAREA_VALUE_ACCESSOR: any = {
	provide: NG_VALUE_ACCESSOR,
	useExisting: forwardRef(() => SmarteditorComponent),
	multi: true,
};

@Component({
	selector: 'smarteditor',
	templateUrl: './smarteditor.component.html',
	providers: [

		EPANDED_TEXTAREA_VALUE_ACCESSOR,
		SmarteditorService,
		loadscriptsService,

	],
	styleUrls: ['./smarteditor.component.scss'],

})
export class SmarteditorComponent implements ControlValueAccessor {

	@ViewChild("editable") public editable: ElementRef;
	@ViewChild("shortmenu") public shortmenu: ElementRef;
	@ViewChild("elementsmenu") public elementsmenu: ElementRef;
	public textStyleMenu: Boolean;

	/* SNACK BAR LIST */
	@ViewChildren(SnackbarComponent) public snackBarElems: QueryList<SnackbarComponent>;
	@ViewChild("result") public result: ElementRef;

	@Output() public dataemit: EventEmitter<any> = new EventEmitter();
	@Input("defaultdata") public defaultEditorData: any = "";
	@Input("imgdata") public imgData: Array<any> = [];

	public galleryImageCaptionStatus: Boolean = false;
	public elementsArr: Array<any> = [];
	public HTMLEditor: Boolean;
	public inputDeleteIndicator: Boolean = false;
	public currentNode: any = 0;
	public infoOutput: String;
	public recoveryInfo: Boolean = false;
	public finalData: any;
	public editotInfoStarter: Boolean;
	public currentNodeName : String;

	public defaultEditorDataStore: any;
	public notEditableElements: Array<string> = ["IMG", "FIGURE", "SECTION"];


	constructor(

		private renderer: Renderer2,
		public smartservice: SmarteditorService,
		public scriptsService: loadscriptsService,
		public i18n : i18nService,
		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(ViewContainerRef) public viewContainerRef

	) {

	}

	onChange;
	onTouched;

	writeValue(value: any): void {

		this.finalData = value;

	}

	registerOnChange(fn: any): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}

	change(val) {

		this.onChange(val);
		this.onTouched(val);

	}


	ngOnInit() {

	}


	ngAfterViewInit() {

		this.editable.nativeElement.innerHTML = this.defaultEditorData;
		this.defaultEditorDataStore = this.defaultEditorData;
		this.smartservice.mainFormatting(this.editable.nativeElement);
		this.scriptsService.loadAllEmbed();

	}


	ngOnChanges(changes: SimpleChanges) {

		//	this.editable.nativeElement.innerHTML = this.defaultEditorData;

	}


	// EDITABLE GET FOCUS //

	public editableGetFocusClick(e): void {

		this.currentNodeName = e.target.nodeName;
		this.renderer.setStyle(document.body, "position", "absolute");
		this.shortmenu.nativeElement.style.opacity = "1";
		this.shortmenu.nativeElement.style.zIndex = "5000";
		this.shortmenu.nativeElement.style.top = e.target.offsetTop - 60 + "px";

	}


	public editableGetFocusNextline(e): void {

		if (isPlatformBrowser(this.platformId)) {

			var offset = window.getSelection().focusNode['offsetTop'];
			this.shortmenu.nativeElement.style.top = offset - 60 + "px";

		}

	}


	public removeMobileMenu(): void {

		this.renderer.setStyle(document.body, "position", "relative");
		this.shortmenu.nativeElement.style.opacity = "0";
		this.shortmenu.nativeElement.style.zIndex = "-1";

	}





	public getCurrentNode(e): void {

		//if(e.target.nodeName === "IMG")  this.editable.nativeElement.blur();
		this.currentNode = this.smartservice.getChildNumber(e.target.parentNode, this.editable.nativeElement);

		}


	/////////////////////////////YOUTUBE  HANDLER////////////


	public makeYoutube(index): void {

		var link = this.renderer.createElement('div');

		link.classList.add("image-link-block");
		link.setAttribute("placeholder", "+ Youtube Link");
		this.editable.nativeElement.insertBefore(link, this.editable.nativeElement.childNodes[index]);
		this.listenYoutubeLink(link, index);

	}


	public listenYoutubeLink(link, index): void {

		link.addEventListener("paste", () => {

			var clipBoardData = event['clipboardData'].getData("text/plain");
			setTimeout(() => link.textContent = "Loading...");

			this.smartservice.youtubeFetch(clipBoardData).subscribe(

				res => {

					if (res.status === 200 && res.data)

						link.parentNode.removeChild(link),
							this.createiFrameNode(res.data.embedurl, index),
							this.refreshActualData();
				},

				err => { this.showInfo(err.statusText), link.innerHTML = ""; console.error(err) }

			)
		})

	}


	public createiFrameNode(url, index): void {

		var iframe = this.renderer.createElement("iframe");
		var wrapper = this.renderer.createElement("section");
		iframe.src = url;
		wrapper.appendChild(iframe);
		this.editable.nativeElement.insertBefore(wrapper, this.editable.nativeElement.childNodes[index]);
		this.videoOptionsHandler(wrapper);
		iframe.setAttribute("allowfullscreen", "");


	}


	public videoOptionsHandler(video): void {

		video.addEventListener('click', (e) => {

			this.editable.nativeElement.blur();
			this.snackBarElems.toArray()[1].open()
			this.currentNode = this.smartservice.getChildNumber(video, this.editable.nativeElement);
			e.stopPropagation();
			e.preventDefault();

		});

	}



	////////// IMAGE OPTIONS /////////////////////////////////////////////

	public originalImgOption(node): void {

		if(this.editable.nativeElement.childNodes[node].nodeName === "FIGURE"){

			this.editable.nativeElement.childNodes[node].classList.add("small-img");
			//this.snackBarElems.toArray()[0].close();
			this.refreshActualData();

		}

		event.stopPropagation();
		event.preventDefault();

	}

	public defaultImgOption(node): void {

		if(this.editable.nativeElement.childNodes[node].nodeName === "FIGURE"){

			this.editable.nativeElement.childNodes[node].classList.remove("small-img");
			this.refreshActualData();

		}

		event.stopPropagation();
		event.preventDefault();

	}

	public deleteOption(node): void {

		console.log(this.editable.nativeElement.childNodes[node].nodeName )

		if(this.editable.nativeElement.childNodes[node].nodeName === "FIGURE" ){

			this.smartservice.removeSpecificNode(this.editable.nativeElement, node)
			this.refreshActualData();

		}

		event.stopPropagation();
		event.preventDefault();


	};



	//////////////////////// VIDEO OPTIONS /////////////////////////////////////////

	public deleteVideoOption(node): void {

		this.smartservice.removeSpecificNode(this.editable.nativeElement, node)
		this.snackBarElems.toArray()[1].close();
		this.refreshActualData();

	};




	public showResult(): void {

		this.HTMLEditor = !this.HTMLEditor;

		setTimeout(() => {

			var Result = this.result.nativeElement;
			var data = this.editable.nativeElement.innerHTML
			Result.setAttribute("contenteditable", "true");
			//HTML.textContent = data.replace(/(\/h2>|\/p>)/g, "$&" + "\n\r");
			Result.textContent = data;

		});
		event.preventDefault();
		event.stopPropagation();

	}


	public handleHTMLeditor(): void {
		this.change(this.result.nativeElement.textContent.trim())
	}


	//LISTENING TO INPUT EVENT IN ORDER TO REFRESH DATA//
	public emitEditorChanges(): void {

		var data = this.editable.nativeElement.innerHTML.trim();
		this.finalData = data;

	}



	public paste(event): void {
		/* LOAD TWITTER NAD INSTA EMBED API */
		this.scriptsService.loadAllEmbed();
		/* FILTER CLIPBOARD TEXT */
		this.smartservice.paste(event);
		this.refreshActualData();
	}


	public listenChanges(element): void {

		const observer = new MutationObserver((mutations) => {
			mutations.forEach((mutation) => this.dataemit.emit(mutation));
		});

		observer.observe(element, {

			attributes: true,
			childList: true,
			characterData: true

		});

	}


	public showInfo(text): void {

		this.infoOutput = this.i18n.translate.instant(text);
		this.snackBarElems.toArray()[0].open();

	};


	public refreshActualData(): void {
		this.change(this.editable.nativeElement.innerHTML.trim())
	};


	public toggleFixedElementsMenu(): void {
		this.elementsmenu.nativeElement.classList.toggle("fixed")
	};


	/* EXTRA OPTIONS */


	public extraOptionsTextStyle(style): void {
		this.smartservice.extraOptionsTextStyle(style);
		this.refreshActualData();
	};


	public extraOptionsTextColor(color): void {
		this.smartservice.extraOptionsTextColor(color);
		this.refreshActualData();
	};


	public transformTo(tag): void {

			document.execCommand('formatBlock', true, `${tag}`);
			this.refreshActualData()

			event.preventDefault();
			event.stopPropagation();
	};




	public transformToLink(): void {
		this.smartservice.transformToLink();
		this.refreshActualData();
	}


	public transformToImage(img): void {

		//var elem = window.getSelection().getRangeAt(0).startContainer.parentNode;
		this.smartservice.transformToImage(img);
		this.refreshActualData();
		this.galleryImageCaptionStatus = false;
		event.preventDefault();

	}


	public transformToImageFromLink() : void{

		this.smartservice.transformToImageFromLink();
		this.refreshActualData();
		event.preventDefault();

	}


	public transformToDevider() : void{

		this.smartservice.transformToDevider();
		this.refreshActualData();
		event.preventDefault();

	}


	public openGalleryImgCaption(): void {
		this.galleryImageCaptionStatus = true;
		event.preventDefault();
	}

	public extraOptionsMakeList(): void {
		this.smartservice.extraOptionsMakeList();
		this.refreshActualData();
	}


	public handleFigureCaption(e): void {
		console.log(e.target)
	}


	///////////////////////////////// TEMP STORAGE METHODS //////////////////////


	public setTempData(): void {

		var data = this.editable.nativeElement.innerHTML;
		localStorage.setItem("flasheditorTempData", data);
		this.showInfo("saved");

	};


	public getTempData(): void {

		// SAVE CURRENT DATA IN ORDER TO RECOVER IT IN  getdefaultData METHOD
		this.defaultEditorDataStore = this.editable.nativeElement.innerHTML;
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		try {

			var temp = localStorage.getItem("flasheditorTempData");
			if (temp) {
				this.editable.nativeElement.innerHTML = temp;
				this.recoveryInfo = true;
				this.refreshActualData();
			}

			else
				this.showInfo("storageisempty");

		} catch (err) { console.log(err) }

	}


	public getdefaultData(): void {

		this.editable.nativeElement.innerHTML = this.defaultEditorDataStore;
		this.refreshActualData();

	};


}
