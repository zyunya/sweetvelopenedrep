import { Component, OnInit ,Output , EventEmitter } from '@angular/core';

@Component({

	selector: 'app-emoji',
	templateUrl: './emoji.component.html',
	styleUrls: ['./emoji.component.scss']
})

export class EmojiComponent implements OnInit {

	public emojiBlock: Boolean;
	@Output('emojiEvent') public emitEmoji= new EventEmitter<String>();


	public EMOJI: Array<String> = [

		"&#128565;", "&#128564;", "&#128563;", "&#128562;", "&#128561;",
		"&#128560;", "&#128559;", "&#128558;", "&#128557;", "&#128556;",
		"&#128555;", "&#128554;", "&#128553;", "&#128552;", "&#128551;",
		"&#128542;", "&#128541;", "&#128540;", "&#128539;", "&#128538;",
		"&#128537;", "&#128536;", "&#128535;", "&#128534;", "&#128533;",
		"&#128532;", "&#128531;", "&#128530;", "&#128529;", "&#128528;",
		"&#128527;", "&#128526;", "&#128525;", "&#128524;", "&#128523;",
		"&#128522;", "&#128521;", "&#128520;", "&#128519;", "&#128518;",
		"&#128517;", "&#128516;", "&#128515;", "&#128514;", "&#128513;",
		"&#128512;", "&#128516;", "&#128515;", "&#128514;", "&#128513;",
		"&#128576;", "&#128575;", "&#128574;", "&#128573;", "&#128572;",
		"&#128571;", "&#128570;", "&#128569;", "&#128568;", "&#128567;",

	]

	constructor() { }

	ngOnInit() {

	}

	public chooseEmoji(e): void {
		this.emitEmoji.emit(e.target.innerHTML);
	}

}
