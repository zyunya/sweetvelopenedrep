# EMOJI CUSTOM COMPONENT

# DEPENDENCIES
`ANGULAR MATERIAL SNACKBAR`

#EVENT TRIGGERS

`emojiEvent` `triggers when you click to emoji icon and return emoji HTML entity`

`EXAMPLE`

`<app-emoji (emojiEvent)="yourMethod($event)></app-emoji>`

`yourMethod( e ){ console.log(e) }`