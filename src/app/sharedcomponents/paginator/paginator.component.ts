import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { LocationStrategy } from '@angular/common';

@Component({
	selector: 'ch-paginator',
	templateUrl: './paginator.component.html',
	styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {


	@Input('pagestep') public pageStep: Array<any>;
	@Input('defaultlimit') public defaultLimit: any;
	public currentPage: any = 0;
	public currentPageFormatted: any = 1;
	public currentUrl: String;


	constructor(
		public route: ActivatedRoute,
		public router: Router,
		private locStrategy: LocationStrategy
	) {
		this.currentUrl = this.router.url;

	}


	ngOnInit() {

		this.route.queryParams.subscribe(params => {

			this.currentPage = params.page;
			this.currentPageFormatted = (this.currentPage / this.defaultLimit) + 1 || 1;

		});

	}


	public setPage(page, pagepoint): void {

		this.currentPage = page;
		this.currentPageFormatted = pagepoint;
		this.router.navigate([ this.getUrl() ], { queryParams: { page: this.currentPage }, queryParamsHandling: 'merge' });

	};


	public getUrl(): any {

		const url = this.locStrategy.path();
		const urlArray = url.split('?');
		return urlArray[0];

	}

}
