import { Component, OnInit, Input , ElementRef , ViewChild } from '@angular/core';

@Component({
	selector: 'ch-typofooter',
	templateUrl: './typofooter.component.html',
	styleUrls: ['./typofooter.component.scss']
})
export class TypofooterComponent implements OnInit {


	@Input() public col: any;
	@Input() public fontcol: any;
	@ViewChild("footerDOM") public footer: ElementRef;


	constructor() { }

	ngOnInit() {
	}


	ngOnChanges() {
		this.footer.nativeElement.style.background = this.col;
		this.footer.nativeElement.style.color = this.fontcol;
	}

}
