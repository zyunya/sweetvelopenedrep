import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypofooterComponent } from './typofooter.component';

describe('TypofooterComponent', () => {
  let component: TypofooterComponent;
  let fixture: ComponentFixture<TypofooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypofooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypofooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
