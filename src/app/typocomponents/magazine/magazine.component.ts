import { Component, OnInit, Input } from '@angular/core';
import { routerTransition2 } from "../../animations";

@Component({
	selector: 'typo-magazine',
	templateUrl: './magazine.component.html',
	styleUrls: ['./magazine.component.scss'],
	animations: [routerTransition2()]
})
export class MagazineComponent implements OnInit {

	@Input("data") public postsData: any;
	@Input("desc") public renderDesc: Boolean = true;

	constructor() { }

	ngOnInit() {
	}

}
