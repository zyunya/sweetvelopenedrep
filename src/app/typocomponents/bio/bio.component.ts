import { Component, OnInit , Input } from '@angular/core';

@Component({
	selector: 'ch-bio',
	templateUrl: './bio.component.html',
	styleUrls: ['./bio.component.scss'],
	
})
export class BioComponent implements OnInit {

	@Input('data') public postsData : any;

	constructor() { }

	ngOnInit() {
	}

}
