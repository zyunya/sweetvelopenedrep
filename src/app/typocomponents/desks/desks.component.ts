import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UsermediaService } from '../../services/usermedia.service';
import { mainRoutes } from '../../services/globalenv.service';


@Component({
	selector: 'ch-desks',
	templateUrl: './desks.component.html',
	styleUrls: ['./desks.component.scss'],
	encapsulation: ViewEncapsulation.Emulated,
})


export class DesksComponent implements OnInit {


	public mainRoutes : any =  mainRoutes;
	public Desks : Array<any> = [];


	constructor(public MEDIA: UsermediaService) { }


	ngOnInit() {
		this.fetchDesks(10);
	}


	public fetchDesks(limit) : void{

		this.MEDIA.fetchDesks(limit).subscribe(

			res => { this.Desks = res.data; /* console.log(res); */},

			err => console.error(err)

		)
	}


}
