import { Directive, ElementRef, Inject ,OnInit, HostListener } from '@angular/core';
import { GlobalenvService } from '../services/globalenv.service';


@Directive({
	selector: '[brokenimg-handler]',
	providers : [ ]
})

export class BrokenimgDirective implements OnInit{


	constructor(
		private hostElem : ElementRef,
		private GlobalenvService : GlobalenvService
		){

	}


	ngOnInit() {

	}


	ngAfterViewInit() {
		this.getHostmages();
	}


	@HostListener('click', ['$event'])
	onClick(event) {

	}


	public getHostmages() : void{

		var host = this.hostElem.nativeElement.querySelectorAll("IMG");

		for( var i = 0; i < host.length ; i++){

			if(host[i].tagName === "IMG"){

				host[i].onerror = (e) =>e.target.src = this.GlobalenvService.Images.broken;
			}
		}
	}




}
