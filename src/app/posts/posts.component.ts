import { Component, OnInit, ViewEncapsulation, HostListener, ViewChild, ElementRef } from '@angular/core';
import { UsermediaService } from '../services/usermedia.service';
import { DataService } from '../kingModule/services/data.service';
import { CategorieselectService } from '../services/categorieselect.service';
import { routerTransition } from "../animations";
import { HelpersService } from "../services/helpers.service";
import { StaticDataService } from "../services/staticdata.service";
import { RefreshpageService } from '../services/refreshpage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { i18nService } from "../services/i18n.service";


@Component({
	selector: 'app-posts',
	templateUrl: './posts.component.html',
	styleUrls: ['./posts.component.scss'],
	providers: [DataService],
	encapsulation: ViewEncapsulation.None,
	animations: [routerTransition()]
})


export class PostsComponent implements OnInit {


	@ViewChild('categoriescontainer') public categoriesContainer: ElementRef;
	public pageStep: Array<any>;
	public currentPage: any = 0;
	public currentPageFormatted: any = 1;
	public defaultSkip: Number = 0;
	public defaultLimit: any = 60;
	public currentCategory: Number | String = "ALL";
	public postsData: any;
	public Categories: any;
	public postsDataLoaded: Boolean = false;
	public categories: any;
	public categoriesScroller: number = 0;
	public subscription;


	constructor(

		public MEDIA: UsermediaService,
		public DATA: DataService,
		public CategorieselectService: CategorieselectService,
		public RefreshpageService: RefreshpageService,
		public HELPERS: HelpersService,
		public route: ActivatedRoute,
		public router: Router,
		public i18n: i18nService,
		public staticData: StaticDataService

	) {

	}


	ngOnInit() {

		/* FETCH AND CACHE INITIAL DATA -- fetchPosts To Cache Response use TRUE if not use FALSE*/
		this.fetchPosts(true);
		this.fetchCategories();

		/* CHANGE LANG DETECTION */
		this.i18n.liveLang.subscribe(res => {  this.fetchPosts()  });

		/* CHANGE QUERY PARAMS DETECTION */
		this.route.queryParams.subscribe(params => this.handleCategoryChange(params));

		/* LISTEN TO REFRESH PAGE EVENT */
		this.RefreshpageService.postsAndCategoriesRefresh.subscribe(data => this.Refresh());

		/* LISTEN TO CATEGORY SELECT AND FETCH */
		//this.CategorieselectService.category.subscribe(data => this.fetchPosts(this.defaultSkip, data));

		/* SCROLL TO ON COMPONENT LOAD */

	}


	public fetchPosts(cache?) {

		this.postsDataLoaded = false;

		this.MEDIA.fetchPosts(this.currentPage, this.defaultLimit, this.currentCategory, cache).subscribe(

			res => {

				this.postsData = res.data;
				this.pageStep = this.pageCounter(res.count);
				this.postsDataLoaded = true;
				this.HELPERS.scrollUp();
				//console.log(res);

			},

			err => { console.log(err); this.postsDataLoaded = true; }
		)

	}


	public handleCategoryChange(params): void | boolean {

		if (Object.keys(params).length < 1) return false;
		this.postsDataLoaded = false;
		this.currentCategory = params.category || 'ALL';
		this.currentPage = params.page;
		this.currentPageFormatted = (this.currentPage / this.defaultLimit) + 1 || 1;
		this.fetchPosts(false);

	}


	public fetchCategories(): void {

		this.subscription = this.staticData.fetchCategories().subscribe(

			res => this.categories = res,

			err => console.error(err)

		)

	}


	public setPage(page, pagepoint): void {

		this.currentPage = page;
		this.currentPageFormatted = pagepoint;
		this.router.navigate([''], { queryParams: { page: this.currentPage }, queryParamsHandling: 'merge' });

	};


	public pageCounter(counter): Array<any> {
		var arr = [];
		for (var i = 0; i < counter / this.defaultLimit; i++) arr.push(i);
		return arr;
	};


	public scrollHorizontal(direction): void {

		var scrollOffset = 200;

		if (direction === "right") {

			if (this.categoriesScroller < (this.categoriesContainer.nativeElement.offsetWidth / 2)) {
				this.categoriesScroller += scrollOffset;
				this.categoriesContainer.nativeElement.scroll({ top: 0, left: this.categoriesScroller, behavior: 'smooth' })
			}
		}
		else if (direction === "left") {

			if (this.categoriesScroller > 0) {
				this.categoriesScroller -= scrollOffset,
					this.categoriesContainer.nativeElement.scroll({ top: 0, left: this.categoriesScroller, behavior: 'smooth' })
			}

		}


	}


	public fetchCategory(id): void {
		this.router.navigate([], { queryParams: { category: id } });
	};


	public async Refresh() {
		this.fetchCategories();
		this.fetchPosts(false);
	};


	public checkData(arr): Boolean {
		if (arr.length === 0)
			return false;
		else
			return true;
	}



}
