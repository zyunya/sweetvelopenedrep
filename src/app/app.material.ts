import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
	MatButtonModule,
	MatCheckboxModule,
	MatRadioModule,
	MatToolbarModule,
	MatSelectModule,
	MatIconModule,
	MatCardModule,
	MatInputModule,
	MatFormFieldModule,
	MatProgressSpinnerModule,
	MatTabsModule,
	MatPaginatorModule,
	MatMenuModule,
	MatChipsModule,
	MatTooltipModule,
	MatSlideToggleModule,
	MatSnackBarModule,
	MatDialogModule,
	MatExpansionModule

} from '@angular/material';

@NgModule({
	imports: [
		BrowserAnimationsModule,
		MatButtonModule,
		MatCheckboxModule,
		MatRadioModule,
		MatToolbarModule,
		MatSelectModule,
		MatIconModule,
		MatCardModule,
		MatInputModule,
		MatFormFieldModule,
		MatProgressSpinnerModule,
		MatTabsModule,
		MatPaginatorModule,
		MatMenuModule,
		MatChipsModule,
		MatTooltipModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatDialogModule,
		MatExpansionModule
	],
	exports: [
		BrowserAnimationsModule,
		MatButtonModule,
		MatCheckboxModule,
		MatRadioModule,
		MatToolbarModule,
		MatSelectModule,
		MatIconModule,
		MatCardModule,
		MatInputModule,
		MatFormFieldModule,
		MatProgressSpinnerModule,
		MatTabsModule,
		MatPaginatorModule,
		MatMenuModule,
		MatChipsModule,
		MatTooltipModule,
		MatSlideToggleModule,
		MatSnackBarModule,
		MatDialogModule,
		MatExpansionModule
	],
})

export class MaterialModule { }
