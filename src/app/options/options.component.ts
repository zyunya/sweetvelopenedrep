import { Component, OnInit } from '@angular/core';
import { mainRoutes, kingRoutes } from '../services/globalenv.service';
import { i18nService } from '../services/i18n.service';
import { HelpersService } from "../services/helpers.service";
import { routerTransition } from "../animations";


@Component({
	selector: 'app-options',
	templateUrl: './options.component.html',
	styleUrls: ['./options.component.scss'],
	animations: [routerTransition()],
})
export class OptionsComponent implements OnInit {


	public mainRoutes : any = mainRoutes;
	public kingRoutes : any = kingRoutes;


	constructor( public i18n : i18nService , public HELPERS : HelpersService ) { }


	ngOnInit() {
		this.HELPERS.scrollUp();
	}


}
