import { Component, Inject, ViewContainerRef, ViewChildren, ViewChild, OnInit, AfterViewInit, Renderer2, ElementRef, QueryList } from '@angular/core';
import { ModalService } from "./modal/modal.service";
import { AddtohomescreenDialog } from "./dialogs/addtohomescreen/dialog.aths";
import { HelpersService } from "./services/helpers.service";
import { LocaldataService } from "./services/localdata.service";
import { Router, NavigationEnd } from "@angular/router";
import { i18nService } from './services/i18n.service';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	providers: [ModalService , LocaldataService ]
})


export class AppComponent implements OnInit {


	public instructionSeen: Boolean;


	constructor(

		@Inject(ModalService) public service,
		@Inject(ViewContainerRef) public viewContainerRef,
		public HELPERS: HelpersService,
		public LocaldataService : LocaldataService,
		public router: Router,
		public i18n : i18nService

	) {

		this.instructionSeen = false;
		this.i18n.defaultLang();
		this.initSavedRoute();

	}


	ngOnInit(): void {

		if(this.HELPERS.iosApp()) this.keepCurrentRoute();

	};


	public keepCurrentRoute() : void{

		this.LocaldataService.saveCurrentRoute();

	};


	public initSavedRoute() : void{

		if(this.HELPERS.iosApp())
			this.router.navigate([this.LocaldataService.getLastUrl()]);

	}


	public openInstallInstruction(): any {

		this.router.events.subscribe((event) => {

			if (event instanceof NavigationEnd) {

				if (event.url.match(`/ch/*`) || event.url == "/") {

					if (!this.HELPERS.standAlone() && (this.HELPERS.isIOS() || this.HELPERS.isAndroid())) {

						if (!this.instructionSeen) {

							setTimeout(() => {
								this.service.openModal(AddtohomescreenDialog);
								this.instructionSeen = true;
							}, 12000);

						}

					}

				}

			}

		});

	}


}
