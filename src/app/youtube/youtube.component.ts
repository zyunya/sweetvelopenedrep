import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { routerTransition } from "../animations";
import { InitfetchService } from '../services/initfetchservice';


@Component({
	selector: 'app-youtube',
	templateUrl: './youtube.component.html',
	styleUrls: ['./youtube.component.scss'],
	animations: [routerTransition()],
	encapsulation: ViewEncapsulation.Emulated,
})


export class YoutubeComponent implements OnInit {


	public videoData : any = [];


	constructor(public InitfetchService : InitfetchService) { }

	ngOnInit() {
		//this.fetchYouTubeChannel();
		this.fetchYouTubeChannelPlaylist();
	}

	public fetchYouTubeChannel() : void {

		this.InitfetchService.fetchYoutubeChannel().subscribe(

			res => { console.log(res) },

			err => console.error(err)
		)
	}


	public fetchYouTubeChannelPlaylist() : void {

		this.InitfetchService.fetchYoutubeChannelPlaylist().subscribe(

			res => { this.videoData = this.createVideoArray(res.items);},

			err => console.error(err)
		)
	}


	public createVideoArray(arr) : Array<any>{

		return arr.map( el => { return el.snippet.resourceId.videoId = `https://youtube.com/embed/${el.snippet.resourceId.videoId}?mute=0`  })
	}

}
