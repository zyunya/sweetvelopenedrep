import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser, isPlatformServer, Location } from '@angular/common';
import { Router, NavigationStart } from '@angular/router';

@Component({
	selector: 'app-noconnection',
	templateUrl: './noconnection.component.html',
	styleUrls: ['./noconnection.component.scss']
})
export class NoconnectionComponent implements OnInit {

	public isRoot : Boolean;

	constructor(
		@Inject(PLATFORM_ID) private platformId: Object,
		public location : Location,
		public router :  Router
	) { }

	ngOnInit() {

		this.router.events.subscribe(event => {
			if (this.location.path() !== '') {
				//this.isRoot = false;
			} else {
			//	this.isRoot = true;
			}
		});

	}

	public reload(): any {

		if (isPlatformBrowser(this.platformId)) {

			if (navigator.onLine) this.location.back();

		}
	}

}
