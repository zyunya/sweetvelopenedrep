import { Component, OnInit, PLATFORM_ID, Inject, ViewChild ,ElementRef } from '@angular/core';
import { ForeignermediaService } from "../services/foreignermedia.service";
import { ActivatedRoute, Router } from "@angular/router";
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { DomSanitizer, DOCUMENT } from '@angular/platform-browser';
import { MetatagsService } from "../services/metatags.service";
import { HelpersService } from "../services/helpers.service";
import { RefreshpageService } from '../services/refreshpage.service';
import { MatSnackBar } from '@angular/material';


@Component({
	selector: 'app-foreigner',
	templateUrl: './foreigner.component.html',
	styleUrls: ['./foreigner.component.scss'],
	providers: [ForeignermediaService, MetatagsService]
})


export class ForeignerComponent implements OnInit {

	@ViewChild('categoriescontainer') public categoriesContainer: ElementRef;
	public categoriesScroller: number = 0;
	public pageStep: Array<any>;
	public currentPage: any = 0;
	public currentPageFormatted: any = 1;
	public defaultSkip: Number = 0;
	public defaultLimit: any = 20;
	public currentCategory: Number | String = "ALL";
	public Data: any = [];
	public Categories: any;
	public DataLoaded: Boolean = true;
	public urlParams: any;
	public host: any;


	constructor(

		@Inject(PLATFORM_ID) private platformId: Object,
		@Inject(DOCUMENT) private document: any,
		public FMS: ForeignermediaService,
		public route: ActivatedRoute,
		public router: Router,
		public META: MetatagsService,
		public HELPERS: HelpersService,
		public snackbar: MatSnackBar,
		public RefreshpageService: RefreshpageService


	) {
		this.route.params.subscribe(params => this.urlParams = params);

		/* LISTEN TO REFRESH PAGE EVENT */
		this.RefreshpageService.postsAndCategoriesRefresh.subscribe(data => this.fetchPosts());
	}


	async ngOnInit() {

		this.route.queryParams.subscribe(params => {

			this.currentCategory = params.category || 'ALL';
			this.currentPage = params.page;
			this.currentPageFormatted = (this.currentPage / this.defaultLimit) + 1 || 1;
			this.fetchPosts();
			this.scrollWindow();

		});

		this.Categories = await this.FMS.fetchCategories(this.urlParams.name).toPromise();

		this.host = this.document.location;

	}


	public fetchPosts() {

		this.DataLoaded = false;

		this.FMS.fetchPosts(this.urlParams.name, this.currentPage, this.defaultLimit, this.currentCategory).subscribe(

			res => {

				try {
					this.META.addOgMetaTagsStaticForeigner(this.fetchOgData(res));
					this.META.setTitle(res.owner.login.toUpperCase());
				}
				catch (err) { console.error(err) }

				this.Data = res;
				this.Data.posts.forEach(el => el.owner = this.Data.owner)
				this.pageStep = this.pageCounter(res.count);
				this.DataLoaded = true;
				this.scrollWindow();
				//console.log(res)

			},

			err => { console.error(err); this.DataLoaded = true; }
		)

	}



	public pageCounter(counter): Array<any> {
		var arr = [];
		for (var i = 0; i < counter / this.defaultLimit; i++) arr.push(i);
		return arr;
	};


	public scrollWindow(): void {

		if (isPlatformBrowser(this.platformId)) {

			//window.scroll({top: 0,left: 0,behavior: 'smooth'});
			window.scrollTo(0, 0);

		}

	};


	public fetchOgData(data): any {

		var typo = data.owner.tps ? data.owner.tps.name : "";
		var login = data.owner.login || "";
		var about = data.owner.about || "";
		var image = data.owner.pokerfaceformatted || "";

		var ogdata = {

			title: `${login} ${typo}`,
			name: login,
			desc: about,
			mainimage: image

		}

		return ogdata;

	}


	public copyLink(): void {
		var loc = this.host;
		this.HELPERS.copyToClipBoard(loc);
		this.snackbar.open(loc, 'copied', { duration: 2000 });
	}


	public fetchCategory(id): void {
		this.router.navigate([], { queryParams: { category: id } });
	};


	public scrollHorizontal(direction): void {
		this.HELPERS.scrollHorizontal(direction , this.categoriesContainer.nativeElement )
	}




}
