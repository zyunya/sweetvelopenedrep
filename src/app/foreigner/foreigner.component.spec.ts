import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForeignerComponent } from './foreigner.component';

describe('ForeignerComponent', () => {
  let component: ForeignerComponent;
  let fixture: ComponentFixture<ForeignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForeignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForeignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
