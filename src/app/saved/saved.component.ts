import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { UsermediaService } from '../services/usermedia.service';
import { LocaldataService } from "../services/localdata.service";
import { HelpersService } from "../services/helpers.service";
import { routerTransition } from "../animations";


@Component({
	selector: 'app-saved',
	templateUrl: './saved.component.html',
	styleUrls: ['./saved.component.scss'],
	providers: [UsermediaService, LocaldataService],
	animations: [routerTransition()],
})



export class SavedComponent implements OnInit {


	@ViewChild("savedpostsblock") public savedpostsblock: ElementRef;
	public postsData: Array<any> = [];
	public dataLoaded: Boolean = false;
	public limit: number = 10;
	public skip: number = 0;
	public savedPosts = this.LocaldataService.getLocalPostsString();


	constructor(

		public MEDIA: UsermediaService,
		public LocaldataService: LocaldataService,
		public HELPERS : HelpersService

	) {


	}


	ngOnInit() {

		this.fetchPostsSaved();
		this.HELPERS.scrollUp();
		//console.log(this.LocaldataService.getLocalPostsString());
		//console.log(this.LocaldataService.getLocalPosts());

	}


	@HostListener('window:scroll', ['$event'])
	onWindowScroll($event) {

		if (this.savedpostsblock.nativeElement.distanceToBottom() < -80 && this.skip <= this.postsData.length) this.fetchPostsSaved();

	}


	public fetchPostsSaved() {

		this.dataLoaded = false;

		this.MEDIA.fetchPostsSaved(this.skip, this.limit, this.savedPosts).subscribe(

			res => { this.postsData.addArray(res.data); this.dataLoaded = true ; },

			err => { console.log(err); this.dataLoaded = true; }

		)

		this.skip += this.limit;

	}


}




