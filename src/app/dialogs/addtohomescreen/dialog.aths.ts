import { Component, Inject,OnInit , ViewChild , ElementRef , ViewContainerRef } from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { ModalService} from "../../modal/modal.service";
import { HelpersService } from "../../services/helpers.service";


@Component({
	selector: 'addtohomescreen-dialog',
	templateUrl: 'dialog.aths.html',
	providers: [ModalService],
	styleUrls: ['dialog.aths.scss'],
})


export class AddtohomescreenDialog {

	MODAL : any;

	constructor(public dynamic : ModalService , public Helpers : HelpersService ){

	}


	ngOnInit(){
		this.dynamic.closeModalHandler(this.MODAL);
	}


	ngOnDestroy(){

	}


	public closeDialog(){
		this.MODAL.destroy();
	}






}

