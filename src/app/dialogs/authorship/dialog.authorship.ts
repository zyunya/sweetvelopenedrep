import { Component, Inject,OnInit  } from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { ModalService} from "../../modal/modal.service";

@Component({
	selector: 'authorship-dialog',
	templateUrl: 'dialog.authorship.html',
	styleUrls: ['dialog.authorship.scss'],
	providers : [ModalService]
})


export class AuthorshipDialog {


	MODAL : any;

	constructor(
	public dynamic : ModalService 
		){

	}


	ngOnInit(){
		this.dynamic.closeModalHandler(this.MODAL);
	}


	public closeDialog(){
		this.MODAL.destroy();
	}


}

