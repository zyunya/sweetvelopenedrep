import { Component, OnInit ,Inject,ViewContainerRef} from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { ModalService } from "../../modal/modal.service";
import { Router, NavigationEnd } from "@angular/router";


@Component({
	selector: 'dialog-editorgallery',
	templateUrl: './editorgallery.dialog.html',
	styleUrls: ['./editorgallery.dialog.scss'],
	providers : [ModalService]
})
export class EditorgalleryDialog implements OnInit {

	MODAL: any;
	public images : Array<any> = [];

	constructor(

		public ModalService  : ModalService,
		public router : Router
		) { }

	async ngOnInit() {
		this.ModalService.closeModalHandler(this.MODAL);
		this.images = this.MODAL.data;
	}

	public closeDialog(){
		this.MODAL.destroy();
		event.preventDefault();
	}

	public selectImage(data) : void{
		this.ModalService.ModalDataPasser(data);
	}



}
