import { Component, OnInit } from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { ModalService } from "../../modal/modal.service";
import { UsermediaService } from "../../services/usermedia.service";
import { CategorieselectService } from '../../services/categorieselect.service';
import { Router, NavigationEnd } from "@angular/router";


@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

	MODAL: any;
	public categories : Array<Object> | any;

	constructor(
		public dynamic: ModalService,
		public CategorieselectService  : CategorieselectService ,
		public userMedia : UsermediaService,
		public router : Router
		) { }

	async ngOnInit() {
		this.categories = await this.userMedia.fetchCategories().toPromise();
		this.dynamic.closeModalHandler(this.MODAL);
	}

	public closeDialog(){
		this.MODAL.destroy();
	}

	public fetchCategory(id) : void {

		this.router.navigate(['']);
		setTimeout( () =>{
			this.CategorieselectService.selectCategory(id);
			this.dynamic.remove();
			 window.scrollTo(0,0)
		})

	}

}
