import { Component, Inject,OnInit } from '@angular/core';
import { ModalComponent } from '../../modal/modal.component';
import { ModalService} from "../../modal/modal.service";

@Component({
	selector: 'rules-dialog',
	templateUrl: 'dialog.rules.html',
	styleUrls: ['dialog.rules.scss'],
	providers : [ModalService]
})


export class RulesDialog {

	MODAL : any;

	constructor(public dynamic : ModalService){

	}


	ngOnInit(){
		this.dynamic.closeModalHandler(this.MODAL);
	}


	public closeDialog(){
		this.MODAL.destroy();
	}



}

