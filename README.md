# SERVERRENDER

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.4.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

##SEED MONGO

`Go to Seeders Dir`

`seed superuser`
	sudo NODE_ENV=dev/prod node seedsuperuser.js

`seed categories`
	sudo NODE_ENV=dev/prod node seedcategories.js

`seed typos`
	sudo NODE_ENV=dev/prod node seedtypos.js


##UPDATE SIGN UP KEY

`https://chakirberg.com/api/v1/sign/superadminkeyupdater/:password`

##MONGO BACKUP REMOTLY AND LOCALLY

`sudo mongodump -h 185.70.109.18:27017 -d krabsburger -u LOGIN -p PASSWORD  -o ~/backupdir`

##MONGO RESTORE REMOTLY AND LOCALLY

`sudo mongorestore -h 185.70.109.18:27017 -u LOGIN -p PASSWORD --db krabsburger --drop  ~/backupdir/krabsburger/`

`WHERE ~/backupdir/krabsburger/ is a path to backup which you want to restore`

`you can restore mongodb from local backup to remote server`



##MONGO RESTORE ON LOCALHOST EXAMPLE

`sudo mongorestore -h localhost:27017 --db krabsburger --drop ~/Desktop/CHAKIRBERG/BACKUPS/krabsburger/`


##MONGO DB AND MEDIA FILES BACKUP


`On the server cron makes automatic backup every day at 5:00 AM ( it runs script "/home/zyunya/CHBACKUPS/backup.sh")You can check it by command "crontab -e" (without sudo because with sudo it will show you  only root user cron operations) , it makes backup of mongoDB 	database krabsburger and recursively backups "images" folder .Server Backup folder location is "~/CHBACKUPS" which contains  "krabsburger folder(mongodb backup )" and mediabackup.zip(images backup archive) `

`If you start project on new server don't forget to create folder ~/CHBACKUPS in your user home dir`

#BASH SCRIPT TO BACKUP SERVER DATA THERE IS FILE EXPAMPLE IN PROJECT backupserver.sh

////////////////////////////////////
#!/bin/bash
PATH=/sbin:/bin:/usr/sbin:/usr/bin
zip -r ~/CHBACKUPS/mediabackup.zip /var/www/chakirberg/SERVER/images && mongodump -h 185.70.109.18 -d krabsburger -u chakirberg -p Mediamatrix16kbs -o ~/CHBACKUPS
///////////////////////////////////////////


#ALIAS TO FETCH BACKUP FROM REMOTE SERVER

`sudo nano ~/.bash_aliases`

and paste  in one line

`alias chmediabackup='scp  sw:/home/zyunya/CHBACKUPS/mediabackup.zip ~/Desktop/CHAKIRBERG/BACKUPS/ &&
 mongodump -h 185.70.109.18 -d krabsburger -u chakirberg -p Mediamatrix16kbs -o ~/Desktop/CHAKIRBERG/BACKUPS`

 `source ~/.bash_aliases`



## AUTO BACKUP ALIAS ( THIS ALIAS WORKS ON LOCAL AND REMOTE MACHINES )

`chmediabackup`

#UBUNTU GOOGLE DRIVE BACKUP WITH DRIVE

#ADD REPO
$ sudo add-apt-repository ppa:twodopeshaggy/drive
$ sudo apt-get update
$ sudo apt-get install drive

# Download and install Go
$ cd /tmp
$ wget https://storage.googleapis.com/golang/go1.5.1.linux-amd64.tar.gz
$ tar -C /usr/local -xzf go1.5.1.linux-amd64.tar.gz

# Set the environment variables on you ~/.bashrc
export GOROOT=/usr/local/go
export GOPATH=$HOME/go-workspace
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
$ mkdir ~/go-workspace
$ sudo apt-get install git
$ go get -u github.com/odeke-em/drive/cmd/drive
# If you follow these steps, you should use `drive-google` command instead of `drive`

#FINALLY
$ mkdir ~/gdrive
$ cd ~/gdrive
$ drive init
Visit this URL to get an authorization code https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=fsd3j2g2ff2g2gg2g2...
Paste authorization code:

# List the contents
$ drive list
# Download the contents
$ drive pull
# Upload our files
$ drive push
# More commands and help
$ drive help

`drive is like git repositiry so if you want to backup folders and files in CHBACKUPS you must run -drive init- inside this dir (like init repository in git ) `

#BACK UP TO GOOGLE DRIVE EXAMPLE
`drive push  -no-prompt  ~/CHBACKUPS/mediabackup.zip  ~/CHBACKUPS/krabsburger`